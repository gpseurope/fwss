# Table of contents

[[_TOC_]]


# 1. Introduction #


FWSS (Flask Web Services Server) is part of GLASS software package and is a Web Service to insert GNSS station metadata, RINEX file metadata and quality indicators in the GLASS database. 



**Developer:** Daniele Randazzo, Patrizia Pizzulo 

**Co-Developer:** Jean-Luc Menut, Juliette Legrand

**Previous developers:** Fjalar Sigurðarson, Tim Sonnemann, Sergio Bruni

**Document created:** 10. February 2017

**Last updated:** 2024-10-11

## 1.1 Current version of FWSS
The current version of the FWSS is [v2.0.13]. This documentation should reflect the status of the system in this version.



The version available in the branch `py3_master` is based on Python 3.10 .
For legacy, python 2.7 version is available in the branch `python2_master`. This version is outdated, has known issues, and should not be used.


## 1.2 Current version of the database##
When installing and using the FWSS, you will also need to set up a database. For this version of the FWSS, version [v1.3.0](https://gitlab.com/gpseurope/database/tags/v1.3.0) of the **gnss-europe** database is supported.


## 1.3 Files in the project ##
The following files are maintained in the archive:

| File                        | Description                                                                                           |
|-----------------------------|-------------------------------------------------------------------------------------------------------|
| web_server.py               |  The web service server.                                                                              |
| web_server.cfg.default      |  Default config file for the server.                                                                  |
| queries.py                  |  Queries for each API call working on the SQLAlchemy data models.                                     |
| datamodels.py               |  SQLAlchemy data models to match the gnss-europe database.                                            |
| parse_qc_xml.py             |  Method to parse QC-XML text into DB-related dicts.                                                   |
| README.md                   |  This readme file you are reading.                                                                    |
| README_Production_Server.md |  This readme explain how to run fwss on a prodution server using nginx and gunicorn                   |
| /archive-swagger            |  archive of Swagger yaml file for Service documentation and install instructions. Need to be updated  |
| /archive-doc                |  archive existing and outdated documentation                                                          |
| /templates                  |  HTML template file for Flask server homepage.                                                        |

# 2. Installation #
## 2.1 Installation ##
See [README_installation.md](https://gitlab.com/gpseurope/fwss/-/blob/py3_master/README_installation.md)

## 2.2 Run the server ##

In a terminal, run the flask server simply like this:
API documentation for programmers
    $ python web_server.py

To monitor the output from the queries.py module in python, the main functional module, run the following command in a separate terminal:

    $ touch queries.log && tail -f queries.log

Then, just for convenience you can delete the log and start over like this (the log will grow):

    $ rm queries.log && touch queries.log && tail -f queries.log


## 2.3 Troubleshooting ##

Please put your issues in [Issues in IssueTracker project](https://gitlab.com/gpseurope/issuetracker/-/issues) with the label fwss.



# 3. Known issues & bugs #

See [Issues in IssueTracker project](https://gitlab.com/gpseurope/issuetracker/-/issues) to know more about current known issues and bugs.

For legacy 
´´´
1. Results for /gps/station/{marker} can give two results for the same station. This is because contacts are not properly prioritized as primary and secondary in the Sitelogs resulting in multiple outputs.
2. Sitelog importing can return error if the structure of the sitelog JSON is corrupted. Integrity check is needed for incoming sitelog JSON to properly manage invalid structures.
3. When indexing datacenters and agency does not exist, method skips error! Given ID is then a tuple(msg-str, code).
´´´

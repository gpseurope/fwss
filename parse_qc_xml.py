# -*- coding: utf-8 -*-
"""
Parse QC XML file of rinex data from GNSS projects,
return dicts equivalent to DB tables.

COMMENTS:
  - Requires QC-XML from Anubis v2.1.3 or later!
  - Anubis v2.1.4: No <data><position>, but <head><receiver_vers> new
  
Created on Mon Dec  4 15:04:49 2017
Mod 2018-05-22: Update for Anubis v2.1.4 QC-XML
@author: tim
"""

import xmltodict
from collections import OrderedDict
import re
import hashlib


def na2none(val):
    if val == 'n/a':
        return None
    else:
        return val

def parse_qcxml(xml, body):
    """
    Take XML text and parse it into dicts corresponding to database tables.
    """

#todo show log.info the Anubis version

    #xml = xmltodict.parse(body)  # have it as dict now
    # break down for convenience:
    meta = xml['QC_GNSS']['meta']
    nsys = xml['QC_GNSS']['navi']['system']
    if isinstance(nsys, dict):
        nsys = [nsys]
    data = xml['QC_GNSS']['data']
    dsys = data['system']
    if isinstance(dsys, dict):
        dsys = [dsys]
    if 'position' in data:
        # Anubis v2.1.3
#todo write log.error to force using Anubis > 2.3, otherwise exit
        dpos = data['position']
        if isinstance(dpos, dict):
            dpos = [dpos]
    else:
        # Anubis v2.1.4
        dpos = []
    head = xml['QC_GNSS']['head']
    mset = meta['settings']
    msetsys = mset['system']
    if isinstance(msetsys, dict):
        msetsys = [msetsys]
    lmsys = [d['@type'] for d in msetsys]
    lnsys = [d['@type'] for d in nsys]
    #
    # QualityFile
    qf = OrderedDict([
        ('name', xml['QC_GNSS']['@gml:id']),
        ('id_rinexfile', None),  # query later
        ('id_data_center_structure', None),  # query later, but how???
        ('file_size', str(len(body)+1)),
        ('id_file_type', 'QC-XML'),  # query later
        ('relative_path', None),
        ('creation_date', meta['created']),
        ('md5checksum', hashlib.md5(body).hexdigest()),
        ('status', None),
    ])
    # RinexFile
    rf = OrderedDict([
        ('name', head['file_name']),
        ('md5checksum', head['file_md5sum']),
    ])
    # Header (station info to cross-validate)
    pos = head['position']['coordinate']['gml:Point']['gml:pos'].split()
    for ec in head['position']['eccentricity']['axis']:
        if ec['@name'] == 'N':
            ecn =ec['#text']
        elif ec['@name'] == 'E':
            ece = ec['#text']
        elif ec['@name'] == 'U':
            ecu = ec['#text']
    fformat = head['file_format']
    refind = re.findall(r'(RINEX)(?: ?)(\d{1})(?:.\d{1,2})?', fformat)
    file_format = refind[0][0]+refind[0][1]
    if 'receiver_vers' in head:
        # Anubis v2.1.3
        receiver_vers = head['receiver_vers']
    else:
        # Anubis v2.1.4
        receiver_vers = None
    # 
    hd = OrderedDict([
        ('file_format', file_format),
        ('site_id', head['site_id']),
        ('marker_numb', head['marker_numb']),
        ('marker_name', head['marker_name']),
        ('receiver_type', head['receiver_type']),
        ('receiver_numb', head['receiver_numb']),
        ('receiver_vers', receiver_vers),
        ('antenna_type', head['antenna_type']),
        ('antenna_dome', head['antenna_dome']),
        ('antenna_numb', head['antenna_numb']),
        ('software', head['software']),
        ('data_int', head['data_int']),
        ('x_crd', pos[0]),
        ('y_crd', pos[1]),
        ('z_crd', pos[2]),
        ('ecc_n', ecn),
        ('ecc_e', ece),
        ('ecc_u', ecu),
    ])
    #
    # Header systems dict and observations list
    hdsys = {}
    hsys = head['system']
    if isinstance(hsys, dict):
        hsys = [hsys]
    for d in hsys:
        hobs = d['obs']
        if isinstance(hobs, dict):
            hobs = [hobs]
        hdsys[d['@type']] = [e['@type'] for e in hobs]
    #
    # QcReportSummary
    qrs = OrderedDict([
        ('id_rinexfile', None),  # query later
        ('id_qc_parameters', None),  # query later
        ('date_beg', data['time_beg']),
        ('date_end', data['time_end']),
        ('data_smp', data['data_int']),
        ('obs_elev', data['total']['elev_min']),
        ('obs_have', na2none(data['total']['have'])),
        ('obs_expt', na2none(data['total']['expt'])),
        ('user_have', na2none(data['total']['have_usr'])),
        ('user_expt', na2none(data['total']['expt_usr'])),
        ('cyc_slps', data['total']['cyc_slps']),
        ('clk_jmps', data['total']['clk_jmps']),
        ('xbeg', data['excluded']['xbeg']),
        ('xend', data['excluded']['xend']),
        ('xint', data['excluded']['xint']),
        ('xsys', data['excluded']['xsys']),
    ])
    # QcParameters
    qcp = OrderedDict([
        ('software_name',
            re.findall(r'(^.*)(?: \[.*$)', meta['program'])[0]),
        ('software_version',
            re.findall(r'(?:^.*\[)(.*)(?:\]$)', meta['program'])[0]),
        ('elevation_cutoff', str(int(float(mset['elev_min'])))),
        ('gps', 'GPS' in lmsys),
        ('glo', 'GLO' in lmsys),
        ('gal', 'GAL' in lmsys),
        ('bds', 'BDS' in lmsys),
        ('qzs', 'QZS' in lmsys),
        ('sbs', 'SBS' in lmsys),
        ('nav_gps', 'GPS' in lnsys),
        ('nav_glo', 'GLO' in lnsys),
        ('nav_gal', 'GAL' in lnsys),
        ('nav_bds', 'BDS' in lnsys),
        ('nav_qzs', 'QZS' in lnsys),
        ('nav_sbs', 'SBS' in lnsys),
    ])
    # QcNavigationMsg
    qnm = [];
    for x in nsys:
        qna = OrderedDict([
            ('id_qc_report_summary', None),  # query later
            ('id_constellation', x['@type']),  # query later
            ('nsat', x['@nsat']),
            ('have', x['have']),
        ])
        qnm.append(qna)
    # QcConstellationSummary
    qcs = [];
    for x in dsys:
        qosc = []  # QcObservationSummaryC
        qosl = []  # QcObservationSummaryL
        qosd = []  # QcObservationSummaryD
        qoss = []  # QcObservationSummaryS
        # avoid a crash if only one obs available (added 2023-12-04 by Juliette Legrand to solve MTSV issue)
        if isinstance(x['obs'], dict):
            x['obs'] = [x['obs']]
        for obs in x['obs']:  # collect all observations first
            onam = obs['@type']
            otyp = onam[0]
            #fbnd = onam[1]
            #isRINEX2 = len(onam) == 2
            #if isRINEX2:
            #    chn = None
            #else:
            #    chn = onam[2]
            if otyp == 'C' or otyp == 'P':  # code, pseudorange
                qos = OrderedDict([
                    ('id_qc_constellation_summary', None),  # query later
                    ('id_gnss_obsnames', onam),  # query later
                    ('obs_sats', obs['nsat']),
                    ('obs_have', na2none(obs['have'])),
                    ('obs_expt', na2none(obs['expt'])),
                    ('user_have', na2none(obs['have_usr'])),
                    ('user_expt', na2none(obs['expt_usr'])),
                    ('cod_mpth', na2none(obs['mpth'])),
                ])
                qosc.append(qos)
                continue
            if otyp == 'D':  # doppler
                qos = OrderedDict([
                    ('id_qc_constellation_summary', None),  # query later
                    ('id_gnss_obsnames', onam),  # query later
                    ('obs_sats', obs['nsat']),
                    ('obs_have', na2none(obs['have'])),
                    ('obs_expt', na2none(obs['expt'])),
                    ('user_have', na2none(obs['have_usr'])),
                    ('user_expt', na2none(obs['expt_usr'])),
                ])
                qosd.append(qos)
                continue
            if otyp == 'L':  # carrier phase
                qos = OrderedDict([
                    ('id_qc_constellation_summary', None),  # query later
                    ('id_gnss_obsnames', onam),  # query later
                    ('obs_sats', obs['nsat']),
                    ('obs_have', na2none(obs['have'])),
                    ('obs_expt', na2none(obs['expt'])),
                    ('user_have', na2none(obs['have_usr'])),
                    ('user_expt', na2none(obs['expt_usr'])),
                    ('pha_slps', obs['slps']),
                ])
                qosl.append(qos)
                continue
            if otyp == 'S':  # signal strength
                qos = OrderedDict([
                    ('id_qc_constellation_summary', None),  # query later
                    ('id_gnss_obsnames', onam),  # query later
                    ('obs_sats', obs['nsat']),
                    ('obs_have', na2none(obs['have'])),
                    ('obs_expt', na2none(obs['expt'])),
                    ('user_have', na2none(obs['have_usr'])),
                    ('user_expt', na2none(obs['expt_usr'])),
                ])
                qoss.append(qos)
        # cont. QcConstellationSummary
        # avoid a crash if only one bnd available (added 2023-12-04 by Juliette Legrand to solve MTSV issue)
        if isinstance(x['bnd'], dict):
            x['bnd'] = [x['bnd']]
        for y in x['bnd']: # separate code and phase bnd info
            if y['@type'] == 'code':
                xcod = y
            elif y['@type'] == 'phase':
                xpha = y
        if dpos:
            # Anubis v2.1.3, or v2.1.4
            found = False
            for z in dpos: # associate position with system
                if x['@type'] == z['@type']:
                    p = z
                    found = True
                    break
            if found:
                pos = p['coordinate']['gml:Point']['gml:pos'].split()
                for dv in p['deviation']['axis']: # ensure correct component
                    if dv['@name'] == 'X':
                        dx = dv['#text']
                    elif dv['@name'] == 'Y':
                        dy = dv['#text']
                    elif dv['@name'] == 'Z':
                        dz = dv['#text']
            else:  # no guarantee to have position for all systems
                pos = [None,None,None]
                dx = None
                dy = None
                dz = None
        else:
            # Anubis v2.1.4: if no NAVIGATION or too few satellites
            # or no dual-freq signal available.
            pos = [None,None,None]
            dx = None
            dy = None
            dz = None
        qco = OrderedDict([
            ('id_qc_report_summary', None),  # query later
            ('id_constellation', x['@type']),  # query later
            ('nsat', x['@nsat']),
            ('xele', x['@xele']),
            ('epo_expt', x['epo']['expt']),
            ('epo_have', x['epo']['have']),
            ('epo_usbl', x['epo']['dual']),
            ('xcod_epo', xcod['xepo']),
            ('xcod_sat', xcod['xsat']),
            ('xpha_epo', xpha['xepo']),
            ('xpha_sat', xpha['xsat']),
            ('xint_epo', x['amb']['nepo']),
            ('xint_sat', x['amb']['nsat']),
            ('xint_sig', x['amb']['nsig']),
            ('xint_slp', x['amb']['nslp']),
            ('x_crd', pos[0]),
            ('y_crd', pos[1]),
            ('z_crd', pos[2]),
            ('x_rms', dx),
            ('y_rms', dy),
            ('z_rms', dz),
            ('QcObservationSummaryC', qosc),
            ('QcObservationSummaryD', qosd),
            ('QcObservationSummaryL', qosl),
            ('QcObservationSummaryS', qoss),
        ])
        qcs.append(qco)
    # Additional parameters for QC validation but not DB indexing:
    oth = OrderedDict([
            ('numb_epo',data['numb_epo']),
            ('numb_gap',data['numb_gap']),
    ])
    # Combining QC information to one output dict
    qc = OrderedDict([
        ('QualityFile', qf),
        ('RinexFile', rf),
        ('Header', hd),
        ('QcReportSummary', qrs),
        ('QcParameter', qcp),
        ('QcNavigationMsg', qnm),
        ('QcConstellationSummary', qcs),
        ('QcOthers', oth),
        ('hdsys', hdsys)
    ])
    #
    return qc

swagger: '2.0'
info:
  title: Geodetic Web Service API
  description: >-
    The Geodetic Web Service API contains services that facilitate access to
    station information, data and products.
  version: 0.1.0
host: '127.0.0.1:5000'
schemes:
  - http
basePath: /gps
produces:
  - application/json
paths:
  /station:
    get:
      summary: >-
        Service to get general station information from the Icelandic geodetic
        database.
      description: >
        Service that by default returns a list of all currently operational
        geodetic stations in Iceland with a general information summary for
        each. Future development may add the parameters date_from and date_to,
        so that stations that are valid for the given time interval will be
        summarised and returned.
      tags:
        - Station
      responses:
        '200':
          description: General station information in JSON format.
          schema:
            type: array
            items:
              $ref: '#/definitions/Station'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    post:
      summary: >-
        Service to post general station information to the Icelandic geodetic
        database.
      description: >-
        Service that accepts a general information summary in JSON format and
        posts it to the Icelandic geodetic database.
      tags:
        - Station
      responses:
        '201':
          description: Posting was successful.
          schema:
            type: array
            items:
              $ref: '#/definitions/Success'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  '/station/{station_id}':
    get:
      summary: >-
        Service that returns a general information summary for a single geodetic
        station.
      description: >
        By providing a station marker or id number as data to this endpoint, a
        JSON summary for the specified station will be returned if found.
      parameters:
        - name: station_id
          in: path
          description: >-
            Four character designation for the station, or integer index number
            of station in database.
          required: true
          type: string
      tags:
        - Station
      responses:
        '200':
          description: General information for a single station.
          schema:
            $ref: '#/definitions/Station'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  /sitelog:
    get:
      summary: NOT IMPLEMENTED YET
      description: >
        Service to get and post Sitelog information to the geodetic database.
        Sitelog is a format of a station's history and is widely used and thus
        implemented here.
      parameters:
        - name: starttime
          in: query
          description: Any valid time in UTC format.
          required: false
          type: string
          format: date
        - name: endtime
          in: query
          description: Any valid time in UTC format.
          required: false
          type: string
          format: date
        - name: minlatitude
          in: query
          description: Degrees.
          required: false
          type: number
      tags:
        - Sitelog
      responses:
        '200':
          description: Sitelog report in JSON format.
          schema:
            type: array
            items:
              $ref: '#/definitions/Sitelog'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    post:
      summary: Sitelog information posting for a geodetic station.
      description: >
        Post and insert a new sitelog to the database. Only one at a time
        allowed, must be JSON, must have certain info parameters.
      tags:
        - Sitelog
      consumes:
        - application/json
      parameters:
        - in: body
          name: body
          description: Sitelog object containing full station information
          required: true
          schema:
            $ref: '#/definitions/Sitelog'
      responses:
        '201':
          description: Posting was successful.
          schema:
            type: array
            items:
              $ref: '#/definitions/Success'
        '409':
          description: Conflict. Station already in the database.
          schema:
            type: array
            items:
              $ref: '#/definitions/Conflict'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  '/sitelog/{station_id}':
    get:
      summary: Get the sitelogs for a specific station.
      description: >
        Service to get the Sitelog information of one specific station. Sitelog
        is a format of a station's history and is widely used and thus
        implemented here.
      parameters:
        - name: station_id
          in: path
          description: >-
            Four character designation for the station, or integer index number
            of station in database.
          required: true
          type: string
      tags:
        - Sitelog
      responses:
        '200':
          description: Sitelog report in JSON format.
          schema:
            type: array
            items:
              $ref: '#/definitions/Sitelog'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  /data/rinex:
    get:
      summary: >-
        Service that returns statistics on 15 second 24 hour RINEX data for all
        Icelandic geodetic stations. [NOT IMPLEMENTED YET]
      description: >
        Service that returns statistics on 15 second 24 hour RINEX data for all
        Icelandic geodetic stations.
      tags:
        - Data
      responses:
        '200':
          description: A list of events
          schema:
            type: array
            items:
              $ref: '#/definitions/Statistics'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    post:
      summary: >-
        Service that receives information for a new datafile and posts it to the
        Icelandic geodetic database.
      description: >
        Service that receives information for a new datafile and posts it to the
        Icelandic geodetic database.
      tags:
        - Data
      responses:
        '201':
          description: Posting was successful.
          schema:
            type: array
            items:
              $ref: '#/definitions/Success'
        '409':
          description: Conflict. Data file already in the database.
          schema:
            type: array
            items:
              $ref: '#/definitions/Conflict'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  '/data/rinex/{station_id}':
    get:
      summary: >-
        Service that returns list of RINEX files with info for a
        particular station.
      description: >
        Service that returns JSON array of indexed RINEX files with associated info for a
        particular station.
      parameters:
        - name: station_id
          in: path
          description: Four digit designation for the station.
          required: true
          type: string
        - name: status
          in: query
          description: RINEX file status number in DB.
          required: false
          type: integer
        - name: date_from
          in: query
          description: Any valid time in UTC format.
          required: false
          type: string
          format: date
        - name: date_to
          in: query
          description: Any valid time in UTC format.
          required: false
          type: string
          format: date
      tags:
        - Data
      responses:
        '200':
          description: A list of RINEX files with info.
          schema:
            type: array
            items:
              $ref: '#/definitions/File'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  '/data/rinex/{station_id}/{file_id}':
    get:
      summary: >-
        Service that returns information regarding a particular file.
         [NOT IMPLEMENTED YET]
      description: >
        Service that returns statistics on 15 second 24 hour RINEX data for a
        particular station.
      parameters:
        - name: station_id
          in: path
          description: Four digit designation for the station.
          required: true
          type: string
        - name: file_id
          in: path
          description: Integer id for a file.
          required: true
          type: string
      tags:
        - Data
      responses:
        '200':
          description: File information.
          schema:
            $ref: '#/definitions/File'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  /data/qcfile:
    post:
      summary: Post QC-XML based quality info for RINEX data
      description: >
        A QC-XML file (quality summary) for a RINEX file can be posted here. 
        The body is expected to be the XML text produced by the Anubis software
        for one RINEX file.
      consumes:
      - "application/xml"
      parameters:
        - in: body
          name: body
          description: QC-XML text containing RINEX quality summary
          required: true
          schema:
            $ref: '#/definitions/QC_file'
      tags:
        - Data
      responses:
        '200':
          description: Posting was successful.
          schema:
            type: array
            items:
              $ref: '#/definitions/QC_Success'
        '409':
          description: Conflict. QC summary for this RINEX file already in the database.
          schema:
            type: array
            items:
              $ref: '#/definitions/QC_Success'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  '/data/qcfile/{station_id}/{file_id}':
    get:
      summary: >-
        Get QC-XML information regarding a particular RINEX file.
         [NOT IMPLEMENTED YET]
      description: >
        Service that returns QC-XML information (JSON?) regarding a particular
        RINEX file.
      parameters:
        - name: station_id
          in: path
          description: Four digit designation for the station.
          required: true
          type: string
        - name: file_id
          in: path
          description: Integer id for a file.
          required: true
          type: string
      tags:
        - Data
      responses:
        '200':
          description: QC summary.
          schema:
            $ref: '#/definitions/QC_file'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  /agency:
    get:
      summary: List all agencies registered in database.
      description: |
        Return a list of all indexed agencies with additional info.
      tags:
        - Other
      responses:
        '200':
          description: JSON array of all agency info.
          schema:
            type: array
            items:
              $ref: '#/definitions/agency'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
definitions:
  Station:
    type: object
    properties:
      agency:
        type: object
        properties:
          name:
            type: string
      id:
        type: number
      marker:
        type: string
      name:
        type: string
      location:
        type: object
        properties:
          country:
            type: string
          city:
            type: string
          coordinates:
            type: object
            properties:
              altitude:
                type: number
              longitude:
                type: number
              latitude:
                type: number
          state:
            type: string
      date_from:
        type: string
        description: ISO 8601 date string (UTC)
      date_to:
        type: string
        description: ISO 8601 date string (UTC)
      station_networks:
        type: array
        items:
          properties:
            network:
              type: object
              properties:
                name:
                  type: string
  Statistics:
    type: object
    properties:
      azimuthalgap:
        type: number
        description: Description needed.
      date_str:
        type: string
        format: date
        description: Description needed.
      depth:
        type: number
        description: Description needed.
  File:
    type: object
    properties:
      id:
        type: integer
      station_marker:
        type: string
      station_name:
        type: string
      file_name:
        type: string
      file_type:
        type: string
      sampling_window:
        type: string
      sampling_frequency:
        type: string
      data_center:
        type: string
      protocol:
        type: string
      hostname:
        type: string
      root_path:
        type: string
      directory_naming:
        type: string
      relative_path:
        type: string
      reference_date:
        type: string
        format: date
      creation_date:
        type: string
        format: date
      published_date:
        type: string
        format: date
      file_size:
        type: integer
      md5_checksum:
        type: string
      status:
        type: integer
  Sitelog:
    type: object
    properties:
      antennta_num:
        type: number
      cdp_num:
        type: number
      instrument_collocation:
        type: array
        items:
          type: object
      comment:
        type: string
      conditions:
        type: array
        items:
          type: object
      country:
        type: string
      date_from:
        type: string
      date_to:
        type: string
      description:
        type: string
      geological:
        type: object
      iers_domes:
        type: string
      local_ties:
        type: string
      location:
        type: object
      logs:
        type: string
      marker:
        type: string
      monument:
        type: object
      name:
        type: string
      receiver_num:
        type: number
      station_contacts:
        type: array
        items:
          type: object
      station_items:
        type: array
        items:
          type: object
      station_network:
        type: array
        items:
          type: object
      station_type:
        type: string
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      fields:
        type: string
  Success:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      fields:
        type: string
  Conflict:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      fields:
        type: string
  QC_file:
    type: object
    description: >-
      This schema definition still has to be finished! But it would be pretty
      big...
  QC_Success:
    type: object
    description: >-
      The JSON response can contain messages of type info, warning and error.
    properties:
      messages:
        type: object
        properties:
          info:
            type: array
            items:
              type: string
          warning:
            type: array
            items:
              type: string
          error:
            type: array
            items:
              type: string
  agency:
    type: object
    properties:
      id:
        type: integer
      name:
        type: string
      abbreviation:
        type: string
      address:
        type: string
      www:
        type: string
      infos:
        type: string

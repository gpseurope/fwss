# -*- coding: utf-8 -*-
# Authors:
#   Fjalar SigurÃ°arson (fjalar@vedur.is)
# Date:   2016-2017

'''
This is the module doc string
'''
import http.client
## General imports
import re
import sys
import json
import logging
import logging.handlers
import decimal
from contextlib import closing
from datetime import datetime
#from httplib import NO_CONTENT, OK, NOT_FOUND, CONFLICT, BAD_REQUEST, INTERNAL_SERVER_ERROR, CREATED
from http.client import NO_CONTENT, OK, NOT_FOUND, CONFLICT, BAD_REQUEST, INTERNAL_SERVER_ERROR, CREATED, ACCEPTED, NOT_MODIFIED

## SqlAlchemy imports
from sqlalchemy import create_engine, or_, and_, func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound

from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import text
import os
import xmltodict
import inspect

#todo 09/06/2023 better format with datamodel.Node
## Data model imports
from datamodel import (
    Station,
    Contact,
    StationContact,
    StationNetwork,
    StationColocation,
    Connection,
    Document,
    EstimatedCoordinate,
    OtherFile,
    Jump,
    Noise,
    ReferencePositionVelocity,
    Log,
    UserGroupStation,
    SeasonalSignal,
    Query,
    FileGenerated,
    Location,
    Agency,
    City,
    State,
    Country,
    Network,
    Bedrock,
    Geological,
    Tectonic,
    StationType,
    Coordinates,
    Monument,
    LocalTie,
    InstrumentColocation,
    ItemType,
    Attribute,
    RinexFile,
    FileType,
    DataCenter,
    StationItem,
    Item,
    ItemAttribute,
    ReceiverType,
    AntennaType,
    RadomeType,
    DataCenterStructure,
    QualityFile,
    QcParameter,
    QcConstellationSummary,
    QcNavigationMsg,
    QcObservationSummaryC,
    QcObservationSummaryD,
    QcObservationSummaryL,
    QcObservationSummaryS,
    QcReportSummary,
    Constellation,
    GnssObsname,
    RinexErrorType,
    RinexError,
    Node,
    Log,
    LogType,
    Effect,
    Condition,
    DatacenterStation,
)


# from datamodel import (
#     Station, Contact, StationContact, StationNetwork, StationColocation,
#     Connection, Document, EstimatedCoordinate, OtherFile, Jump, Noise,
#     ReferencePositionVelocity, Log, UserGroupStation, SeasonalSignal,
#     Query, FileGenerated,
#     Location, Agency, City, State, Country, Network,
#     Bedrock, Geological, Tectonic, StationType, Coordinates,
#     Monument, LocalTie, InstrumentColocation, ItemType, Attribute, RinexFile,
#     FileType, DataCenter, StationItem, Item, ItemAttribute, ReceiverType,
#     AntennaType, RadomeType, DataCenterStructure,
#     QualityFile, QcParameter, QcConstellationSummary, QcNavigationMsg,
#     QcObservationSummaryC, QcObservationSummaryD,
#     QcObservationSummaryL, QcObservationSummaryS,
#     QcReportSummary, Constellation, GnssObsname,
#     RinexErrorType, RinexError,
#     Log, LogType, Effect, Condition,
#     DatacenterStation
# )



# XML parser for QC files:
from parse_qc_xml import parse_qcxml

# __SB__
from sqlalchemy.dialects import postgresql

import string
from random import *


"""
    Stuff needed for benchmarking
"""
from functools import wraps
import time


#  THIS PART IS SPECIFIC FOR BENCHMARK LOG
#=======================================================
__benchmark__ = {'active': False, 'threshold_seconds': 0.5}
benchmark_logger = None
PROF_DATA = {}

def profile(func):
    global __benchmark__

    @wraps(func)
    def with_profiling(*args, **kwargs):
        start_time = time.time()

        ret = func(*args, **kwargs)

        if __benchmark__['active']:
            elapsed_time = time.time() - start_time

            if func.__name__ not in PROF_DATA:
                PROF_DATA[func.__name__] = {'called_times': 0, 'min_time': 999999999, 'max_time': 0, 'avg_time': 0}
            if elapsed_time < PROF_DATA[func.__name__]['min_time']:
                PROF_DATA[func.__name__]['min_time'] = elapsed_time
            if elapsed_time > PROF_DATA[func.__name__]['max_time']:
                PROF_DATA[func.__name__]['max_time'] = elapsed_time
            PROF_DATA[func.__name__]['avg_time'] = \
                (PROF_DATA[func.__name__]['avg_time'] * PROF_DATA[func.__name__]['called_times'] + elapsed_time) / (PROF_DATA[func.__name__]['called_times'] + 1)
            PROF_DATA[func.__name__]['called_times'] += 1

            if elapsed_time >= __benchmark__['threshold_seconds']:
                msg = "Function %s called %d times. Execution time last: %.3f,  min: %.3f, max: %.3f, average: %.3f" \
                  % (func.__name__,
                     PROF_DATA[func.__name__]['called_times'],
                     elapsed_time,
                     PROF_DATA[func.__name__]['min_time'],
                     PROF_DATA[func.__name__]['max_time'],
                     PROF_DATA[func.__name__]['avg_time'])
                benchmark_logger.info(msg)
        return ret

    return with_profiling

def clear_prof_data():
    global PROF_DATA
    PROF_DATA = {}

"""
     prepare the logger for benchmark messages
"""
def create_benchmark_log(log_info):
    global benchmark_logger
    benchmark_logger = logging.getLogger('BC_' + __name__)
    benchmark_logger.setLevel(logging.DEBUG)

    log_dir = 'log'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir, 0o755)

    bc_log_file_name = os.path.join(log_dir, 'benchmark_queries.log')
    bc_f_handler = logging.handlers.RotatingFileHandler(
        bc_log_file_name,
        maxBytes=log_info['maxBytes'],
        backupCount=log_info['backupCount'])
    bc_f_handler.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    bc_formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                                     datefmt='%m/%d/%y %H:%M')

    # create formatter and add it to the handlers
    bc_f_handler.setFormatter(bc_formatter)

    # add the handlers to the logger
    benchmark_logger.addHandler(bc_f_handler)
#=======================================================



## Support classes ##

class NoIDException(Exception):
    """
    Exception classe for query handling
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class NoValueException(Exception):
    """
    Exception classe for query handlingjoin
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class GenericException(Exception):
    pass

class JSONCustomEncoder(json.JSONEncoder):
    """ Custom JSON encoder to handle datetime objects
    """
    def default(self, obj):

        if isinstance(obj, datetime):        
            return obj.strftime('%Y-%m-%d %H:%M:%S')

        if isinstance(obj, decimal.Decimal):
            return float(obj)

        return json.JSONEncoder.default(self, obj)

## Utility functions ##
        
def row2dict(row):
    """
    Convert sqlalchemy query output (1 table object) into dict.
    This avoids having to deal with 'column.__dict__', which includes an extra
    field '_sa_instance_state' and may or may not get more in future releases.
    """
    d = {}
    for column in row.__table__.columns:
        d[column.name] = getattr(row, column.name)
    return d

def querylist2dictlist(query_list):
    """
    Convert sqlalchemy query output (list of tuples of table objects) into
    a list of dicts.
    This is convenient if multiple (n>1) results for multiple output tables
    are given by session.query().
    The output of this method will have the structure:
        out_list[0][table_name][column_name]
        ...
        out_list[n-1][table_name][column_name]
    """
    out_list = []
    for query_tuple in query_list:
        mod_dict = {}
        # Convert each table object (1 row) to dict:
        for q_obj in query_tuple:
            mod_dict[q_obj.__tablename__] = row2dict(q_obj)
        out_list.append(mod_dict)
    return out_list


## Main class ##

class Queries(object):
    '''
    This is a doc string
    '''

    ### Init ###

    def __init__(self, db_info, log_info):
        '''
        This is a doc string
        '''

        ## -------------------------------------------------- ##
        ## 1) Set up database connection and bind to session  ##
        ## -------------------------------------------------- ##

        # FIXME: This needs to be moved to a config file.
        #db_info = {'db_type':'postgresql', 'user':'gps', 'password':'gps',
        #           'hostname':'localhost', 'db_name':'gps'}
        #self.benchmark = benchmark

        engine = create_engine(
            '{0}://{1}:{2}@{3}:{4}/{5}'.format(
                db_info['db_type'],
                db_info['username'],
                db_info['password'],
                db_info['hostname'],
                db_info['db_port'],
                db_info['db_name']),
                    echo=False) # switch to True to display SQL requests
        self.session = sessionmaker(bind=engine)


        ## ----------------- ##
        ## 2) Define logger  ##
        ## ----------------- ##

        # create logger with current application
        log_dir = 'log'
        if not os.path.exists(log_dir):
            os.makedirs(log_dir, 0o755)

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create file handler which logs even debug messages
        log_file_name = os.path.join(log_dir, 'queries.log')
        f_handler = logging.handlers.RotatingFileHandler(
                log_file_name,
                maxBytes=int(log_info['maxBytes']),
                backupCount=int(log_info['backupCount']))
        f_handler.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.ERROR)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                                       datefmt='%m/%d/%y %H:%M')
        # create formatter and add it to the handlers
        f_handler.setFormatter(formatter)
        c_handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger.addHandler(f_handler)
        self.logger.addHandler(c_handler)

        self.logger.info("Logger has been defined")
        self.logger.info("Queries object instantiated")
        self.logger.info("Rotation info. maxBytes: {}; backupCount: {}" \
                         .format(log_info['maxBytes'],
                                 log_info['backupCount']))

        """
            Create a separate logger for qc Error/Warning
        """
        self.qc_logger = logging.getLogger('QC_'+__name__)
        self.qc_logger.setLevel(logging.DEBUG)

        qc_log_file_name = os.path.join(log_dir, 'qc_queries.log')
        qc_f_handler = logging.handlers.RotatingFileHandler(
                qc_log_file_name,
                maxBytes=log_info['maxBytes'],
                backupCount=log_info['backupCount'])
        qc_f_handler.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        qc_formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                                       datefmt='%m/%d/%y %H:%M')

        # create formatter and add it to the handlers
        qc_f_handler.setFormatter(qc_formatter)

        # add the handlers to the logger
        self.qc_logger.addHandler(qc_f_handler)

        # create container for expandable warning message list:
        self.warnings = []
        # create container for error messages:
        self.errors = []
        # create container for normal messages:
        self.infos = []

        # only the functions whose context include the rinex file use this attribute
        self.rinex_filename = None


    def station_list_get(self,args):
        '''
        This is a doc string
        '''

        self.logger.info('station_list() was called')

        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:

            ### Execute query ###
            self.logger.info('Just before the query...')

            # Sergio Bruni. Added the three outerjoin the make query to have success
            # also for stations who do not have any contact
            # q = session.query(Station, Agency, Coordinates,\
            #                 City, State, Country, Network)\
            #         .outerjoin(StationContact)\
            #         .outerjoin(Contact)\
            #         .outerjoin(Agency)\
            #         .join(Location)\
            #         .join(Coordinates)\
            #         .join(City)\
            #         .join(State)\
            #         .join(Country)\
            #         .join(StationNetwork)\
            #         .join(Network)
            # my_test = str(q)


            query_list = session.query(Station, Agency, Coordinates,\
                            City, State, Country, Network)\
                    .outerjoin(StationContact)\
                    .outerjoin(Contact)\
                    .outerjoin(Agency)\
                    .join(Location)\
                    .join(Coordinates)\
                    .join(City)\
                    .join(State)\
                    .join(Country)\
                    .join(StationNetwork)\
                    .join(Network)\
                    .all()
            print(str(query_list))

            if query_list:

                ### Define a dict to collect the dictified models
                model_dicts = {'agency':{'name':None},
                               'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None, 'country_code':None, 'monument_num':None, 'receiver_num':None},
                               'country':{'name':None},
                               'city':{'name':None},
                               'state':{'name':None},
                               'network':{'name':None},
                               'coordinates':{'lat':None,'lon':None,'altitude':None}
                            }

                ### Loop through each collection of tables for every station
                a = 0
                for query in query_list:
                    a += 1
                    ### Warp the model object to a dict
                    for query_object in query:
                        # Sergio Bruni. Added the following check to avoid exception
                        # related to three previous added outerjoin
                        if query_object:
                            # FIXME: why this is needed has to be explained. I have a theory...
                            try:
                                query_dict = query_object.__dict__
                                query_dict.pop('_sa_instance_state')
                            except:
                                pass

                            model_dicts[query_object.__tablename__] = query_dict

                    ### Construct response ###
                    response = {
                        'agency': {
                            'name': model_dicts['agency']['name']
                        },
                        'id' : model_dicts['station']['id'],
                        'marker': model_dicts['station']['marker'],
                        'monument_num': model_dicts['station']['monument_num'],
                        'receiver_num': model_dicts['station']['receiver_num'],
                        'name': model_dicts['station']['name'],
                        'location': {
                            'country': {
                                'name': model_dicts['country']['name']
                            },
                            'city': {
                                'name': model_dicts['city']['name']
                            },
                            'coordinates': {
                                'altitude': model_dicts['coordinates']['altitude'],
                                'lon': model_dicts['coordinates']['lon'],
                                'lat': model_dicts['coordinates']['lat']
                            },
                            'state': {
                                'name': model_dicts['state']['name']
                            }
                        },
                        'date_to': model_dicts['station']['date_to'],
                        'date_from': model_dicts['station']['date_from'],
                        'country_code': model_dicts['station']['country_code'],
                        'station_networks': [
                            {
                                'network': {
                                    'name': model_dicts['network']['name']
                                }
                            }
                        ]
                    }

                    response_list.append(response)

            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def station_list_get_new(self,args):
        self.logger.info('station_list() was called')

        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:
            query_list = session.query(Station,Coordinates, City, State, Country)\
                .join(Location)\
                .join(Coordinates)\
                .join(City)\
                .join(State)\
                .join(Country)\
                .all()


            if query_list:

                ### Define a dict to collect the dictified models
                model_dicts = {
                               'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None, 'country_code':None, 'monument_num':None, 'receiver_num':None},
                               'country':{'name':None},
                               'city':{'name':None},
                               'state':{'name':None},
                               'coordinates':{'lat':None,'lon':None,'altitude':None}
                            }

                ### Loop through each collection of tables for every station
                for query in query_list:
                    ### Warp the model object to a dict
                    for query_object in query:
                        # Sergio Bruni. Added the following check to avoid exception
                        # related to three previous added outerjoin
                        if query_object:
                            try:
                                query_dict = query_object.__dict__
                                query_dict.pop('_sa_instance_state')
                            except:
                                pass

                            model_dicts[query_object.__tablename__] = query_dict

                    ### Construct response ###
                    response = {
                        'id' : model_dicts['station']['id'],
                        'marker': model_dicts['station']['marker'],
                        'monument_num': model_dicts['station']['monument_num'],
                        'receiver_num': model_dicts['station']['receiver_num'],
                        'name': model_dicts['station']['name'],
                        'location': {
                            'country': {
                                'name': model_dicts['country']['name']
                            },
                            'city': {
                                'name': model_dicts['city']['name']
                            },
                            'coordinates': {
                                'altitude': model_dicts['coordinates']['altitude'],
                                'lon': model_dicts['coordinates']['lon'],
                                'lat': model_dicts['coordinates']['lat']
                            },
                            'state': {
                                'name': model_dicts['state']['name']
                            }
                        },
                        'date_to': model_dicts['station']['date_to'],
                        'date_from': model_dicts['station']['date_from'],
                        'country_code': model_dicts['station']['country_code'],
                        'station_networks': [
                        ],
                        'station_contacs': [
                        ]
                     }

                    networks_list = session.query(Network)\
                        .join(StationNetwork)\
                        .filter(StationNetwork.id_station == query.Station.id)

                    for n in networks_list:
                        response['station_networks'].append({'name': n.name})

                    contacts_list = session.query(Contact)\
                        .join(StationContact)\
                        .filter(StationContact.id_station == query.Station.id)

                    for c in contacts_list:
                        agency_name = session.query(Agency.name).filter(Agency.id == c.id_agency).scalar()

                        response['station_contacs'].append({
                            'agency': agency_name,
                            'name': c.name,
                            'email': c.email
                        })

                    response_list.append(response)

            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    # def station_single_get(self, marker_id=None):
    #     '''
    #     This is a doc string
    #     '''
    #
    #     ## Preset result
    #     result = None
    #     status_code = NO_CONTENT
    #
    #     self.logger.info('station_single() was called with marker_id: {}'.format(marker_id))
    #
    #     id_station = None
    #     marker = None
    #     query_list = None
    #     response_list = []
    #
    #     #Station identifier from http-input parameters
    #     if marker_id:
    #         if marker_id.isdigit():
    #             self.logger.info('marker_id is TYPE "digit"')
    #             id_station = int(marker_id)
    #         else:
    #             self.logger.info('marker_id is TYPE "marker"')
    #             marker = marker_id.upper()
    #     else:
    #         self.logger.info('marker_id: {} -> value is missing'.format(marker_id))
    #
    #
    #     with closing(self.session()) as session:
    #
    #         ### Execute query ###
    #
    #         query_list = session.query(Station, Agency, Coordinates,\
    #                             City, State, Country, Network)\
    #                             .outerjoin(StationContact)\
    #                             .outerjoin(Contact)\
    #                             .outerjoin(Agency)\
    #                             .join(Location)\
    #                             .join(Coordinates)\
    #                             .join(City)\
    #                             .join(State)\
    #                             .join(Country)\
    #                             .join(StationNetwork)\
    #                             .join(Network)\
    #                             .filter(or_(Station.marker == marker, Station.id == id_station)) \
    #                             .one_or_none()
    #
    #         if query_list:
    #
    #             self.logger.info('Length of query result list: {0}'.format(len(query_list)))
    #
    #             ### Define a dict to collect the dictified models
    #             model_dicts = {'agency':{'name':None},
    #                            'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None},
    #                            'country':{'name':None},
    #                            'city':{'name':None},
    #                            'state':{'name':None},
    #                            'network':{'name':None},
    #                            'coordinates':{'lat':None,'lon':None,'altitude':None}
    #                         }
    #
    #             ### Warp the model object to a dict
    #             for query_object in query_list:
    #                 if query_object:
    #                     query_dict = {}
    #                     try:
    #                         query_dict = query_object.__dict__
    #                         query_dict.pop('_sa_instance_state')
    #                     except:
    #                         pass
    #
    #                     model_dicts[query_object.__tablename__] = query_dict
    #
    #             ### Construct response ###
    #             response = {
    #                 'agency': {
    #                     'name': model_dicts['agency']['name']
    #                 },
    #                 'id' : model_dicts['station']['id'],
    #                 'marker': model_dicts['station']['marker'],
    #                 'name': model_dicts['station']['name'],
    #                 'location': {
    #                     'country': {
    #                         'name': model_dicts['country']['name']
    #                     },
    #                     'city': {
    #                         'name': model_dicts['city']['name']
    #                     },
    #                     'coordinates': {
    #                         'altitude': model_dicts['coordinates']['altitude'],
    #                         'lon': model_dicts['coordinates']['lon'],
    #                         'lat': model_dicts['coordinates']['lat']
    #                     },
    #                     'state': {
    #                         'name': model_dicts['state']['name']
    #                     }
    #                 },
    #                 'date_to': model_dicts['station']['date_to'],
    #                 'date_from': model_dicts['station']['date_from'],
    #                 'station_networks': [
    #                     {
    #                         'network': {
    #                             'name': model_dicts['network']['name']
    #                         }
    #                     }
    #                 ]
    #             }
    #
    #             response_list.append(response)
    #             result = json.dumps(response_list, cls=JSONCustomEncoder)
    #             status_code = OK
    #
    #         else:
    #             # No result - no content
    #             status_code = NO_CONTENT
    #             result = None
    #
    #     #print response_list
    #     #result = json.dumps(response_list, cls=JSONCustomEncoder)
    #     #status_code = OK
    #
    #     return result, status_code

    # Sergio Bruni. new version of station_single_get
    # old version did not work with stations that are related to more than one network
    def station_single_get_new(self, marker_id=None):
        '''
        This is a doc string
        '''

        ## Preset result
        result = None
        status_code = NO_CONTENT

        self.logger.info('station_single() was called with marker_id: {}'.format(marker_id))

        id_station = None
        marker = None
        query_list = None
        response_list = []

        #Station identifier from http-input parameters
        if marker_id:
            if marker_id.isdigit():
                self.logger.info('marker_id is TYPE "digit"')
                id_station = int(marker_id)
            else:
                self.logger.info('marker_id is TYPE "marker"')
                marker = marker_id.upper()
        else:
            self.logger.info('marker_id: {} -> value is missing'.format(marker_id))


        with closing(self.session()) as session:

            ### Execute query ###

            #query_list = session.query(Station, Agency, Coordinates,\

            try:
                query_list = session.query(Station, Coordinates,\
                                    City, State, Country)\
                                    .join(Location)\
                                    .join(Coordinates)\
                                    .join(City)\
                                    .join(State)\
                                    .join(Country)\
                                    .filter(or_(Station.marker == marker, Station.id == id_station)) \
                                    .one_or_none()
            except MultipleResultsFound:
                status = 'More then one station found with marker: [{}]. I do not know which station return'\
                    .format(marker)
                self.logger.error(status)
                status_code = CONFLICT
                result = json.dumps({'error': status})
                return result, status_code


            if not query_list:
                status = 'station with marker: [{}] NOT FOUND' \
                    .format(marker)
                self.logger.error(status)
                result = json.dumps({'error': status})
                status_code = NOT_FOUND
                return result, status_code

            self.logger.info('Length of query result list: {0}'.format(len(query_list)))

            ### Define a dict to collect the dictified models
            model_dicts = {
                'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None},
                'country':{'name':None},
                'city':{'name':None},
                'state':{'name':None},
                'coordinates':{'lat':None,'lon':None,'altitude':None},
                'networks': [],
                'contacs': []
            }


            ### Warp the model object to a dict
            for query_object in query_list:
                # FIXME: why this is needed has to be explained. I have a theory...
                if query_object:
                    query_dict = {}
                    try:
                        query_dict = query_object.__dict__
                        query_dict.pop('_sa_instance_state')
                    except:
                        pass

                    model_dicts[query_object.__tablename__] = query_dict
            # add separately, the networks and contacts to response structure

            networks = session.query(Network).\
                join(StationNetwork).\
                filter(StationNetwork.id_station==query_list.Station.id)

            for n in networks:
                model_dicts['networks'].append({'name': n.name})

            contacts = session.query(Contact).\
                join(StationContact).\
                filter(StationContact.id_station==query_list.Station.id)

            for c in contacts:
                agency_name = session.query(Agency.name).filter(Agency.id==c.id_agency).scalar()
                model_dicts['contacs'].append({
                    'agency': agency_name,
                    'name': c.name,
                    'email': c.email
                })

            ### Construct response ###
            response = {
                'id' : model_dicts['station']['id'],
                'marker': model_dicts['station']['marker'],
                'name': model_dicts['station']['name'],
                'location': {
                    'country': {
                        'name': model_dicts['country']['name']
                    },
                    'city': {
                        'name': model_dicts['city']['name']
                    },
                    'coordinates': {
                        'altitude': model_dicts['coordinates']['altitude'],
                        'lon': model_dicts['coordinates']['lon'],
                        'lat': model_dicts['coordinates']['lat']
                    },
                    'state': {
                        'name': model_dicts['state']['name']
                    }
                },
                'date_to': model_dicts['station']['date_to'],
                'date_from': model_dicts['station']['date_from'],
                'networks': model_dicts['networks'],
                'contacs': model_dicts['contacs']
            }



            #response_list.append(response)
            result = json.dumps(response, cls=JSONCustomEncoder)
            status_code = OK
            return result, status_code

    # Sergio Bruni
    # I prefer to comment this function because of it is tied to the endpoint station2 that
    # is not put into the splash default page and do not work
    # if marker is not unique
    #==========================
    # def station_single_get2(self, marker_id=None):
    #     '''
    #     This is a doc string
    #     '''
    #
    #     ## Preset result
    #     result = None
    #     status_code = NO_CONTENT
    #
    #     self.logger.info('station_single2() was called with marker_id: {}'.format(marker_id))
    #
    #     id_station = None
    #     marker = None
    #     query_list = None
    #     response_list = []
    #
    #     #Station identifier from http-input parameters
    #     if marker_id:
    #         marker = marker_id.upper()
    #     else:
    #         self.logger.info('marker_id: {} -> value is missing'.format(marker_id))
    #
    #
    #     with closing(self.session()) as session:
    #
    #         ### Execute query ###
    #
    #         q = session.query(Station) \
    #                             .filter(Station.marker == marker) \
    #                             .one_or_none()
    #
    #         if q:
    #             status_code = OK
    #             result = None
    #
    #         else:
    #             # No result - no content
    #             status_code = NO_CONTENT
    #             result = None
    #
    #     return result, status_code

    def station_single_delete(self, name_or_id=None):
        #self.logger.info('station_single_delete() was called with marker_id: {}'.format(marker_id))

        with closing(self.session()) as session:

            # Station identifier from http-input parameters
            if name_or_id:
                if name_or_id.isdigit():
                    station_id = session.query(Station.id) \
                        .filter(Station.id == name_or_id) \
                        .scalar()
                else:
                    if not len(name_or_id) == 9:
                        msg = "station_single_delete: station name must be 9 characters len (marker + monument number + receiver number + country code. [{}]" \
                            .format(name_or_id)
                        self.logger.error(msg)
                        status_code = BAD_REQUEST
                        result = msg
                        return result, status_code

                    marker =  name_or_id[:4]
                    monument_num = int(name_or_id[4])
                    receiver_num = int(name_or_id[5])
                    country_code = name_or_id[6:]

                    station_id = session.query(Station.id) \
                        .filter(and_(Station.marker == marker, \
                            Station.monument_num == monument_num, \
                            Station.receiver_num == receiver_num, \
                            Station.country_code == country_code)) \
                            .scalar()
                    name = name_or_id.upper()
            else:
                msg = 'station_single_delete: parameter name/id  missing'
                self.logger.error(msg)
                status_code = BAD_REQUEST
                result = msg
                return result, status_code

            if not  station_id:
                status_code = NOT_FOUND
                result = None
                return result, status_code

            try:
                # station_network
                session.query(StationNetwork).filter_by(id_station=station_id).delete(synchronize_session=False)

                # station_contact
                session.query(StationContact).filter_by(id_station=station_id).delete(synchronize_session=False)

                # get all items of the station to delete
                item_ids = session.query(StationItem.id_item).filter(StationItem.id_station == station_id)

                # item_attribute
                session.query(ItemAttribute).filter(ItemAttribute.id_item.in_(item_ids.subquery())).delete(synchronize_session=False)

                # station_item
                session.query(StationItem).filter(StationItem.id_station == station_id).delete(synchronize_session=False)

                # item
                session.query(Item).filter(Item.id.in_(item_ids.subquery())).delete(synchronize_session=False)

                # datacenter_station
                session.query(DatacenterStation).filter(DatacenterStation.id_station == station_id).delete(synchronize_session=False)

                # station_colocation
                session.query(StationColocation).filter(StationColocation.id_station == station_id).delete(synchronize_session=False)

                # condition
                session.query(Condition).filter(Condition.id_station == station_id).delete(synchronize_session=False)

                # file_generated
                session.query(FileGenerated).filter(FileGenerated.id_station == station_id).delete(synchronize_session=False)

                # instrument_collocation
                session.query(InstrumentColocation).filter(InstrumentColocation.id_station == station_id).delete(synchronize_session=False)

                # connections
                session.query(Connection).filter(Connection.station == station_id).delete(synchronize_session=False)

                # document
                session.query(Document).filter(Document.id_station == station_id).delete(synchronize_session=False)

                # estimated_coordinates
                session.query(EstimatedCoordinate).filter(EstimatedCoordinate.id_station == station_id).delete(synchronize_session=False)

                # get all rinex_files of the station to delete
                rinex_files_ids_list = session.query(RinexFile.id).filter(RinexFile.id_station == station_id).all()
                if rinex_files_ids_list:
                    for rinex_id in rinex_files_ids_list:
                        # quality_file
                        session.query(QualityFile).filter(QualityFile.id_rinexfile == rinex_id).delete(synchronize_session=False)

                        # qc_report_summary
                        self.delete_QcReportSummary(session, rinex_id)

                # other_files
                session.query(OtherFile).filter(OtherFile.id_station == station_id).delete(synchronize_session=False)

                # jump
                session.query(Jump).filter(Jump.id_station == station_id).delete(synchronize_session=False)

                # noise
                session.query(Noise).filter(Noise.id_station == station_id).delete(synchronize_session=False)

                # reference_position_velocities
                session.query(ReferencePositionVelocity).filter(ReferencePositionVelocity.id_station == station_id).delete(synchronize_session=False)

                # log
                session.query(Log).filter(Log.id_station == station_id).delete(synchronize_session=False)

                # user_group_station
                session.query(UserGroupStation).filter(UserGroupStation.id_station == station_id).delete(synchronize_session=False)

                # seasonal_signal
                session.query(SeasonalSignal).filter(SeasonalSignal.id_station == station_id).delete(synchronize_session=False)

                # queries
                session.query(Query).filter(Query.station_id == station_id).delete(synchronize_session=False)

                # local_ties
                session.query(LocalTie).filter(LocalTie.id_station == station_id).delete(synchronize_session=False)

                # station
                session.query(Station).filter(Station.id == station_id).delete(synchronize_session=False)

                session.commit()

            except Exception as e:
                status_code = INTERNAL_SERVER_ERROR
                result = str(e)
                return result, status_code

        status_code = OK
        msg = "station_single_delete: station [{}] has been deleted" \
            .format(name_or_id)
        self.logger.info(msg)
        result = msg
        return result, status_code

    def station_post(self, body):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code

    def sitelog_single_get(self, marker_id=None):
        '''
        This is a doc string
        '''

        ## Preset result
        result = None
        status_code = NO_CONTENT

        self.logger.info('sitelog_station_single() was called with marker or station_id: {}'.format(marker_id))

        id_station = None
        marker = None
        query_list = None

        #Station identifier from http-input parameters
        if marker_id:
            if marker_id.isdigit():
                self.logger.info('marker_id is TYPE "digit"')
                id_station = int(marker_id)
            else:
                self.logger.info('marker_id is TYPE "marker"')
                marker = marker_id.upper()
        else:
            self.logger.info('marker_id: {} -> value is missing'.format(marker_id))


        # 1) First we create the station part of the sitelog JSON and save that in the response dict
        with closing(self.session()) as session:

            self.logger.info('Building station JSON')

            # .join(StationContact) \
            #     .join(Contact) \
            #.join(Agency) \
            # .join(StationNetwork) \
            #     .join(Network) \

            # q = session.query(Station, Location, Coordinates, \
            #                               City, State, Country, Tectonic, Geological, Bedrock, \
            #                               StationType, Monument) \
            #     .join(Location) \
            #     .join(Monument) \
            #     .join(StationType) \
            #     .join(Coordinates) \
            #     .join(City) \
            #     .join(State) \
            #     .join(Country) \
            #     .join(Geological) \
            #     .join(Tectonic) \
            #     .join(Bedrock) \
            #     .filter(or_(Station.marker == marker, Station.id == id_station))
            # a=str(q)

            try:
                station_query = session.query(Station, Location, Coordinates,\
                                    City, State, Country, Tectonic, Geological, Bedrock,\
                                    StationType, Monument)\
                                    .join(Location)\
                                    .join(Monument)\
                                    .join(StationType)\
                                    .join(Coordinates)\
                                    .join(City)\
                                    .join(State)\
                                    .join(Country)\
                                    .join(Geological)\
                                    .join(Tectonic)\
                                    .join(Bedrock)\
                                    .filter(or_(Station.marker == marker, Station.id == id_station)) \
                                    .one_or_none()
            except MultipleResultsFound:
                status = 'More then one station found with marker: [{}]. I do not know which station return' \
                    .format(marker)
                self.logger.error(status)
                status_code = CONFLICT
                result = json.dumps({'error': status})
                return result, status_code

            if not station_query:
                status = 'station with marker: [{}] NOT FOUND' \
                    .format(marker)
                self.logger.error(status)
                result = json.dumps({'error': status})
                status_code = NOT_FOUND
                return result, status_code

            self.logger.info('Length of station query result list: {0}'.format(len(station_query )))
            #self.logger.info('Query result list: {0}'.format(station_query))

            ### Define a dict to collect the dictified models
            station_dict = {}

            ### Warp the model object to a dict
            for station_query_object in station_query:
                # FIXME: why this is needed has to be explained. I have a theory...
                try:
                    query_dict = station_query_object.__dict__
                    query_dict.pop('_sa_instance_state')
                except:
                    pass

                station_dict[station_query_object.__tablename__] = query_dict

            ### Construct response ###
            response = {'monument_num': station_dict['station']['monument_num'],
                        'cpd_num': station_dict['station']['cpd_num'],
                        'instrument_collocation': None,
                        'comment': station_dict['station']['comment'],
                        'conditions': 'DUPLICATE ENTRY?',
                        'country': 'DUPLICATE ENTRY?',
                        'date_from': station_dict['station']['date_from'],
                        'date_to': station_dict['station']['date_to'],
                        'description': station_dict['station']['description'],
                        'geological': {
                            'bedrock': {'condition': station_dict['bedrock']['condition'],
                                        'type': station_dict['bedrock']['type']},
                            'characteristic': station_dict['geological']['characteristic'],
                            'distance_to_fault': station_dict['geological']['distance_to_fault'],
                            'fault_zone': station_dict['geological']['fault_zone'],
                            'fracture_spacing': station_dict['geological']['fracture_spacing']},
                        'iers_domes': station_dict['station']['iers_domes'],
                        'local_ties': None,
                        'location':{'city': station_dict['city']['name'],
                                    'coordinates':{
                                        'X': station_dict['coordinates']['x'],
                                        'Y': station_dict['coordinates']['y'],
                                        'Z':station_dict['coordinates']['z'],
                                        'alt': station_dict['coordinates']['altitude'],
                                        'lat': station_dict['coordinates']['lat'],
                                        'lon': station_dict['coordinates']['lon'] },
                                   'country': station_dict['country']['name'],
                                   'description': station_dict['location']['description'],
                                   'state': station_dict['state']['name'],
                                   'tectonic':{'plate_name':station_dict['tectonic']['plate_name']}},
                        'logs': None,
                        'marker': station_dict['station']['marker'],
                        'monument': {'description': station_dict['monument']['description'],
                                    'foundation': station_dict['monument']['foundation'],
                                    'foundation_depth': station_dict['monument']['foundation_depth'],
                                    'height': station_dict['monument']['height'],
                                    'monument_inscription': station_dict['monument']['inscription']},
                        'name': station_dict['station']['name'],
                        'receiver_num': station_dict['station']['receiver_num'],
                        'station_type': station_dict['station_type']['name'],
                        'station_items' : None,
                        'station_networks' : None,
                        'station_contacts' : None
                    }

        # 2) Next we build the station_contacts part of the sitelog JSON
        with closing(self.session()) as session:

            self.logger.info('Building station_contacts JSON')

            contacts_dict_list = []
            contact_list = []
            contacts_query = None

            # Next we tackle contacts            
            contacts_query = session.query(Station, StationContact, Contact, Agency)\
                            .join(StationContact)\
                            .join(Contact)\
                            .join(Agency)\
                            .filter(or_(Station.marker == marker, Station.id == id_station))\
                            .all()

            if contacts_query:
                #self.logger.info('contacts_query: {0}'.format(contacts_query))
                self.logger.info('Length of contacts_query result list: {0}'.format(len(contacts_query )))

                for contact_results in contacts_query:

                    contacts_dict = {}

                    #self.logger.info('contact_result: {0}'.format(contact_results))
                    for contact_query_object in contact_results:
                        
                        # Just resetting this dict between runs
                        query_dict = None
                        
                        try:
                            query_dict = contact_query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        contacts_dict[contact_query_object.__tablename__] = query_dict

                    contacts_dict_list.append(contacts_dict)

                
                #self.logger.info('contacts_list: {0}'.format(contacts_dict_list))

                for contact in contacts_dict_list:
                    #self.logger.info('contact: {0}'.format(contact))
                    contact_dict = None
                    contact_dict ={
                        'agency': {
                            'abbreviation': contact['agency']['abbreviation'],
                            'address': contact['agency']['address'],
                            'infos': contact['agency']['infos'],
                            'name': contact['agency']['name'],
                            'www': contact['agency']['www']
                            },
                        'comment': contact['contact']['comment'],
                        'email': contact['contact']['email'],
                        'name': contact['contact']['name'],
                        'phone': contact['contact']['phone'],
                        'role': contact['contact']['role'],
                        'title': contact['contact']['title']
                    }

                    contact_list.append(contact_dict)
            else:
                contact_list = None

        response['station_contacts'] = contact_list

        # 3) Similar as to before we buld the station_networks part of the sitelog JSON
        with closing(self.session()) as session:

            self.logger.info('Building station_networks JSON')

            networks_dict_list = []
            network_list = []
            networks_query = None

            # Next we tackle contacts            
            networks_query = session.query(Station, StationNetwork, Network)\
                            .join(StationNetwork)\
                            .join(Network)\
                            .filter(or_(Station.marker == marker, Station.id == id_station))\
                            .all()

            if networks_query:
                #self.logger.info('contacts_query: {0}'.format(contacts_query))
                self.logger.info('Length of networks_query: {0}'.format(len(networks_query )))

                for network_results in networks_query:

                    networks_dict = {}

                    #self.logger.info('network_result: {0}'.format(network_results))
                    for network_query_object in network_results:
                        
                        # Just resetting this dict between runs
                        query_dict = None
                        
                        try:
                            query_dict = network_query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        networks_dict[network_query_object.__tablename__] = query_dict

                    networks_dict_list.append(networks_dict)

                
                self.logger.info('networks_list: {0}'.format(networks_dict_list))

                for network in networks_dict_list:
                    self.logger.info('network: {0}'.format(network))
                    network_dict = None
                    network_dict ={
                        'name': network['network']['name']
                    }

                    network_list.append(network_dict)
            else:
                network_list = None
                self.logger.info('networks_query is None -> no results')

        response['station_networks'] = network_list

        # 4) Now we build the station_items part of the sitelog JSON
        with closing(self.session()) as session:

            self.logger.info('Building station_items JSON')

            items_dict_list = []
            item_list = []
            items_query = None

            # Next we tackle contacts            
            items_query = session.query(Station, StationItem, Item, ItemType, ItemAttribute, Attribute)\
                            .join(StationItem)\
                            .join(Item)\
                            .join(ItemAttribute)\
                            .join(ItemType)\
                            .join(Attribute)\
                            .filter(or_(Station.marker == marker, Station.id == id_station))\
                            .filter(ItemType.name == 'antenna')\
                            .all()

            if items_query:
                #self.logger.info('contacts_query: {0}'.format(contacts_query))
                self.logger.info('Length of items_query: {0}'.format(len(items_query )))
                #self.logger.info('items_query: {0}'.format(items_query ))

                for item_results in items_query:

                    items_dict = {}

                    #self.logger.info('item_result: {0}'.format(item_results))
                    for item_query_object in item_results:
                        
                        # Just resetting this dict between runs
                        query_dict = None
                        
                        try:
                            query_dict = item_query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        items_dict[item_query_object.__tablename__] = query_dict

                    items_dict_list.append(items_dict)

                
                #self.logger.info('items_list: {0}'.format(items_dict_list))

                for item in items_dict_list:
                    self.logger.info('item: {0}'.format(item))
                    item_dict = None
                    item_dict ={
                        'item': {
                            'comment':item['item']['comment'],
                            'date_from': item['station_item']['date_from'],
                            'date_to': item['station_item']['date_to'],
                            'item_attributes':{ 'attribute' : {
                                                    'name': item['attribute']['name']}, 
                                                'value_varchar': item['item_attribute']['value_varchar'],
                                                'value_date': item['item_attribute']['value_date'],
                                                'value_numeric': item['item_attribute']['value_numeric'],
                                                'date_from': item['item_attribute']['date_from'],
                                                'date_to': item['item_attribute']['date_to']
                                                },
                            'item_type':{
                                'name':item['item_type']['name']}
                        }
                    }   

                    item_list.append(item_dict)
            else:
                item_list = None
                self.logger.info('items_query is None -> no results')

        response['station_items'] = item_list


        #Finally finally return the response as results!
        result = json.dumps(response, cls=JSONCustomEncoder)
        status_code = OK
        return result, status_code

    @profile
    def sitelog_post(self, body):
        '''
        This method accepts a sitelog JSON-turned-dict "body"
        and attempts to index a new GNSS station with all supplied
        attributes and items.

        *** Sergio Bruni INGV ****
        This method substitute the previous sitelog_post. It has been implemented using
        the indications reported in issue N. 51
        "In sitelogs, a contact is then uniquely identified by name and email, while a separate
        function exists for inserting and updating contacts with all their additional info"
        '''

        ## Preset result
        status_code = NO_CONTENT
        station_marker = None
        station_id = None

        self.logger.info('station_post() was called')
        self.logger.info('Station marker: {}'.format(body['marker']))

        # Check first if station already exists in the database.
        #with closing(self.session()) as session:

        with closing(self.session()) as session:
            try:
                station_id = session.query(
                    Station.id) \
                    .filter(
                        and_(
                            Station.marker == body['marker'].upper(),
                            Station.monument_num == body['monument_num'],
                            Station.receiver_num == body['receiver_num'],
                            Station.country_code == body['country_code_ISO']
                        )
                    ) \
                    .scalar()

                if station_id:
                    self.logger.info(
                        ('station {} was found in the database.'
                         '').format(body['marker']))
                    station_marker, station_id, status_code =  self.sitelog_update(session, station_id, body)
                    #self.print_benchmark()
                    return station_marker, station_id, status_code
                else:
                    station_marker, station_id, status_code =  self.sitelog_insert(session, body)
                    #self.print_benchmark()
                    return station_marker, station_id, status_code

            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                self.logger.error(
                    "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                        .format(
                        fname,
                        self.__class__.__name__,
                        inspect.currentframe().f_code.co_name,
                        exc_tb.tb_lineno,
                        str(e),
                        exc_type))
                raise e

    @profile
    def sitelog_insert(self, session, body):
        try:
            # If the status_code is unchanged, then no station was found and
            # we can create one from the sitelog.
            self.logger.info(
                ('station {} was NOT found in the database. '
                 'Creating now...').format(body['marker']))

            # 1 #
            bedrock_id = self.insert_bedrock(body, session)
            geological_id = self.insert_geological(body, session, bedrock_id)

            # 2 #
            country_id, country_iso_code = self.insert_country(
                body, session)
            state_id = self.insert_state(body, session, country_id)
            city_id = self.insert_city(body, session, state_id)
            tectonic_id = self.insert_tectonic(body, session)
            coordinates_id = self.insert_coordinates(body, session)
            monument_id = self.insert_monument(body, session)
            location_id = self.insert_location(
                body, session, tectonic_id, city_id, coordinates_id)

            station_type_id = self.insert_station_type(body, session)

            # 3 #

            # Sergio Bruni (removed)
            # ======================================
            # contact_id_list = []
            #
            # for contact in body['station_contacts']:
            #     agency_id = self.insert_agency(contact, session)
            #     contact_id = self.insert_contact(contact, session, agency_id)
            #     contact_id_list.append(contact_id)
            # ======================================

            # TODO: Contact for network is not provided in sitelogs...
            contact_id = 'Not a number and never used...'
            network_id_list = self.insert_network(body, session)

            # 4 #
            station_marker, station_id = self.insert_station(
                body,
                session,
                station_type_id,
                location_id,
                geological_id,
                monument_id,
                country_iso_code,
            )

            self.insert_station_contact(body, session, station_id)

            # 5 #

            self.insert_local_tie(body, session, station_id)
            self.insert_instrument_collocation(body, session, station_id)

            self.insert_conditions(body, session, station_id)

            # Since Sitelog JSON have inadequate contact info for logs,
            # Contacts will be inserted with email = None and agency name = 'None'
            self.insert_log(body, session, station_id)

            self.insert_item(body, session, station_id)

            self.insert_station_network(session, station_id, network_id_list)

            session.commit()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error(
                "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                    .format(
                    fname,
                    self.__class__.__name__,
                    inspect.currentframe().f_code.co_name,
                    exc_tb.tb_lineno,
                    str(e),
                    exc_type))
            session.rollback()
            raise e

        status_code = CREATED

        #self.print_benchmark()

        return station_marker, station_id, status_code



    # def print_benchmark(self):
    #     if __benchmark__['active']:
    #         messages = print_prof_data()
    #         for message in messages:
    #             self.benchmark_logger.warning(message)
    #
    #     clear_prof_data()

    @profile
    def sitelog_update(self, session, station_id, body):
        curr_station = {}

        # Retrieve from db the entire sitelog structure
        # curr_station will contain all element of the Entities related to station, initialized to False
        # While those entities are updated, all already existimg elements are set to True
        # At the end of update process the elements still set to False are delete from Station
        #with closing(self.session()) as session:
        station = session.query(
                Station,
                Geological,
                Location,
                City,
                State) \
            .join(Geological) \
            .join(Location) \
            .join(City) \
            .join(State) \
            .filter(Station.id == station_id) \
            .one_or_none()

        curr_station['StationType'] = station.Station.id_station_type
        curr_station['Geological'] = station.Geological.id
        curr_station['Bedrock'] = station.Geological.id_bedrock
        curr_station['Location'] = station.Location.id
        curr_station['Tectonic'] = station.Location.id_tectonic
        curr_station['Coordinates'] = station.Location.id_coordinates
        curr_station['City'] = station.City.id
        curr_station['State'] = station.State.id
        curr_station['Country'] = station.State.id_country
        curr_station['Monument'] = station.Station.id_monument

        curr_station_contact_list = session.query(StationContact) \
            .filter(StationContact.id_station == station_id)

        # following dictionary contains all contact id linked to the station
        # through tables: network, item, log, station_contacts
        curr_station['Contact'] = {}
        for elem in curr_station_contact_list:
            curr_station['Contact'][elem.id_contact] = False


        curr_station_network_list = session.query(StationNetwork) \
            .filter(StationNetwork.id_station == station_id)

        curr_station['Network'] = {}
        for elem in curr_station_network_list:
            curr_station['Network'][elem.id_network] = False

        station_log_list = session.query(Log) \
            .filter(Log.id_station == station_id)

        curr_station['Log'] = {}
        curr_station['LogType'] = {}
        curr_station['LogContact'] = {}
        for elem in station_log_list:
            curr_station['Log'][elem.id] = False
            if not elem.id_log_type in curr_station['LogType']:
                curr_station['LogType'][elem.id_log_type] = False
            if elem.id_contact and\
                    not elem.id_contact in curr_station['LogContact']:
                curr_station['LogContact'][elem.id_contact] = False

        station_item_list = session.query(StationItem, Item) \
            .join(Item) \
            .filter(StationItem.id_station == station_id)

        curr_station['Item'] = {}
        curr_station['ItemContact'] = {}
        for elem in station_item_list:
            if elem.Item.id_contact_as_owner and \
                    not elem.Item.id_contact_as_owner in curr_station['ItemContact']:
                curr_station['ItemContact'][ elem.Item.id_contact_as_owner] = False
            if elem.Item.id_contact_as_producer \
                    and not elem.Item.id_contact_as_producer in curr_station['ItemContact']:
                curr_station['ItemContact'][elem.Item.id_contact_as_producer] = False

            curr_station['Item'][elem.Item.id] = False

        station_localtie_list = session.query(LocalTie) \
            .filter(LocalTie.id_station == station_id)

        curr_station['LocalTie'] = {}
        for elem in station_localtie_list:
            curr_station['LocalTie'][elem.id] = False

        station_instrumentcolocation_list = session.query(InstrumentColocation) \
            .filter(InstrumentColocation.id_station == station_id)

        curr_station['InstrumentColocation'] = {}
        for elem in station_instrumentcolocation_list:
            curr_station['InstrumentColocation'][elem.id] = False

        station_condition_list = session.query(Condition) \
            .filter(Condition.id_station == station_id)

        curr_station['Effect'] = {}
        for elem in station_condition_list:
            if not elem.id_effect in curr_station['Effect']:
                curr_station['Effect'][elem.id_effect] = False

        bedrock_id = self.insert_bedrock(body, session)

        geological_id = self.insert_geological(body, session, bedrock_id)

        if curr_station['Bedrock'] != bedrock_id:
            # found bugs in insertion: when new bedrock is created and Geolocical already exixts
            # It is not updated with new bedrock id
            # On the other hands changing the bedrock_id of the Geological will reflect to all others
            # stations linked to this Geological ...
            if geological_id == curr_station['Geological']:
                session.query(Geological).filter_by(id=geological_id).update({"id_bedrock": bedrock_id})

            # delete old bedrock if it has no more link
            if session.query(Geological).filter(Geological.id_bedrock == curr_station['Bedrock']).count() == 0:
                 session.query(Bedrock).filter_by(id=curr_station['Bedrock']).delete(synchronize_session=False)

        # 2 # Country, State, City, Coordinates, Tectonic, Location, Monument

        country_id, country_iso_code = self.insert_country(body, session)
        state_id = self.insert_state(body, session, country_id)

        if country_id != curr_station['Country']:
            if session.query(State).filter(State.id_country == curr_station['Country']).count() == 0:
                 session.query(Country).filter_by(id=curr_station['Country']).delete(synchronize_session=False)

        city_id = self.insert_city(body, session, state_id)

        if state_id != curr_station['State']:
            # found bugs in insertion: when new state is created and City already exixts
            # It is not updated with new state id
            # On the other hands changing the state_id of the city will reflect to all others
            # location linked to this city ...
            if city_id == curr_station['City']:
                session.query(City).filter_by(id=city_id).update({"id_state": state_id})
            if session.query(City).filter(City.id_state == curr_station['State']).count() == 0:
                 session.query(State).filter_by(id=curr_station['State']).delete(synchronize_session=False)

        tectonic_id = self.insert_tectonic(body, session)
        coordinates_id = self.insert_coordinates(body, session)
        location_id = self.insert_location(body, session, tectonic_id, city_id, coordinates_id)

        monument_id = self.insert_monument(body, session)
        # In case the monument already exist
        # the field inscription should be updated, but, for now, I'm not sure (is an open issue)

        station_type_id = self.insert_station_type(body, session)
        # In case the station_type already exist
        # the field name should be updated, but, for now, I'm not sure (is an open issue)

        # 3 #
        self.update_station(
            body,
            session,
            station_id,
            station_type_id,
            location_id,
            geological_id,
            monument_id,
            country_iso_code,
        )

        # 4
        # Station link to Monument, StationType, Geological and  Location
        # If the foreign-keys are changed, following check are neede to delete
        # elements with no more stations linked to them
        if monument_id != curr_station['Monument']:
            if session.query(Station).filter(Station.id_monument == curr_station['Monument']).count() == 0:
                 session.query(Monument).filter_by(id=curr_station['Monument']).delete(synchronize_session=False)

        if station_type_id != curr_station['StationType']:
            if session.query(Station).filter(Station.id_station_type == curr_station['StationType']).count() == 0:
                 session.query(StationType).filter_by(id=curr_station['StationType']).delete(synchronize_session=False)

        # Geological is referenced only by Station
        if geological_id != curr_station['Geological']:
            if session.query(Station).filter(Station.id_geological == curr_station['Geological']).count() == 0:
                session.query(Geological).filter_by(id=curr_station['Geological']).delete(synchronize_session=False)
                # Bedrock is referenced only by Geological
                if session.query(Geological).filter(Geological.id_bedrock == curr_station['Bedrock']).count() == 0:
                     session.query(Bedrock).filter_by(id=curr_station['Bedrock']).delete(synchronize_session=False)

        # Location is referenced only by Station
        if location_id != curr_station['Location']:
            if session.query(Station).filter(Station.id_location == curr_station['Location']).count() == 0:
                session.query(Location).filter_by(id=curr_station['Location']).delete(synchronize_session=False)
                # City, Coordinates and Tectonic are referenced only by Location
                if session.query(Location).filter(Location.id_city == curr_station['City']).count() == 0:
                    session.query(City).filter_by(id=curr_station['City']).delete(synchronize_session=False)
                    # State is referenced only by City
                    if session.query(City).filter(City.id_state == curr_station['State']).count() == 0:
                        session.query(State).filter_by(id=curr_station['State']).delete(synchronize_session=False)
                        # Country is referenced by State
                        if session.query(State).filter(State.id_country == curr_station['Country']).count() == 0:
                             session.query(Country).filter_by(id=curr_station['Country']).delete(synchronize_session=False)

                if session.query(Location).filter(Location.id_coordinates == curr_station['Coordinates']).count() == 0:
                     session.query(Coordinates).filter_by(id = curr_station['Coordinates']).delete(synchronize_session=False)
                if session.query(Location).filter(Location.id_tectonic == curr_station['Tectonic']).count() == 0:
                     session.query(Tectonic).filter_by(id = curr_station['Tectonic']).delete(synchronize_session=False)

        # 6 Update station contacts
        #
        self.update_station_contact(body, session, station_id, curr_station['Contact'])
        # unlink contacts that don't belong to the station anymore
        del_list = list((k) for k,v in curr_station['Contact'].items() if not v)
        if del_list:
            session.query(StationContact).filter(
                and_(StationContact.id_station == station_id,
                     StationContact.id_contact.in_(del_list))).delete(synchronize_session=False)

        # 7 Log
        self.update_log(body, session, station_id,
                curr_station['Log'], curr_station['LogType'], curr_station['LogContact'])

        # check for Log to be deleted
        del_list = list((k) for k,v in curr_station['Log'].items() if not v)
        if del_list:
            session.query(Log).filter(Log.id.in_(del_list)).delete(synchronize_session=False)

        # check for logType to be deleted
        del_list = list((k) for k, v in curr_station['LogType'].items() if not v)
        for id_log_type in del_list:
            if session.query(Log).filter(Log.id_log_type == id_log_type).count() == 0:
                session.query(LogType).filter(LogType.id==id_log_type).delete(synchronize_session=False)

        # 8 NETWORK
        self.update_station_network(body, session, station_id,
                               curr_station['Network'])

        # unlink network that don't belong to the station anymore
        del_list = list((k) for k, v in curr_station['Network'].items() if not v)
        if del_list:
            session.query(StationNetwork).filter(
                and_(StationNetwork.id_station == station_id,
                     StationNetwork.id_network.in_(del_list))).delete(synchronize_session=False)
            # delete networks with not any linked station
            for id_network in del_list:
                if session.query(StationNetwork).filter(StationNetwork.id_network == id_network).count() == 0:
                    session.query(Network).filter(id == id_network).delete(synchronize_session=False)

            # here should be considered all contacts linked by delete networks
            # to check if they are not linked from anyone and if so deleted
            # but, for now, networks have no linked contact so ... (it's a subject of discussion)

        # 9 Item
            # decided, for speed purpose, to reuse insert function
            # in this way all item will be renewed and current items deleted
        self.insert_item(body, session, station_id)

            # delete all item not referenced from any station
            # It needs a trigger that before row is delete from  ItemAttribute
            # delete the evenctually filter table that reference to It
        del_list = list((k) for k, v in curr_station['Item'].items() if not v)
        if del_list:
            session.query(StationItem).filter(
                and_(StationItem.id_station == station_id,
                     StationItem.id_item.in_(del_list))).delete(synchronize_session=False)

            for id_item in del_list:
                if session.query(StationItem).filter(StationItem.id_item == id_item).count()==0:
                    session.query(ItemAttribute).filter(
                        ItemAttribute.id_item==id_item).delete(synchronize_session=False)
                    session.query(Item).filter(
                        Item.id==id_item).delete(synchronize_session=False)

        # 10 LocalTie
        self.update_local_tie(body, session, station_id, curr_station['LocalTie'])
        del_list = list((k) for k, v in curr_station['LocalTie'].items() if not v)
        if del_list:
           session.query(LocalTie).filter(
                LocalTie.id.in_(del_list)).delete(synchronize_session=False)

        # 11 InstrumentColocation
        self.update_instrument_colocation(body, session, station_id, curr_station['InstrumentColocation'])
        del_list = list((k) for k, v in curr_station['InstrumentColocation'].items() if not v)
        if del_list:
            session.query(InstrumentColocation).filter(
                InstrumentColocation.id.in_(del_list)).delete(synchronize_session=False)

        # 12 Condition
        self.update_conditions(body, session, station_id, curr_station['Effect'])
        session.query(Condition).filter(
            Condition.id_station==station_id).delete(synchronize_session=False)
        del_list = list((k) for k, v in curr_station['Effect'].items() if not v)
        for id_effect in del_list:
            if session.query(Condition).filter(Condition.id_effect == id_effect).count() == 0:
                session.query(Effect).filter(
                    Effect.id == id_effect).delete(synchronize_session=False)

        # delete all contact with no anymore reference
        del_list_station_contact = list((k) for k, v in curr_station['Contact'].items() if not v)
        del_list_log_contact = list((k) for k, v in curr_station['LogContact'].items() if not v)
        del_list_item_contact = list((k) for k, v in curr_station['ItemContact'].items() if not v)

        # merge three list to one, who contains all items candidate to be removed
        # if they are not referenced anymore
        del_list = list(set(del_list_station_contact + del_list_log_contact + del_list_item_contact))
        for id_contact in del_list:
             if session.query(StationContact).filter(StationContact.id_contact == id_contact).count() + \
                    session.query(Item).filter(Item.id_contact_as_owner == id_contact).count() + \
                    session.query(Item).filter(Item.id_contact_as_producer == id_contact).count() + \
                    session.query(Log).filter(Log.id_contact == id_contact).count() + \
                    session.query(Network).filter(Network.id_contact == id_contact).count() == 0:
                id_agency = session.query(Contact.id_agency).filter(Contact.id == id_contact).scalar()
                session.query(Contact).filter_by(id = id_contact).delete(synchronize_session=False)
                if session.query(Contact).filter(Contact.id_agency == id_agency).count() == 0:
                    session.query(Agency).filter_by(id = id_agency).delete(synchronize_session=False)
        try:
            session.commit()
        except Exception as e:
            session.rollback()
            raise e

        status_code = OK
        return body['marker'], station_id, status_code


#-------------------

    def data_rinex_post(self, body):
        # INGV 24/03/2023
        self.logger.info('data_rinex_post() was called')
        # Check mandatory fields
        status_ar = []
        for key in ('file_name'
                    , 'relative_path'
                    , 'reference_date'
                    , 'station_marker'
                    , 'data_center'
                    , 'file_type'
                    , 'sampling_window'
                    , 'sampling_frequency'
                    , 'md5_checksum'
                    , 'revision_date'
                    , "creation_date"
                    ):
            if not key in body or not body[key]:
                # Define return status message
                status_ar.append(f"{key}")

            if status_ar:
                status = f"Missing body mandatory keys: {', '.join(status_ar)}"
                self.logger.error(status)
                self.logger.error(body)
                # Update status code
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

        if body['sampling_window'] in ('01D', '01d', '1D', '1d', '24H'):
            body['sampling_window'] = '24h'
            self.logger.info(f"""[{body['file_name']}] SAMPLING_WINDOW RECEIVED id [{body['sampling_window']}] HAS BEEN CONVERTED TO [24h]""")

        self.logger.info(f"[{body['file_name']}] JSON body looks like this: {body}")

        self.rinex_filename = body['file_name']

        # retrieve needed parameters to update or reset the rinex_file table: station_id, datacente_id, file_type
        with closing(self.session()) as session:
            try:
                station_id = session.query(Station.id) \
                    .filter(Station.marker == body['station_marker']) \
                    .scalar()
            except MultipleResultsFound:
                status = f"""[{body['file_name']}]. Found multiple records with the same marker: [{body['station_marker']}]."""
                self.logger.error(status)
                status_code = CONFLICT
                self.rinex_filename = None
                return status, status_code

            if not station_id:
                # Define return status message
                status = f"""[{body['file_name']}] Station with marker "{body['station_marker']}" was not found. 
                Station must be registered in the database before files can be indexed."""
                self.logger.error(status)
                # Update status code
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

#-
            data_center_id = session.query(DataCenter.id) \
                .filter(DataCenter.acronym == body['data_center']) \
                .scalar()

            if not data_center_id:
                status = f"""[{body['file_name']}] Data center with acronym: "{body['data_center']}" not in database.
                Please contact the DGW, the data_center information should be inserted by the DGW."""
                self.logger.error(status)
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

#-
            file_type_id = session.query(FileType.id) \
                .filter(and_(FileType.format == body['file_type'],
                             FileType.sampling_window == body['sampling_window'],
                             FileType.sampling_frequency == body['sampling_frequency'])) \
                .scalar()

            if not file_type_id:
                status = f"""[{body['file_name']}] File_type "{body['file_type']}" not in database.
                Please contact the DGW, the file_type information should be inserted by the DGW."""
                self.logger.error(status)
                status_code = INTERNAL_SERVER_ERROR
                self.rinex_filename = None
                return status, status_code

            # We have a data center registered, so ... still continue
            self.logger.info(f"""[{body['file_name']}] found in DB with Data center: "{body['data_center']}" ID: {data_center_id} - File_type "{body['file_type']}".""")

#---

            #   1^ case: query rinex_file with the same md5
            try:
                rinex_file = session.query(RinexFile, DataCenterStructure) \
                    .join(DataCenterStructure) \
                    .filter(RinexFile.md5checksum == body['md5_checksum']) \
                    .one_or_none()
            except MultipleResultsFound:
                status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" result duplicated inside the database"""
                self.logger.error(status)
                status_code = CONFLICT
                self.rinex_filename = None
                return status, status_code

            if rinex_file:# 1^case condition: from query(RinexFile, DataCenterStructure), with same md5
                # check if the rinex_file (identified by name, type and data_center) is unique
                if (session.query(RinexFile) \
                        .join(DataCenterStructure) \
                        .filter(and_(
                            RinexFile.name == body['file_name'],
                            RinexFile.id_file_type == file_type_id,
                            DataCenterStructure.id_data_center == data_center_id,
                            RinexFile.md5checksum != body['md5_checksum']
                        )).count() > 0):  # TESTED
                    status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" 
                    file_name+file_type+data_center must be unique. You are trying to set it to {body['file_name']} - {body['sampling_window']} - {body['sampling_frequency']}, but these values already exist for file with another md5checksum"""
                    self.logger.error(status)
                    status_code = CONFLICT
                    self.rinex_filename = None
                    return status, status_code

                # initialize status
                status = f"""[{body['file_name']}] Rinex file was found by id: {rinex_file.RinexFile.id}. 
                Nothing seems changed. File has not been updated!"""
                status_code = NOT_MODIFIED

                #---
                # cases which require the reset of rinex_file
                force_reset_update_rinex = False
                if (station_id != rinex_file.RinexFile.id_station):
                    status = f"""Station_Id is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                if (file_type_id != rinex_file.RinexFile.id_file_type):
                    status = f"""File_type_id is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                if (datetime.strptime(body['reference_date'], '%Y-%m-%d') != rinex_file.RinexFile.reference_date): # TESTED
                    status = f"""Reference_date is different on rinex_file table, forcing the reset/update"""
                    self.logger.info(status)
                    force_reset_update_rinex = True

                #--- check dates if creation and/or revision date are NULL or if different from the body
                # and initializes the rinex_file.creation_date and rinex_file.revision_date
                # which are not changed by reset or update
                force_update_rinex = self.check_creation_revision_date(body, rinex_file.RinexFile) # TESTED

                if force_reset_update_rinex:
                    status, status_code = self.reset_rinex_file(body, session, rinex_file.RinexFile,  station_id, data_center_id, file_type_id)
                    self.rinex_filename = None
                    return status, status_code


                if (body['file_name'] != rinex_file.RinexFile.name): # TESTED
                    status = f"""[{body['file_name']}] Setting new file_name from body, found [{rinex_file.RinexFile.name}] in DB"""
                    self.logger.info(status)
                    force_update_rinex = True

                if (data_center_id != rinex_file.DataCenterStructure.id_data_center):   # TESTED
                    status = f"""[{body['file_name']}] Setting new data_center from body, found [{body['data_center']}] in body"""
                    try:
                        data_center_structure_id = session.query(DataCenterStructure.id)\
                            .join(DataCenter)\
                            .filter(and_(
                                (DataCenter.id == data_center_id),
                                (DataCenterStructure.id_file_type == file_type_id)))\
                            .scalar()
                    except MultipleResultsFound as e:
                        status = f"""[{body['file_name']}] md5checksum "{body['md5_checksum']}" DataCenterStructure multiple entry"""
                        self.logger.error(status)
                        status_code = CONFLICT
                        self.rinex_filename = None
                        return status, status_code
                    self.logger.info(status)
                    force_update_rinex = True

                if (body['relative_path'] != rinex_file.RinexFile.relative_path): # TESTED
                    status = f"""[{body['file_name']}] Setting relative_path from body, found [{body['relative_path']}] in body"""
                    self.logger.info(status)
                    force_update_rinex = True

                # if one of them has changed, then just update, but keep status  and don't delete file
                if force_update_rinex:
                    self.logger.info(f"""[{body['file_name']}] Updating rinex_file information""")
                    self.update_rinex_file(body, session, rinex_file.RinexFile, station_id, data_center_id, file_type_id, rinex_file.RinexFile.status)
                    session.flush()
                    session.commit()
                    status_code = ACCEPTED # INDICATES TO NOT RUN THE QC


            else:  # 2^ case: rinex_file w/ DIFFERENT MD5, retrieve by file_name
                try:
                    rinex_file = session.query(RinexFile) \
                        .join(DataCenterStructure) \
                        .filter(and_(
                        RinexFile.name == body['file_name'],
                        RinexFile.id_file_type == file_type_id,
                        DataCenterStructure.id_data_center == data_center_id
                    )).one_or_none()
                except MultipleResultsFound:
                    status = f"""More then one rinex file with name: [{body['file_name']}] type: "{body['file_type']}
                    +{body['sampling_window']}+{body['sampling_frequency']}" and data center: {body['data_center']} exist on DB ..."""
                    self.logger.error(status)
                    status_code = CONFLICT
                    self.rinex_filename = None
                    return status, status_code

                if rinex_file:
                    #--- check dates if creation and/or revision date are NULL or if different from the body
                    # and initialize the rinex_file.creation_date and rinex_file.revision_date
                    force_update_rinex = self.check_creation_revision_date(body, rinex_file) # TESTED
                    status, status_code = self.reset_rinex_file(body, session, rinex_file, station_id, data_center_id, file_type_id)
                    # self.rinex_filename = None
                    # return status, status_code

                else: # 3^ case: create a new db entry for rinex file
                    self.logger.info(f"""[{body['file_name']}] Inserting new rinex file record in the database""")
                    status, status_code = self.insert_rinex_file(body, session, station_id, data_center_id, file_type_id)

        self.rinex_filename = None
        return status, status_code




    def check_creation_revision_date(self, body, rinex_file):
        force_update_rinex = False
        if (type(rinex_file.revision_date) is datetime):
            revision_date_db = rinex_file.revision_date
        else:
            revision_date_db = None
            self.logger.info('RinexFile.revision_date is NULL')

        if (type(rinex_file.creation_date) is datetime):
            creation_date_db = rinex_file.creation_date
        else:
            creation_date_db = None
            self.logger.info('RinexFile.creation_date is NULL')

        # Grouped NULL cases for revision_date and creation_date

        if (revision_date_db == None) and (creation_date_db == None):
            rinex_file.creation_date = datetime.strptime(body['creation_date'], '%Y-%m-%d %H:%M:%S')
            rinex_file.revision_date = datetime.strptime(body['revision_date'], '%Y-%m-%d %H:%M:%S')
            status = f"""Setting creation_date and revision_date as from the json: [{body['creation_date']}, {body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        elif (revision_date_db != None) and (creation_date_db == None):
            rinex_file.creation_date = revision_date_db  # TESTED!!!
            rinex_file.revision_date = datetime.strptime(body['revision_date'], '%Y-%m-%d %H:%M:%S')
            status = f"""Setting creation_date = revision_date_db [{revision_date_db}], and revision_date from json [{body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        elif (revision_date_db == None) and (creation_date_db != None):
            rinex_file.revision_date = datetime.strptime(body['revision_date'], '%Y-%m-%d %H:%M:%S')
            status = f"""Setting revision_date = body(revision_date) [{revision_date_db}], and revision_date from json [{body['revision_date']}]"""
            self.logger.info(status)
            force_update_rinex = True

        # think again about it ;)
        if (revision_date_db != None) and (datetime.strptime(body['revision_date'], '%Y-%m-%d %H:%M:%S') != revision_date_db):
            rinex_file.revision_date = datetime.strptime(body['revision_date'], '%Y-%m-%d %H:%M:%S')
            status = f"""Setting revision_date same as body: {rinex_file.revision_date}"""
            self.logger.info(status)
            force_update_rinex = True

        return force_update_rinex
#        return rinex_file, force_update_rinex




#-------------------------

    ## delete all QC data related to a specific rinex file
    def deleteRinexFile(self, session, RF):

        # quality file (this table is not contemplated in on inserting QC data)
        session.query(QualityFile).filter_by(id_rinexfile=RF.id).delete(synchronize_session=False)

        # node_rinex_file
        # cannot delete from this table because has not been implemented on dataModel (WHY ?)

        self.delete_QcReportSummary(session, RF.id)

        session.query(RinexFile).filter_by(id=RF.id).delete(synchronize_session=False)


    def data_rinex_station_get(self, args):
        '''
        Returns a list of rinex files for a given station by marker/id and
        limited by passed arguments.
        
        Required: <station_identifier> = station ID (4/9 char marker name)
        Optional query parameters:
        date_from = start date
        date_to   = end date
        status    = RINEX file status number in DB
        '''

        ## Preset result
        result = None
        status_code = NO_CONTENT

        self.logger.info(('data_rinex_station_get() was called with '
                          'arguments {}').format(args))

        # DEFAULT values:
        id_station = None
        marker = None

        # Station identifier from http-input parameters
        if 'station_id' in args and args['station_id']:
            if args['station_id'].isdigit():
                self.logger.info('marker_id is TYPE "digit"')
                id_station = int(args['station_id'])
            else:
                self.logger.info('marker_id is TYPE "marker"')
                marker = args['station_id'].upper()
        else:
            self.logger.info('marker_id: -> value is missing')
            result = json.dumps({'message':'WARNING: No station ID given!'})
            return result, OK

        # Execute sqlAlchemy query
        with closing(self.session()) as session:
            
            # Collect criteria as tuple of SQLAlchemy filters:
            filters = ()
            if id_station:
                filters += (Station.id == id_station,)
            if marker:
                filters += (Station.marker == marker,)
            if 'date_from' in args and args['date_from']:
                filters += (RinexFile.reference_date >= args['date_from'],)
            if 'date_to' in args and args['date_to']:
                filters += (RinexFile.reference_date <= args['date_to'],)
            if 'status' in args and args['status']:
                filters += (RinexFile.status == args['status'],)

            query_list = session.query(
                RinexFile,
                Station,
                FileType,
                DataCenterStructure,
                DataCenter,
                )\
                    .join(Station)\
                    .join(FileType)\
                    .join(DataCenterStructure)\
                    .join(DataCenter)\
                    .filter(*filters)\
                    .order_by(RinexFile.name)\
                    .all()
        #
        if not query_list:
            # No result, no RINEX file
            msg = 'Result: No RINEX file matches criteria'
            self.logger.info(msg)
            result = json.dumps({})
            return result, OK
        # Log
        self.logger.info(('Length of query result list: '
            '{0}').format(len(query_list)))

        ### Turn list of tuples with table objects to list of dicts
        response_list = []
        for query_tuple in query_list:
            md = {}
            ### Turn table objects to dicts
            for query_object in query_tuple:
                md[query_object.__tablename__] = row2dict(query_object)

            ### Construct response ###
            rf = md['rinex_file']
            ft = md['file_type']
            dcs = md['data_center_structure']
            dc = md['data_center']
            response = {
                'id' : rf['id'],
                'station_marker' : md['station']['marker'],
                'station_name':  md['station']['name'],
                'file_name' : rf['name'],
                'file_type' : ft['format'],
                'sampling_window' : ft['sampling_window'],
                'sampling_frequency' : ft['sampling_frequency'],
                'data_center': dc['acronym'],
                'protocol': dc['protocol'],
                'hostname': dc['hostname'],
                'root_path': dc['root_path'],
                'directory_naming': dcs['directory_naming'],
                'relative_path' : rf['relative_path'],
                'reference_date': rf['reference_date'],
                'creation_date': rf['creation_date'],
                'published_date': rf['published_date'],
                'file_size' : rf['file_size'],
                'md5_checksum': rf['md5checksum'],
                'md5_uncompressed': rf['md5uncompressed'],
                'status' : rf['status'],
                }

            response_list.append(response)

        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def data_rinex_file_get(self, marker_id=None, file_id=None):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code

    def agency_list_get(self,args=None):
        '''
        This is a doc string
        '''

        self.logger.info('agency_list_get() was called')

        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:

            ### Execute query ###
            self.logger.info('Just before the query...')

            query_list = session.query(Agency)\
                    .all()

            if query_list:

                self.logger.info('agency_query_list: {0}'.format(query_list))

                ### Define a dict to collect the dictified models
                model_dicts = {'id':None,
                               'name' : None,
                               'abbrevation' : None,
                               'address' : None,
                               'www' : None,
                               'infos' : None
                             }


                ### Loop through each collection of tables for every station
                for query in query_list:

                    ### Warp the model object to a dict
                    #for query_object in query:
                        # FIXME: why this is needed has to be explained. I have a theory...
                    try:
                        query_dict = query.__dict__
                        query_dict.pop('_sa_instance_state')
                    except:
                        pass

                    model_dicts[query.__tablename__] = query_dict

                    ### Construct response ###
                    response = {
                        
                        'id' : model_dicts['agency']['id'],
                        'name' : model_dicts['agency']['name'],
                        'abbreviation' : model_dicts['agency']['abbreviation'],
                        'address' : model_dicts['agency']['address'],
                        'www' : model_dicts['agency']['www'],
                        'infos' : model_dicts['agency']['infos']
                        }

                    response_list.append(response)

            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def datacenter_list_get(self,args=None):
        '''
        This is a doc string
        '''

        self.logger.info('data_center_list_get() was called')

        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:

            ### Execute query ###
            self.logger.info('Just before the query...')
            # query_list = session.query(FileType, DataCenterStructure,DataCenter, Agency).join(DataCenterStructure).join(DataCenter).join(Agency).all() # need DataCenterStructure.id_file_type to be a key to  FileType.id
            query_list = session.query(FileType, DataCenterStructure,DataCenter, Agency).join(DataCenterStructure, FileType.id == DataCenterStructure.id_file_type).join(DataCenter).join(Agency).all()

            if query_list:
                self.logger.info('data_center_query_list: {0}'.format(query_list))

                ### Define a dict to collect the dictified models
                model_dicts = {'agency': {'abbreviation': None},
                               'agency_id': None,
                               'acronym': None,
                               'protocol': None,
                               'name': None,
                               'hostname': None,
                               'root_path': None,
                               'directory_structure': {
                                   'format':None,
                                   'sampling_window': None,
                                   'sampling_frequency': None,
                                   'directory_naming': None},
                               }



                ### Loop through each collection of tables for every station
                for query in query_list:
                    # print 10*'#'
                    # for i in  query:
                    #     print vars(i)
                    ### Warp the model object to a dict
                    for query_object in query:
                        # FIXME: why this is needed has to be explained. I have a theory...
                        self.logger.info('data_center_query_object: {0}'.format(query_object))
                        try:
                            query_dict = query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        print(query_dict)

                        model_dicts[query_object.__tablename__] = query_dict

                    ### Construct response ###
                    response={
                        'agency': {
                            'abbreviation': model_dicts['agency']['abbreviation']
                        },

                        'hostname':  model_dicts['data_center']['hostname'],
                        'acronym':  model_dicts['data_center']['acronym'],
                        'protocol':  model_dicts['data_center']['protocol'],
                        'root_path':  model_dicts['data_center']['root_path'],
                        'name':  model_dicts['data_center']['name'],
                        'directory_structure': {
                            'format': model_dicts['file_type']['format'],
                            'sampling_window': model_dicts['file_type']['sampling_window'],
                            'sampling_frequency': model_dicts['file_type']['sampling_frequency'],
                            'directory_naming': model_dicts['data_center_structure']['directory_naming']
                        }
                    }

                    response_list.append(response)
            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def data_center_post(self, body):

        self.logger.info('data_center_post() was called')
        self.logger.info('JSON body looks like this: {0}'.format(body))

        # Otherwise create new data center:
        with closing(self.session()) as session:
            status_code = self.insert_datacenter(body,session)

        return self.get_json_msg(), status_code


    ### Sitelog POST oriented functions ###

    def insert_bedrock(self, body, session):

        try:
            #### Insert Bedrock info ####
            #self.logger.info('Checking bedrock information.')
            bedrock = Bedrock()
            bedrock_id = None

            ### 1 ### Fill N/A in for none
            if body['geological']['bedrock']['condition']:
                bedrock_condition = body['geological']['bedrock']['condition'].lower()
            else:
                bedrock_condition = 'None'

            if body['geological']['bedrock']['type']:
                bedrock_type = body['geological']['bedrock']['type'].lower()
            else:
                bedrock_type = 'None'
                # TODO: Check if string 'None' should really be indexed!

            ### 2 ### Check if bedrock description exists and create one if not.
            bedrock_id = session.query(
                Bedrock.id)\
                    .filter(and_(
                        Bedrock.condition == bedrock_condition,
                        Bedrock.type == bedrock_type))\
                            .one_or_none()

            if bedrock_id:
                bedrock_id = bedrock_id[0]
                self.logger.info('Bedrock information was found with id: {}'.format(bedrock_id))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Bedrock information not found.. Creating one now.')
                bedrock.condition = bedrock_condition
                bedrock.type = bedrock_type
                session.add(bedrock)
                session.flush()

                # Get the id for the new bedrock description.
                # Will be stored in the geological table.
                bedrock_id = bedrock.id
                self.logger.info('Bedrock id: {}'.format(bedrock.id))

            return bedrock_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_geological(self, body, session, bedrock_id):
        try:
            #### Insert Geological info ####
            self.logger.info('Checking geological information')
            geological = Geological()
            geological_id = None

            ### 1 ### Fill N/A in for none
            # TODO: Check if string 'None' should really be indexed!
            if body['geological']['characteristic']:
                geological_characteristic = body['geological']['characteristic'].lower()
            else:
                geological_characteristic = 'None'

            if body['geological']['fracture_spacing']:
                geological_fracture_spacing = body['geological']['fracture_spacing'].lower()
            else:
                geological_fracture_spacing = 'None'

            if body['geological']['fault_zone']:
                geological_fault_zone = body['geological']['fault_zone'].lower()
            else:
                geological_fault_zone = 'None'

            if body['geological']['distance_to_fault']:
                geological_distance_to_fault = body['geological']['distance_to_fault'].lower()
            else:
                geological_distance_to_fault = 'None'

            ### 2 ### Check if geological description exists and create one if not.
            geological_id = session.query(Geological.id)\
                          .filter(and_(Geological.characteristic == geological_characteristic,
                            Geological.fracture_spacing == geological_fracture_spacing,
                            Geological.fault_zone == geological_fault_zone,
                            Geological.distance_to_fault == geological_distance_to_fault))\
                          .one_or_none()

            if geological_id:
                geological_id = geological_id[0]
                self.logger.info('Geological description was found: {}'.format(geological_id))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Geological description not found: {}'.format(geological_id))
                geological.id_bedrock = bedrock_id
                geological.characteristic = geological_characteristic
                geological.fracture_spacing = geological_fracture_spacing
                geological.fault_zone = geological_fault_zone
                geological.distance_to_fault = geological_distance_to_fault
                session.add(geological)
                session.flush()

                # Get the id for the new geological description.
                # Will be stored in the station table.
                geological_id = geological.id
                self.logger.info('Geological id: {}'.format(geological.id))

            return geological_id

        except Exception as e:
            session.rollback()
            raise e


    def insert_country(self, body, session):

        try:
            #### Insert country info ####
            country = Country()
            country_id = None

            ### 1 ### Fill N/A in for none

            if body['location']['country']:
                if isinstance(body['location']['country'], unicode):
                    country_name = body['location']['country'].encode('utf8')
                else:
                    country_name = body['location']['country']

                #print ('Country: {}'.format(country_name))
                self.logger.info('Country: {}'.format(country_name))
            else:
                country_name = 'None'
                # TODO: Check if string 'None' should really be indexed!

            if body['location'].has_key('country_iso_code'):
                if body['location']['country_iso_code']:
                    country_iso_code =  body['location']['country_iso_code']
                    self.logger.info('Country ISO Code: {}'.format(country_iso_code))
                else:
                    country_iso_code = 'None'
            else:
                country_iso_code = 'None'


            ### 2 ### Check if country description exists and create one if not.
            country_obj = session.query(Country)\
                              .filter(Country.name == country_name)\
                              .one_or_none()

            if country_obj:
                country_id = country_obj.id
                country_iso_code = country_obj.iso_code
                self.logger.info((
                    'Country description was found: ID: {} '
                    '/Name: {} /ISO code: {}').format(
                            country_id, country_name, country_iso_code))

            else:
                errMsg = 'Country name {} not found: '.format(country_name)
                self.logger.error(errMsg)
                raise GenericException(errMsg)

                # if country_iso_code == 'None':
                #     errMsg = 'Country name {} not found: and key [country_iso_code] not present in [location]. Can not insert country with ISO_CODE set to [None]'.format(country_name)
                #     self.logger.error(errMsg)
                #     raise GenericException(errMsg)
                # # Description does not exist so it will be created in db.
                # self.logger.info('Country name not found. Creating one now. Preloading country list with ISO codes is reccommended.')
                # country.name = country_name
                # country.iso_code = country_iso_code
                # session.add(country)
                # session.flush()
                #
                # # Get the id for the new country description. Will be stored in the geological table.
                # country_id = country.id
                # self.logger.info('New country id: {}'.format(country.id))

            return country_id, country_iso_code

        except Exception as e:
            session.rollback()
            raise e

    def insert_state(self, body, session, country_id):

        try:
            #### Insert State info ####
            self.logger.info('Checking state information')
            state = State()
            state_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['state']:
                state_name = body['location']['state'].title()
            else:
                state_name = 'None'
                # TODO: Check if string 'None' should really be indexed!

            ### 2 ### Check if state description exists and create one if not.
            state_id = session.query(State.id)\
                          .filter(and_(State.name == state_name,\
                                        State.id_country == country_id))\
                          .one_or_none()

            if state_id:

                state_id = state_id[0]
                self.logger.info('State description was found: {}'.format(state_id))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('State description not found. New state will be created.')
                state.id_country = country_id
                state.name = state_name
                session.add(state)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                state_id = state.id
                self.logger.info('State id: {}'.format(state.id))

            return state_id

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e

    def insert_city(self, body, session, state_id):

        try:
            #### Insert City info ####
            self.logger.info('Checking city information')
            city = City()
            city_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['city']:
                city_name = body['location']['city'].title()
            else:
                city_name = 'None'

            ### 2 ### Check if city description exists and create one if not.
            city_id = session.query(City.id)\
                          .filter(and_(City.name == city_name,
                                         City.id_state == state_id))\
                          .one_or_none()

            if city_id:

                city_id = city_id[0]
                self.logger.info('City description was found: {}'.format(city_id))
                
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('City description not found. New city will be created.')
                city.id_state = state_id
                city.name = city_name
                session.add(city)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                city_id = city.id
                self.logger.info('City id: {}'.format(city.id))

            return city_id

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e

    def insert_tectonic(self, body, session):

        try:
            #### Insert Tectonic info ####
            self.logger.info('Checking tectonic information.')
            tectonic = Tectonic()

            ### 1 ### Fill N/A in for none
            if body['location']['tectonic']['plate_name']:
                tectonic_plate_name = body['location']['tectonic']['plate_name'].upper()
            else:
                tectonic_plate_name = 'None'
                # TODO: Check if string 'None' should really be indexed!

            ### 2 ### Check if tectonic plate exists and create one if not.
            tectonic_plate_id = session.query(Tectonic.id)\
                              .filter(Tectonic.plate_name == tectonic_plate_name)\
                              .one_or_none()

            if tectonic_plate_id:
                self.logger.info('Tectonic plate was found: {}'.format(tectonic_plate_name))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Tectonic plate was not found. Creating one now.')
                tectonic.plate_name = tectonic_plate_name
                session.add(tectonic)
                session.flush()

                # Get the id for the new tectonic_plate description. Will be stored in the geological table.
                tectonic_plate_id = tectonic.id
                self.logger.info('New tectonic_plate id: {}'.format(tectonic.id))

            return tectonic_plate_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_monument(self, body, session):

        try:
            #### Insert Monument info ####
            self.logger.info('Checking monument information.')
            monument = Monument()
            monument_id = None

            ### 1 ### Fill N/A in for none
            # TODO: Check if string 'None' should really be indexed!
            if body['monument']['description']:
                monument_description = body['monument']['description']
            else:
                monument_description = 'None'

            if body['monument']['monument_inscription']:
                monument_inscription = body['monument']['monument_inscription']
            else:
                monument_inscription = 'None'


            # TODO: Check if this field is used in Sitelog
            #if body['monument']['inscription']:
            #    monument_inscription = body['monument']['inscription']
            #else:
            #    monument_inscription = body['monument']['inscription']

            if body['monument']['foundation']:
                monument_foundation = body['monument']['foundation']
            else:
                monument_foundation = 'None'

            if body['monument']['height']:
                monument_height = body['monument']['height']
            else:
                monument_height = 0.00

            if body['monument']['foundation_depth']:
                monument_foundation_depth = body['monument']['foundation_depth']
            else:
                monument_foundation_depth = 0.00

            self.logger.info(
                'monument: {} {}'.format(
                    type(monument_foundation_depth),
                    type(monument_height)))

            ### 2 ### Check if monument exists and create one if not.
            monument_id = session.query(
                    Monument.id)\
                        .filter(and_(
                            Monument.description == monument_description,
                            Monument.height == monument_height,
                            Monument.foundation == monument_foundation,
                            Monument.foundation_depth == monument_foundation_depth))\
                        .one_or_none()

            if monument_id:
                monument_id = monument_id[0]
                self.logger.info('Monument plate was found: {}'.format(monument_id))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Monument plate was not found. Creating one now.')
                monument.description = monument_description
                monument.inscription = monument_inscription
                monument.foundation = monument_foundation
                monument.foundation_depth = monument_foundation_depth
                monument.height = monument_height
                session.add(monument)
                session.flush()

                monument_id = monument.id
                self.logger.info('New monument id: {}'.format(monument.id))

            return monument_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_coordinates(self, body, session):

        try:
            #### Insert coordinates info ####
            self.logger.info('Checking coordinates information.')
            coordinates = Coordinates()
            coordinates_id = None

            ### 1 ### Fill N/A in for none
            # TODO: Check if string 'None' should really be indexed!
            if body['location']['coordinates']['X']:
                coordinates_x = body['location']['coordinates']['X']
            else:
                coordinates_x = None

            if body['location']['coordinates']['Y']:
                coordinates_y = body['location']['coordinates']['Y']
            else:
                coordinates_y = None

            if body['location']['coordinates']['Z']:
                coordinates_z = body['location']['coordinates']['Z']
            else:
                coordinates_z = None

            if body['location']['coordinates']['lat']:
                coordinates_lat = body['location']['coordinates']['lat']
            else:
                coordinates_lat = None

            if body['location']['coordinates']['lon']:
                coordinates_lon = body['location']['coordinates']['lon']
            else:
                coordinates_lon = None

            if body['location']['coordinates']['altitude']:
                coordinates_altitude = body['location']['coordinates']['altitude']
            else:
                coordinates_altitude = None

            ### 2 ### Check if coordinates exists and create them if not.
            coordinates_id = session.query(
                Coordinates.id)\
                    .filter(and_(Coordinates.x == coordinates_x,
                        Coordinates.y == coordinates_y,
                        Coordinates.z == coordinates_z,
                        Coordinates.lat == coordinates_lat,
                        Coordinates.lon == coordinates_lon,
                        Coordinates.altitude == coordinates_altitude))\
                    .one_or_none()

            if coordinates_id:
                coordinates_id = coordinates_id[0]
                self.logger.info('Coordinate description was found: {}'.format(coordinates_id))

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Coordinate description was not found. Creating one now.')
                coordinates.x = coordinates_x
                coordinates.y = coordinates_y
                coordinates.z = coordinates_z
                coordinates.lat = coordinates_lat
                coordinates.lon = coordinates_lon
                coordinates.altitude = coordinates_altitude

                session.add(coordinates)
                session.flush()

                coordinates_id = coordinates.id
                self.logger.info('New coordinates id: {}'.format(coordinates.id))

            return coordinates_id

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error(
                "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                    .format(
                    fname,
                    self.__class__.__name__,
                    inspect.currentframe().f_code.co_name,
                    exc_tb.tb_lineno,
                    str(e),
                    exc_type))
            session.rollback()
            raise e

    def insert_location(self, body, session, tectonic_id, city_id, coordinates_id):

        #try:
            #### Insert Location info ####
            self.logger.info('Checking location information.')
            location = Location()
            location_id = None

            ### 1 ### Fill N/A in for none
            # TODO: Check if string 'None' should really be indexed!
            if 'description' in body['location'] and  body['location']['description']:
                location_description = body['location']['description'].encode('utf-8')
            else:
                location_description = 'None'

            ### 2 ### Check if country description exists and create one if not.
            location_id = session.query(Location.id)\
                                    .filter(and_(Location.description == location_description,\
                                                 Location.id_city == city_id,\
                                                 Location.id_tectonic == tectonic_id,\
                                                 Location.id_coordinates == coordinates_id))\
                                    .one_or_none()

            if location_id:
                self.logger.info('Location was found: {}'.format(location_description))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Location was not found. Creating one now.')

                location.description = location_description
                location.id_city = city_id
                location.id_tectonic = tectonic_id
                location.id_coordinates = coordinates_id

                session.add(location)
                session.flush()

                # Get the id for the new location description. Will be stored in the geological table.
                location_id = location.id
                self.logger.info('New location id: {}'.format(location.id))

            return location_id

        #except Exception as e:
        #    session.rollback()
        #    raise e

    def insert_station_type(self, body, session):
        '''
        This is a doc string
        '''

        try:
            #### Insert Station type info ####
            self.logger.info('Checking station type information.')
            station_type = StationType()
            station_type_id = None

            if body['station_type']:
                station_type_type = body['station_type']
            else:
                station_type_type = 'None'
                # TODO: Check if string 'None' should really be indexed!

            ### 2 ### Check if country description exists and create one if not.
            station_type_id = session.query(StationType.id)\
                               .filter(StationType.type == station_type_type)\
                              .scalar()

            if station_type_id:
                self.logger.info('Station type was found: {}'.format(station_type_type))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Station type plate was not found. Creating one now.')
                station_type.name = station_type_type
                station_type.type = station_type_type
                session.add(station_type)
                session.flush()

                # Get the id for the new station_type description. Will be stored in the geological table.
                station_type_id = station_type.id
                self.logger.info('New station_type id: {}'.format(station_type.id))

            return station_type_id

        except Exception as e:
            session.rollback()
            raise e


    @profile
    def insert_agency(self, contact, session):
        """
        Check if agency description exists and create one if not.
        """
        contact = self.validate_contact(contact)

        if contact['agency']['abbreviation']:
            Q = session.query(Agency)\
                    .filter_by(abbreviation=contact['agency']['abbreviation'])\
                    .scalar()
        elif contact['agency']['name']:
            """
                the check on MultipleResultsFound will be removed when a new
                unique constrain will be created on this field for table agency ...
            """
            try:
                Q = session.query(Agency)\
                        .filter_by(name=contact['agency']['name'])\
                        .scalar()

            except MultipleResultsFound:
                msg = ('Multiple Agency found with name {} was found: The first will be taken.'
                       '').format(contact['agency']['name'])
                self.logger.warning(msg)

                Q = session.query(Agency) \
                    .filter_by(name=contact['agency']['name']) \
                    .first()

        # Q = session.query(Agency)\
        #         .filter_by(abbreviation=contact['agency']['abbreviation'])\
        #         .scalar()

        if Q:
            id_agency = Q.id
            msg = ('Agency with abbreviation {} was found: id: {}'
                   '').format(Q.abbreviation, id_agency)
            self.logger.info(msg)

            # I choose to modify agency if it is found by abbreviation
            # the problem is that agency data will be refreshed according to
            # last contact updated (or inserted) ...
            Q.address = contact['agency']['address']
            Q.www = contact['agency']['www']
            Q.infos = contact['agency']['infos']
            if contact['agency']['name']:
                Q.name = contact['agency']['name']
        else:
            # Since abbreviation is unique, If is not provided, I set It to  <id>-None
            # to avoid discard the Agency
            abbreviation_present = True
            if not contact['agency']['abbreviation']:
                min_char = 20
                max_char = 20
                allchar = string.ascii_letters
                contact['agency']['abbreviation'] = "".join(choice(allchar) for x in range(randint(min_char, max_char)))
                abbreviation_present = False


            self.logger.info('Agency name not found. Creating one now.')
            q = Agency(**contact['agency'])
            try:
                session.add(q)
                session.flush()
                id_agency = q.id
                self.logger.info('New agency id: {}'.format(id_agency))

                if not abbreviation_present:
                    q.abbreviation = str(id_agency) + "-None"
            except Exception as e:
                session.rollback()
                raise e
        return id_agency


    @profile
    def insert_contact(self, contact, session, id_agency):
        """
        Check if contact description exists and create one if not.
        """
        contact = self.validate_contact(contact)
        try:
            Q = session.query(Contact)\
                    .filter_by(name=contact['name'], email=contact['email'])\
                    .scalar()
        except Exception as e:
            self.logger.info(str(e))
            session.rollback()
            raise e

        if Q:
            id_contact = Q.id
            msg = ('Contact description was found: {}'
                   '').format(id_contact)
            self.logger.info(msg)
            status_code = OK
        else:
            self.logger.info('Contact not found. Creating one now.')
            xcontact = contact
            xcontact['id_agency'] = id_agency
            xcontact.pop('agency', None)
            q = Contact(**xcontact)
            try:
                session.add(q)
                session.flush()
                id_contact = q.id
                self.logger.info('New contact id: {}'.format(id_contact))
                status_code = CREATED

            except Exception as e:
                session.rollback()
                raise e

        return status_code, id_contact


    def insert_network(self, body, session):

        network_id_list = []

        for a_network in body['station_networks']:

            try:
                #### Insert Network info ####
                self.logger.info('Checking network information')
                network = Network()
                network_id = None

                ### 1 ### Fill N/A in for none
                if a_network['name']:
                    network_name = a_network['name']
                else:
                    network_name = 'None'

                ### 2 ### Check if network name exists and create one if not.
                network_id = session.query(Network.id)\
                              .filter(Network.name == network_name)\
                              .one_or_none()

                if network_id:

                    network_id = network_id[0]
                    self.logger.info('Network description was found: {}'.format(network_id))
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Network description not found. New netowrk will be created.')
                    network.name = network_name
                    #TODO: See how to handle contact_id-less network info...
                    #network.id_contact = contact_id
                    session.add(network)
                    session.flush()

                    network_id = network.id
                    self.logger.info('Network id: {}'.format(network.id))

                network_id_list.append(network_id)

            except Exception as e:
                session.rollback()
                raise e

        return network_id_list

    def insert_station(
            self, body, session,
            station_type_id, location_id, geological_id,
            monument_id, country_iso_code):

        try:
            #### Finally insert Station info ####
            self.logger.info('Creating station object..')

            station = Station()
            station.name = body['name']
            station.marker = body['marker'].upper()
            station.description = body['description']
            station.date_from = body['date_from']
            station.date_to = body['date_to']
            station.iers_domes = body['iers_domes']
            station.cpd_num = body['cdp_num']
            if 'monument_num' in body:
                station.monument_num = body['monument_num']
            else:
                station.monument_num = 0
            
            station.receiver_num = body['receiver_num']
            station.country_code = country_iso_code
            station.comment = body['comment']
            station.id_geological = geological_id
            station.id_location = location_id
            station.id_monument = monument_id
            station.id_station_type = station_type_id

            session.add(station)
            session.flush()

            station_marker = station.marker
            station_id = station.id

            self.logger.info('ID for new station is: {}'.format(station_id))

            return station_marker, station_id

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))

            session.rollback()
            raise e

    # Sergio Bruni Nov-24-2018
    def update_station(
            self, body, session, station_id,
            station_type_id, location_id, geological_id,
            monument_id, country_iso_code):

        try:

            if 'monument_num' in body:
                station_monument_num = body['monument_num']
            else:
                station_monument_num = 0

            session.query(Station).filter_by(id=station_id).update({
                Station.name: body['name'],
                Station.description: body['description'],
                Station.date_from: body['date_from'],
                Station.date_to: body['date_to'],
                Station.iers_domes: body['iers_domes'],
                Station.cpd_num: body['cdp_num'],
                Station.monument_num: station_monument_num,
                Station.receiver_num: body['receiver_num'],
                Station.country_code: country_iso_code,
                Station.comment: body['comment'],
                Station.id_geological: geological_id,
                Station.id_location: location_id,
                Station.id_monument: monument_id,
                Station.id_station_type: station_type_id
            })

            return

        except Exception as e:
            session.rollback()
            raise e


    # Sergio bruni INGV
    def insert_station_contact(self, body, session, station_id):

        try:
            for contact in body['station_contacts']:
                contact = contact['contact']
                Q = session.query(Contact) \
                    .filter_by(name=contact['name'], email=contact['email']) \
                    .scalar()

                if not Q:
                    self.logger.error("Contact with name: [{0}] and email: [{1}] NOT FOUND in Database!!! It has not been linked to station: [{2}]" \
                                      .format(contact['name'].encode('utf8'), contact['email'], body['marker']))
                    continue

                self.logger.info('Checking station_contact relationship information')
                station_contact = StationContact()

                station_contact_id = session.query(StationContact.id) \
                    .filter(and_(StationContact.id_station == station_id,
                                 StationContact.id_contact == Q.id)) \
                    .one_or_none()

                if station_contact_id:
                        self.logger.info('station_contact relationship description was found: {}'.format(station_contact_id))
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('station_contact relationship not found. New relationship will be created.')
                    station_contact.id_station = station_id
                    station_contact.id_contact = Q.id
                    session.add(station_contact)
                    session.flush()

                    self.logger.info('station_contact id: {}'.format(station_contact_id))

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e

    def insert_station_network(self, session, station_id, network_id_list):

        station_network_id_list = []

        for network_id in network_id_list:

            self.logger.info('station_id: {} network_id: {}'.format(station_id, network_id))

            try:
                #### Insert City info ####
                self.logger.info('Checking station_network relationship information')
                station_network = StationNetwork()
                station_network_id = None

                ### 2 ### Check if state description exists and create one if not.
                station_network_id = session.query(StationNetwork.id)\
                              .filter(and_(StationNetwork.id_station == station_id,
                                            StationNetwork.id_network == network_id))\
                              .one_or_none()

                if station_network_id:

                    self.logger.info('station_network relationship description was found: {}'.format(station_network_id))

                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('station_network relationship not found. New station_network relationship will be created.')
                    station_network.id_station = station_id
                    station_network.id_network = network_id
                    session.add(station_network)
                    session.flush()

                    # Get the id for the new state description. Will be stored in the station table.
                    station_network_id = station_network.id
                    self.logger.info('station_network id: {}'.format(station_network_id))

                station_network_id_list.append(station_network_id)

            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                self.logger.error(
                    "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                        .format(
                        fname,
                        self.__class__.__name__,
                        inspect.currentframe().f_code.co_name,
                        exc_tb.tb_lineno,
                        str(e),
                        exc_type))
                session.rollback()
                raise e

    def insert_local_tie(self, body, session, station_id):

        local_tie_list = []

        for a_local_tie in body['local_ties']:

            try:
                #### Insert Local tie info ####
                self.logger.info('Checking local tie information')
                local_tie = LocalTie()
                local_tie_id = None

                ### 1 ### Fill N/A in for none
                # TODO: Check if string 'None' should really be indexed!
                if a_local_tie['name']:
                    local_tie_name = a_local_tie['name']
                else:
                    local_tie_name = 'None'

                if a_local_tie['usage']:
                    local_tie_usage = a_local_tie['usage']
                else:
                    local_tie_usage = 'None'

                if a_local_tie['cpd_num']:
                    local_tie_cpd_num = a_local_tie['cpd_num']
                else:
                    local_tie_cpd_num = 'None'

                if a_local_tie['dx']:
                    local_tie_dx = a_local_tie['dx']
                else:
                    local_tie_dx = 0

                if a_local_tie['dy']:
                    local_tie_dy = a_local_tie['dy']
                else:
                    local_tie_dy = 0

                if a_local_tie['dz']:
                    local_tie_dz = a_local_tie['dz']
                else:
                    local_tie_dz = 0

                if a_local_tie['accuracy']:
                    local_tie_accuracy = a_local_tie['accuracy']
                else:
                    local_tie_accuracy = 0

                if a_local_tie['survey_method']:
                    local_tie_survey_method = a_local_tie['survey_method']
                else:
                    local_tie_survey_method = 'None'

                if a_local_tie['date_at']:
                    local_tie_date_at = a_local_tie['date_at']
                else:
                    local_tie_date_at = '9999-01-01 00:00:00'

                if a_local_tie['comment']:
                    local_tie_comment = a_local_tie['comment']
                else:
                    local_tie_comment = 'None'

                ### 2 ### Check if state description exists and create one if not.
                local_tie_id = session.query(LocalTie.id)\
                              .filter(and_(LocalTie.name == local_tie_name,
                                        LocalTie.usage == local_tie_usage,
                                        LocalTie.cpd_num == local_tie_cpd_num,
                                        LocalTie.dx == local_tie_dx,
                                        LocalTie.dy == local_tie_dy,
                                        LocalTie.dz == local_tie_dz,
                                        LocalTie.accuracy == local_tie_accuracy,
                                        LocalTie.survey_method == local_tie_survey_method,
                                        LocalTie.date_at == local_tie_date_at,
                                        LocalTie.comment == local_tie_comment))\
                              .one_or_none()
                if local_tie_id:

                    local_tie_id = local_tie_id[0]
                    self.logger.info('Local tie description was found: {}'.format(local_tie_id))
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Local tie description not found. New state will be created.')
                    local_tie.id_station = station_id
                    local_tie.name = local_tie_name
                    local_tie.usage = local_tie_usage
                    local_tie.cpd_num = local_tie_cpd_num
                    local_tie.dx = local_tie_dx
                    local_tie.dy = local_tie_dy
                    local_tie.dz = local_tie_dz
                    local_tie.accuracy = local_tie_accuracy
                    local_tie.survey_method = local_tie_survey_method
                    local_tie.date_at = local_tie_date_at
                    local_tie.comment = local_tie_comment

                    session.add(local_tie)
                    session.flush()
                    local_tie_id = local_tie.id
                    self.logger.info('Local tie id: {}'.format(local_tie.id))

                local_tie_list.append(local_tie_id)

            except Exception as e:
                session.rollback()
                raise e

        return local_tie_list


    # Sergio Bruni 26/11/2018
    def update_local_tie(self, body, session, station_id, local_tie_dict):

        local_tie_list = []

        for a_local_tie in body['local_ties']:

            try:
                #### Insert Local tie info ####
                self.logger.info('Checking local tie information')
                local_tie = LocalTie()
                local_tie_id = None

                ### 1 ### Fill N/A in for none
                # TODO: Check if string 'None' should really be indexed!
                if a_local_tie['name']:
                    local_tie_name = a_local_tie['name']
                else:
                    local_tie_name = 'None'

                if a_local_tie['usage']:
                    local_tie_usage = a_local_tie['usage']
                else:
                    local_tie_usage = 'None'

                if a_local_tie['cpd_num']:
                    local_tie_cpd_num = a_local_tie['cpd_num']
                else:
                    local_tie_cpd_num = 'None'

                if a_local_tie['dx']:
                    local_tie_dx = a_local_tie['dx']
                else:
                    local_tie_dx = 0

                if a_local_tie['dy']:
                    local_tie_dy = a_local_tie['dy']
                else:
                    local_tie_dy = 0

                if a_local_tie['dz']:
                    local_tie_dz = a_local_tie['dz']
                else:
                    local_tie_dz = 0

                if a_local_tie['accuracy']:
                    local_tie_accuracy = a_local_tie['accuracy']
                else:
                    local_tie_accuracy = 0

                if a_local_tie['survey_method']:
                    local_tie_survey_method = a_local_tie['survey_method']
                else:
                    local_tie_survey_method = 'None'

                if a_local_tie['date_at']:
                    local_tie_date_at = a_local_tie['date_at']
                else:
                    local_tie_date_at = '9999-01-01 00:00:00'

                if a_local_tie['comment']:
                    local_tie_comment = a_local_tie['comment']
                else:
                    local_tie_comment = 'None'

                ### 2 ### Check if state description exists and create one if not.
                local_tie_id = session.query(LocalTie.id)\
                              .filter(and_(
                                        LocalTie.id_station == station_id,
                                        LocalTie.name == local_tie_name,
                                        LocalTie.usage == local_tie_usage,
                                        LocalTie.cpd_num == local_tie_cpd_num,
                                        LocalTie.dx == local_tie_dx,
                                        LocalTie.dy == local_tie_dy,
                                        LocalTie.dz == local_tie_dz,
                                        LocalTie.accuracy == local_tie_accuracy,
                                        LocalTie.survey_method == local_tie_survey_method,
                                        LocalTie.date_at == local_tie_date_at,
                                        LocalTie.comment == local_tie_comment))\
                              .one_or_none()
                if local_tie_id:

                    local_tie_id = local_tie_id[0]
                    self.logger.info('Local tie description was found: {}'.format(local_tie_id))
                    if local_tie_id in local_tie_dict:
                        local_tie_dict[local_tie_id] = True
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Local tie description not found. New state will be created.')
                    local_tie.id_station = station_id
                    local_tie.name = local_tie_name
                    local_tie.usage = local_tie_usage
                    local_tie.cpd_num = local_tie_cpd_num
                    local_tie.dx = local_tie_dx
                    local_tie.dy = local_tie_dy
                    local_tie.dz = local_tie_dz
                    local_tie.accuracy = local_tie_accuracy
                    local_tie.survey_method = local_tie_survey_method
                    local_tie.date_at = local_tie_date_at
                    local_tie.comment = local_tie_comment

                    session.add(local_tie)
                    session.flush()
                    local_tie_id = local_tie.id
                    self.logger.info('Local tie id: {}'.format(local_tie.id))

                local_tie_list.append(local_tie_id)

            except Exception as e:
                session.rollback()
                raise e

        return local_tie_list


    def insert_instrument_collocation(self, body, session, station_id):
        #
        instrument_collocation_list = []

        for instrument in body['instrument_collocation']:
            try:
                #### Insert Colocated Instrument info ####
                self.logger.info('Checking instrument colocation information')
                instrument_collocation = InstrumentColocation()
                instrument_collocation_id = None

                ### 1 ### Fill N/A in for none
                # TODO: Check if string 'None' should really be indexed!
                if instrument['type']:
                    instrument_collocation_type = instrument['type']
                else:
                    # not nullable:
                    instrument_collocation_type = 'None'
                instrument_collocation_status = instrument['status']
                instrument_collocation_date_from = instrument['date_from']
                instrument_collocation_date_to = instrument['date_to']
                instrument_collocation_comment = instrument['comment']
                ### 2 ### Check if exists and create one if not.
                instrument_collocation_id = session.query(
                    InstrumentColocation.id)\
                        .filter(and_(
                            InstrumentColocation.id_station == station_id,
                            InstrumentColocation.type == instrument_collocation_type,
                            InstrumentColocation.status == instrument_collocation_status,
                            InstrumentColocation.date_from == instrument_collocation_date_from,
                            InstrumentColocation.date_to == instrument_collocation_date_to))\
                        .one_or_none()

                if instrument_collocation_id:

                    instrument_collocation_id = instrument_collocation_id[0]
                    self.logger.info('Instrument colocation description was found: {}'.format(instrument_collocation_id))
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Instrument colocation description not found. New state will be created.')
                    instrument_collocation.id_station = station_id
                    instrument_collocation.type = instrument_collocation_type
                    instrument_collocation.status = instrument_collocation_status
                    instrument_collocation.date_from = instrument_collocation_date_from
                    instrument_collocation.date_to = instrument_collocation_date_to
                    instrument_collocation.comment = instrument_collocation_comment

                    session.add(instrument_collocation)
                    session.flush()

                    instrument_collocation_id = instrument_collocation.id
                    self.logger.info('Instrument colocation id: {}'.format(instrument_collocation.id))

                instrument_collocation_list.append(instrument_collocation_id)

            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                self.logger.error(
                    "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                        .format(
                        fname,
                        self.__class__.__name__,
                        inspect.currentframe().f_code.co_name,
                        exc_tb.tb_lineno,
                        str(e),
                        exc_type))
                session.rollback()
                raise e

        return instrument_collocation_list

    # Sergio bruni 27/11(2018
    def update_instrument_colocation(self, body, session, station_id, InstrumentColocation_dir):

        instrument_collocation_list = []

        for instrument in body['instrument_collocation']:
            try:
                #### Insert Colocated Instrument info ####
                self.logger.info('Checking instrument colocation information')
                instrument_collocation = InstrumentColocation()
                instrument_collocation_id = None

                ### 1 ### Fill N/A in for none
                # TODO: Check if string 'None' should really be indexed!
                if instrument['type']:
                    instrument_collocation_type = instrument['type']
                else:
                    # not nullable:
                    instrument_collocation_type = 'None'
                instrument_collocation_status = instrument['status']
                instrument_collocation_date_from = instrument['date_from']
                instrument_collocation_date_to = instrument['date_to']
                instrument_collocation_comment = instrument['comment']
                ### 2 ### Check if exists and create one if not.
                instrument_collocation_id = session.query(
                    InstrumentColocation.id)\
                        .filter(and_(
                            InstrumentColocation.id_station == station_id,
                            InstrumentColocation.type == instrument_collocation_type,
                            InstrumentColocation.status == instrument_collocation_status,
                            InstrumentColocation.date_from == instrument_collocation_date_from,
                            InstrumentColocation.date_to == instrument_collocation_date_to))\
                        .one_or_none()

                if instrument_collocation_id:

                    instrument_collocation_id = instrument_collocation_id[0]
                    self.logger.info('Instrument colocation description was found: {}'.format(instrument_collocation_id))
                    if instrument_collocation_id in InstrumentColocation_dir:
                        InstrumentColocation_dir[instrument_collocation_id]=True
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Instrument colocation description not found. New state will be created.')
                    instrument_collocation.id_station = station_id
                    instrument_collocation.type = instrument_collocation_type
                    instrument_collocation.status = instrument_collocation_status
                    instrument_collocation.date_from = instrument_collocation_date_from
                    instrument_collocation.date_to = instrument_collocation_date_to
                    instrument_collocation.comment = instrument_collocation_comment

                    session.add(instrument_collocation)
                    session.flush()

                    instrument_collocation_id = instrument_collocation.id
                    self.logger.info('Instrument colocation id: {}'.format(instrument_collocation.id))

                instrument_collocation_list.append(instrument_collocation_id)

            except Exception as e:
                session.rollback()
                raise e

        return instrument_collocation_list

    def validate_contact(self, contact):
        """
        Force 'contact' input to conform to expected structure
        with DB fields ready for 'insert_contact()' method.
        
        Missing dict fields will be added with None values.
        
        Superfluous dict fields are NOT CHECKED and might cause trouble.
        
        Assumption:
            - either contact is a string with the person's name
            - or contact is a dict with agency and person info
            - arrays are not handled at all
        
        Contact and agency info comes from sitelog JSON input,
        almost all entries are contact dicts with included agency
        dicts, except for 'logs', which only have a 'contact' string.
        Contact arrays exist as well, but they are expected to be
        given element-wise to this method.
        """
        self.logger.info('Checking contact and agency information.')

        # Sergio Bruni (added fax)
        co_list = [
            'name', 'title', 'email', 'phone', 'gsm',
            'comment', 'agency', 'role', 'fax'
        ]
        ag_list = [
            'name', 'abbreviation', 'address', 'www', 'infos'
        ]
        # if isinstance(contact, (str, unicode)):
        if isinstance(contact, str):
            contact = {'name': contact}
        if isinstance(contact, dict):
            # expected: contact['contact'] is the actual dict with all info
            if 'contact' in contact:
                contact = contact['contact']
            for key in co_list:
                if not key in contact:
                    contact[key] = None
            if not isinstance(contact['agency'], dict):
                contact['agency'] = {}
            for key in ag_list:
                if not key in contact['agency']:
                    contact['agency'][key] = None
        # Fill mandatory fields in case they are empty:
        if not contact['name']:
            contact['name'] = 'None'

        if not contact['agency']['name']:
            contact['agency']['name'] = 'None'
        # Raising an error might be better here,
        # but then input must be good enough to use the DB-API at all.
        return contact


    def insert_log(self, body, session, id_station):

        if not body['logs']:
            msg = 'sitelog file does not contains log information!!!'
            self.logger.error(msg)
            return False

        log = body['logs']
        contact = self.validate_contact(log['contact'])

        id_contact = None
        """
            since log part of sitelog-json-input-data has not contact-email
            First I search for unique contact with this name in the table contact
        """
        try:
            Q = session.query(Contact) \
                .filter(Contact.name == contact['name']) \
                .one_or_none()

            if not Q:
                msg = (
                    'Contact name: [{}] was NOT found in contact table. Current Log istance will be not linked to any contact'
                    ).format(contact['name'].encode('utf8'))
                self.logger.info(msg)
            else:
                id_contact = Q.id

        except MultipleResultsFound:
            """
                Since I found multiple contacts with the current name in the table contact
                I look for an unique contact with this name that belongs to the station
            """
            msg = (
                'More instances of contact with name: [{}] exist contact table. So I''ll try to check if one, and only one, of those belongs to station with id [{}]'
            ).format(contact['name'].encode('utf8'), id_station)
            self.logger.info(msg)

            # since log part of sitelog-json-input-data has not contact-email
            # I look for station-contact (hoping it exists and there's only one with that name)
            try:
                 Q = session.query(
                    Station,
                    Contact) \
                    .join(StationContact, StationContact.id_station == Station.id) \
                    .join(Contact, StationContact.id_contact == Contact.id) \
                    .filter(and_(Station.id == id_station, Contact.name == contact['name'])) \
                    .one_or_none()
            except MultipleResultsFound:
                msg = ('More instances of contact with name: [{}] exist in contact table and also more then one of those belog to station id: [{}]. Current Log istance will be not linked to any contact'
                       ).format(contact['name'].encode('utf8'), id_station)
                self.logger.error(msg)
                id_contact = None
            else:
                if not Q:
                    msg = ('More instances of contact with name: [{}] exist in contact table and none of those belong to station id: [{}]. Current Log istance will be not linked to any contact'
                           ).format(contact['name'].encode('utf8'), id_station)
                    self.logger.error(msg)
                    id_contact = None
                else:
                    id_contact = Q.Contact.id

        Q = session.query(LogType)\
            .filter_by(name=log['log_type'])\
            .scalar()
        if Q:
            id_log_type = Q.id
        else:
            q = LogType(name=log['log_type'])
            session.add(q)
            session.flush()
            id_log_type = q.id
            msg = ('Inserted new log_type (id: {}): "{}"'
                   '').format(id_log_type, log['log_type'])
            self.logger.info(msg)

        # Insert log:
        xlog = Log(
            title=log['title'],
            date=log['date'],
            id_log_type=id_log_type,
            id_station=id_station,
            modified=log['modified'],
            previous=log['previous'],
            id_contact=id_contact

        )
        try:
            session.add(xlog)
            session.flush()
            id_log = xlog.id
            self.logger.info('Inserted new log (id: {})'.format(id_log))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                .format(
                fname,
                self.__class__.__name__,
                inspect.currentframe().f_code.co_name,
                exc_tb.tb_lineno,
                str(e),
                exc_type))
            session.rollback()
            raise e

        return True

    # Sergio Bruni
    # This function differ from insert_log only because it receives
    # one additional parameter: contact_dict
    # It use this parameter to flag the contacts (actually only one) that are tied to log
    # the other ones will be deleted (after this function has been called ...)
    def update_log(self, body, session, id_station, log_type_dict, log_dict, contact_dict):
        # the database is structured so the Station is related to one or many log
        # but the site log json file contains only one log for each station ...
        if not body['logs']:
            msg = 'sitelog file does not contains log information!!!'
            self.logger.error(msg)
            return False

        log = body['logs']
        contact = self.validate_contact(log['contact'])

        """
            since log part of sitelog-json-input-data has not contact-email
            First I search for unique contact with this name in the table contact
        """

        id_contact = None
        try:
            Q = session.query(Contact) \
                .filter(Contact.name == contact['name']) \
                .one_or_none()

            if not Q:
                msg = (
                    'Contact name: [{}] was NOT found in contact table. Current Log istance will be not linked to any contact'
                    ).format(contact['name'].encode('utf8'))
                self.logger.info(msg)
            else:
                id_contact = Q.id
                if id_contact in contact_dict:
                    contact_dict[id_contact] = True

        except MultipleResultsFound:
            """
            Since I found multiple contacts with the current name in the table contact
            I look for an unique contact with this name that belongs to the station
            """
            try:
                Q = session.query(
                    Station,
                    Contact) \
                    .join(StationContact, StationContact.id_station == Station.id) \
                    .join(Contact, StationContact.id_contact == Contact.id) \
                    .filter(and_(Station.id == id_station, Contact.name == contact['name'])) \
                    .one_or_none()
            except MultipleResultsFound:
                msg = ('More then one Contact with name: [{}] exist for station id: [{}]. Current Log istance will be not linked to any contact'
                       ).format(contact['name'].encode('utf8'), id_station)
                self.logger.error(msg)
                id_contact = None
            else:
                if not Q:
                    msg = 'Contact name: [{}] was NOT found for station id: [{}]: current Log istance will be not linked to any contact'\
                           .format(contact['name'].encode('utf8'), id_station)
                    self.logger.error(msg)
                    id_contact = None
                else:
                    id_contact = Q.Contact.id
                    if id_contact in contact_dict:
                        contact_dict[id_contact] = True

        Q = session.query(LogType)\
            .filter_by(name=log['log_type'])\
            .scalar()
        if Q:
            id_log_type = Q.id
            if id_log_type in log_type_dict:
                log_type_dict[id_log_type] = True
        else:
            q = LogType(name=log['log_type'])
            session.add(q)
            session.flush()
            id_log_type = q.id
            msg = ('Inserted new log_type (id: {}): "{}"'
                   '').format(id_log_type, log['log_type'])
            self.logger.info(msg)

        # Insert log:
        xlog = Log(
            title=log['title'],
            date=log['date'],
            id_log_type=id_log_type,
            id_station=id_station,
            modified=log['date'],
            previous=log['previous'],
            id_contact=id_contact
        )
        try:
            session.add(xlog)
            session.flush()
            id_log = xlog.id
            self.logger.info('Inserted new log (id: {})'.format(id_log))
        except Exception as e:
            session.rollback()
            raise e

        return True

    def insert_conditions(self, body, session, id_station):
        """
        Insert associated station conditions with given effects.
        """
        if 'conditions' not in body or not body['conditions']:
            return True
        # Prepare expected parameter names:
        k_list = [
            'type', 'date_from', 'date_to', 'degradation', 'comments'
        ]

        conditions = body['conditions']
        if isinstance(conditions, dict):
            conditions = [conditions]
        for cond in conditions:
            # Fill in parameter if missing:
            for key in k_list:
                if not key in cond:
                    cond[key] = None
            # Mandatory key:
            if not cond['type']:
                cond['type'] = 'None'
            # Find effect type:
            Q = session.query(Effect)\
                    .filter_by(type=cond['type'])\
                    .scalar()
            if Q:
                id_effect = Q.id
                msg = ('Effect description was found: {}'
                       '').format(id_effect)
                self.logger.info(msg)
            else:
                self.logger.info('Effect type not found. Creating one now.')
                q = Effect(type=cond['type'])
                try:
                    session.add(q)
                    session.flush()
                    id_effect = q.id
                    self.logger.info('New effect id: {}'.format(id_effect))
                except Exception as e:
                    session.rollback()
                    raise e
            # Insert condition:
            xcond = Condition(
                id_station=id_station,
                id_effect=id_effect,
                date_from=cond['date_from'],
                date_to=cond['date_to'],
                degradation=cond['degradation'],
                comments=cond['comments']
            )
            try:
                session.add(xcond)
                session.flush()
                id_condition = xcond.id
                msg = ('Inserted new station condition (id: {})'
                       '').format(id_condition)
                self.logger.info(msg)
            except Exception as e:
                session.rollback()
                raise e
        return True


    # Sergio Bruni
    def update_conditions(self, body, session, id_station, effect_dict):
        """
        Insert associated station conditions with given effects.
        """
        if 'conditions' not in body or not body['conditions']:
            return True
        # Prepare expected parameter names:
        k_list = [
            'type', 'date_from', 'date_to', 'degradation', 'comments'
        ]

        conditions = body['conditions']
        if isinstance(conditions, dict):
            conditions = [conditions]
        for cond in conditions:
            # Fill in parameter if missing:
            for key in k_list:
                if not key in cond:
                    cond[key] = None
            # Mandatory key:
            if not cond['type']:
                cond['type'] = 'None'
            # Find effect type:
            Q = session.query(Effect)\
                    .filter_by(type=cond['type'])\
                    .scalar()
            if Q:
                id_effect = Q.id
                msg = ('Effect description was found: {}'
                       '').format(id_effect)
                self.logger.info(msg)
                if id_effect in effect_dict:
                    effect_dict[id_effect] = True
            else:
                self.logger.info('Effect type not found. Creating one now.')
                q = Effect(type=cond['type'])
                try:
                    session.add(q)
                    session.flush()
                    id_effect = q.id
                    self.logger.info('New effect id: {}'.format(id_effect))
                except Exception as e:
                    session.rollback()
                    raise e
            # Insert condition:
            xcond = Condition(
                id_station=id_station,
                id_effect=id_effect,
                date_from=cond['date_from'],
                date_to=cond['date_to'],
                degradation=cond['degradation'],
                comments=cond['comments']
            )
            try:
                session.add(xcond)
                session.flush()
                id_condition = xcond.id
                msg = ('Inserted new station condition (id: {})'
                       '').format(id_condition)
                self.logger.info(msg)
            except Exception as e:
                session.rollback()
                raise e
        return True


    def insert_item_attribute(self,item_attribute_obj, session, item_id):

        self.logger.info(
                'Inserting item attribute {0}'.format(item_attribute_obj))
        self.logger.info('Item ID {0}'.format(item_id))
        
        # temp dict to keep parameter values to be indexed:
        td = {
            'id_item': item_id,
            'id_attribute': None,
            'date_from': None,
            'date_to': None,
            'value_varchar': None,
            'value_date': None,
            'value_numeric': None,
        }

        # Check name, date_from, date_to:
        if item_attribute_obj['attribute']['name']:
            name = item_attribute_obj['attribute']['name']
            self.logger.info('Attribute type name found: {0}'.format(name))

            td['id_attribute'] = session.query(Attribute.id)\
                          .filter(Attribute.name == name)\
                          .scalar()
            if td['id_attribute']:
                self.logger.info(
                    'Attribute id "{0}" found'.format(td['id_attribute']))
            else:
                msg = ('WARNING (skip): item attribute name invalid: '
                       'item.id {}, item_attribute.name {}').format(
                               item_id, name)
                self.logger.info(msg)
                return False
        else:
            msg = ('WARNING (skip): No item attribute name given: '
                   'item.id {}, ???').format(item_id)
            self.logger.info(msg)
            return False
        
        if item_attribute_obj['date_from']:
            td['date_from'] = item_attribute_obj['date_from']
        else:
            msg = ('WARNING: item attribute without date_from: '
                   'item.id {}, item_attribute.name {}').format(
                           item_id, name)
            self.logger.info(msg)
        td['date_to'] = item_attribute_obj['date_to']
        #
        # Store other parameters (value_numeric may still change ...)
        if item_attribute_obj['value_varchar'] is None:
            td['value_varchar'] = None
        else:
            # modified by sergio bruni INGV 13/11/2018
            # some string are formatted in unicode, (i.e. CESI: "value_varchar": "SUHNER SUCOFEED \u00a3/8\" HF-FR 04 46")
            # some string contains numeric value (i.e GOPE: "value_varchar": 10.0)
            if isinstance(item_attribute_obj['value_varchar'], str):
                td['value_varchar'] = item_attribute_obj['value_varchar']
            # elif isinstance(item_attribute_obj['value_varchar'], unicode):
            elif isinstance(item_attribute_obj['value_varchar'], str):
                td['value_varchar'] = item_attribute_obj['value_varchar'].encode('utf8')
            else:
                td['value_varchar'] = str(item_attribute_obj['value_varchar'])

            if len(td['value_varchar']) > 50:
                msg = ('WARNING: item attribute value_varchar is longer than 50 chars and has been truncate to 50.'
                       ' Original value is [{}]').format(td['value_varchar'])
                self.logger.info(msg)
                td['value_varchar'] = td['value_varchar'][:50]

        td['value_numeric'] = item_attribute_obj['value_numeric']
        td['value_date'] = item_attribute_obj['value_date']
        #
        ### 2 ### Check if item attribute exists and create one if not.
        item_attribute_id = session.query(ItemAttribute.id)\
            .filter(and_(
                ItemAttribute.id_item == item_id,
                ItemAttribute.id_attribute == td['id_attribute'],
                ItemAttribute.date_from == td['date_from'],
                ItemAttribute.date_to == td['date_to'],
                ItemAttribute.value_numeric == td['value_numeric'],
                ItemAttribute.value_varchar == td['value_varchar'],
                ))\
            .scalar()

        if item_attribute_id:
            self.logger.info(('Item attribute description was found: '
                              '{0}').format(item_attribute_id))
        else:
            # item attribute does not exist so it will be created in db.
            self.logger.info(('Item attribute not found. '
                              'New instance will be created.'))
            item_attribute = ItemAttribute(**td)
            session.add(item_attribute)
            session.flush()

        return True

    def insert_station_item(
            self, session, station_id, item_id, date_from, date_to):
        """
        Insert a link-table entry in 'station_item'.
        Note that 'date_from' is not nullable.
        """
        self.logger.info(
            ('station_id: {0} item_id: {1} date_from: {2} date_to: {3}'
             '').format(station_id, item_id, date_from, date_to))
        si = {
            'id_station': station_id,
            'id_item': item_id,
            'date_from': date_from,
            'date_to': date_to,
        }
        try:
            self.logger.info('Checking station_item relationship information')
            if not si['date_from']:
                si['date_from'] = '1900-01-01 00:00:00'
            # Check if exists, insert if not.
            Q = session.query(StationItem)\
                    .filter_by(id_station=si['id_station'],
                               id_item=si['id_item'])\
                    .scalar()
            if Q:
                id_station_item = Q.id
                self.logger.info(
                    ('station_item relationship description was found: {}'
                     '').format(id_station_item))
            else:
                self.logger.info(
                    ('station_item relationship not found. New '
                     'relationship will be created.'))
                station_item = StationItem(**si)
                session.add(station_item)
                session.flush()
                id_station_item = station_item.id
                self.logger.info('station_item id: {}'.format(id_station_item))
        except Exception as e:
            session.rollback()
            raise e
        return id_station_item

    ### Rinex file POST oriented functions ###

    def insert_datacenter(self, body, session):
        '''
        This is a doc string
        '''

        #### Insert Data center info ####
        self.logger.info('Checking data center information.')
        data_center = DataCenter()
        data_center_id = None

        ### Check required parameters from the JSON file
        list_required_parameters = [
                'name', 'acronym', 'protocol', 'hostname', 'root_path']
        for key in list_required_parameters:
            if key not in body:
                raise NoValueException("JSON File missing {}".format(key))

        # Check special case: agency
        if 'agency' not in body:
            raise NoValueException("JSON File missing 'agency'")
        if 'abbreviation' not in body['agency']:
            raise NoValueException("JSON File missing 'agency/abbreviation'")

        # Check if datacenter exists and create one if not.
        """
            2020-06-19 (Sergio Bruni) removed filter by name because of 
            on database  the unique constraint is defined on acronym only
        """
        # .filter(and_(DataCenter.name == body['name'],
        #              DataCenter.acronym == body['acronym']))\
        data_center_id = session.query(DataCenter.id) \
                .filter(DataCenter.acronym == body['acronym']) \
                .scalar()

        if data_center_id:
            msg = (u'ERROR: Will not overwrite existing data center: '
                   u'{}').format(body['name'])
            self.logger.info(msg)
            self.errors.append(msg.upper())
            return CONFLICT

        # data center is new
        self.logger.info(
            'Data center name not found. Creating one now.')

        # check if agency exists in the DB
        agency_abbr = body['agency']['abbreviation']
        agency_id = session.query(Agency.id)\
            .filter(Agency.abbreviation == agency_abbr)\
            .first() # HACK: this should be one_or_none() !!!
        # TODO: sitelogs can introduce same agency multiple times,
        #   scalar() and one_or_none() raise exceptions then.

        if agency_id:
            agency_id = agency_id[0]
        else:
            msg = 'ERROR: Agency abbreviation not found: {}'.format(
                    agency_abbr)
            self.logger.info(msg)
            self.errors.append(msg)
            return INTERNAL_SERVER_ERROR

        # Description does not exist so it will be created in db.
        data_center.name = body['name']
        data_center.id_agency = agency_id
        data_center.acronym = body['acronym']
        data_center.protocol = body['protocol']
        data_center.hostname = body['hostname']
        data_center.root_path = body['root_path']

        # Temporary hack TODO: remove it
        #temp_id = session.query(DataCenter.id).order_by(DataCenter.id.desc()).first()
        #data_center.id = temp_id[0]+1

        session.add(data_center)
        session.flush()

        data_center_id = data_center.id
        msg = 'New data_center id: {}'.format(data_center.id)
        self.logger.info(msg)
        self.infos.append(msg.upper())
        
        # add the data center structure
        self.insert_data_center_structure(body, session, data_center_id)
        try:
            session.commit()
        except Exception as e:
            session.rollback()
            raise e

        return OK


    def insert_data_center_structure(self, body, session, data_center_id):
        '''
        Insert Data center structure info.
        '''

        my_rinex_filename = ''
        if self.rinex_filename:
            my_rinex_filename = '[{}]'.format(self.rinex_filename)

        self.logger.info('{} Checking data center structure information.'.format(my_rinex_filename))

        data_center_structure = DataCenterStructure()
        data_center_structure_id = None


        # Check if required parameters in JSON file are present,
        # as there are two different functions calling this one with different
        # input parameters...
#        if not body.has_key('directory_structure'):
        if not 'directory_structure' in body:
            raise NoValueException("JSON file missing 'directory_structure'")
      

        if 'id_file_type' in body['directory_structure']:
            # So this function was called by insert_rinex_file()
            if 'id_data_center' in body['directory_structure']:
                if not body['directory_structure']['id_data_center']:
                    body['directory_structure']['id_data_center'] = \
                        data_center_id
            else:
                body['directory_structure']['id_data_center'] = \
                    data_center_id

        
            dcs = body['directory_structure']
            data_center_structure_id = session.query(DataCenterStructure.id)\
                .filter(DataCenterStructure.id_data_center == dcs['id_data_center'])\
                .filter(DataCenterStructure.id_file_type == dcs['id_file_type'])\
                .scalar()
            # TODO: decide whether inserting here should be forbidden,
            #       as not coming from data center.
            # This is an ad-hoc insertion for development tests,
            # as the initial data center structures may not be enough.
            if not data_center_structure_id:
                data_center_structure = DataCenterStructure(
                    **body['directory_structure'])
                session.add(data_center_structure)
                session.flush()

                # Get the id for the new data center structure
                data_center_structure_id = data_center_structure.id
                self.logger.info(
                    'New data_center_structure id: {}'.format(
                            data_center_structure.id))
                #session.commit()
        else:
            list_required_parameters = [
                    'directory_naming',
                    'format',
                    'sampling_window',
                    'sampling_frequency']
            for parameter in list_required_parameters:
                if not body['directory_structure'].has_key(parameter):
                    raise NoValueException(
                        "JSON File missing 'directory_structure/{}'".format(
                                parameter))

            # Add a new data_center structure
            # first test for the existence of the file_type entry
            id_file_type = session.query(FileType.id).filter(
                FileType.format == body['directory_structure']['format'],
                FileType.sampling_window == body['directory_structure']['sampling_window'],
                FileType.sampling_frequency == body['directory_structure']['sampling_frequency']).scalar()

            if id_file_type:
                data_center_structure.id_file_type = id_file_type
            else:
                self.logger.info('File type not found. Posting file type to file_type.')
                id_file_type = self.insert_file_type(body['directory_structure'],session)
                data_center_structure.id_file_type = id_file_type

            data_center_structure.directory_naming = body[
                    'directory_structure']['directory_naming']
            data_center_structure.id_data_center = data_center_id

            # Temporary hack TODO: remove it
            #temp_id = session.query(DataCenterStructure.id).order_by(DataCenterStructure.id.desc()).first()
            #data_center_structure.id = temp_id[0]+1

            session.add(data_center_structure)
            session.flush()

            # Get the id for the new data center structure
            data_center_structure_id = data_center_structure.id
            self.logger.info('New data_center_structure id: {}'.format(data_center_structure.id))
            #session.commit()

        #return data_center_structure_id
        return data_center_structure_id


    def insert_file_type(self, body, session):

        try:
            #### Insert Contact info ####
            self.logger.info('Checking file type information')
            file_type = FileType()
            file_type_id = None

            ### 1 ### Fill N/A in for none
            if body['sampling_window']:
                file_type_sampling_window = body['sampling_window']
            else:
                file_type_sampling_window = 'None'

            if body['sampling_frequency']:
                file_type_sampling_frequency = body['sampling_frequency']
            else:
                file_type_sampling_frequency = 'None'

            if 'file_type' in body:
                file_type_format = body['file_type']
            elif 'format' in body:
                file_type_format = body['format']
            else:
                file_type_format = 'None'

            ### 2 ### Check if state description exists and create one if not.
            file_type_id = session.query(FileType.id)\
                          .filter(and_(FileType.format == file_type_format,
                                       FileType.sampling_window == file_type_sampling_window,
                                       FileType.sampling_frequency == file_type_sampling_frequency))\
                          .one_or_none()

            if file_type_id:
                file_type_id = file_type_id[0]
                self.logger.info('File type description was found: {}'.format(file_type_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('File type description not found. New file type will be created.')
                file_type.format = file_type_format
                file_type.sampling_window = file_type_sampling_window
                file_type.sampling_frequency = file_type_sampling_frequency
                session.add(file_type)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                file_type_id = file_type.id
                self.logger.info('file_type id: {}'.format(file_type.id))
                #session.commit()

            return file_type_id

        except Exception as e:
            myname = sys._getframe().f_code.co_name
            self.logger.error('Error in function {}. error message: {} '.format(myname, str(e)))
            raise e

    def reset_rinex_file(self, body, session, rinex_file, station_id, data_center_id, file_type_id):
        # reset => update to the body parameters, set the status=0 and delete_QC
        try:
            self.update_rinex_file(body, session, rinex_file, station_id, data_center_id, file_type_id, 0)
            self.delete_QcReportSummary(session, rinex_file.id)
            session.flush()
            session.commit()

            status = f"""[{body['file_name']}] change in station_id or file_type or reference_date or other. Going to reset rinex_file"""
            self.logger.info(status)
            status_code = OK

        except Exception as e:
            session.rollback()
            status_code = INTERNAL_SERVER_ERROR
            myname = sys._getframe().f_code.co_name
            status = f"""[{body['file_name']}] Error occurred in reset_rinex_file().id {rinex_file.id}, 
            Error in function {myname}. Error message: {str(e)}"""
            self.logger.error(status)

        return status, status_code




    def insert_rinex_file(self, body, session, station_id, data_center_id, file_type_id):
        try:
            rinex_file = RinexFile()
            rinex_file.creation_date = body['creation_date']
            rinex_file.revision_date = body['revision_date']

            self.update_rinex_file(body, session, rinex_file, station_id, data_center_id, file_type_id, 0)
            session.add(rinex_file)
            self.logger.info(f"""[{body['file_name']} rinex_file id: {rinex_file.id}""")
            session.flush()
            session.commit()
            status_code = CREATED
            status = f"""[{body['file_name']}] Rinex file has been indexed with id {rinex_file.id}"""
            self.logger.info(status)
        except Exception as e:
            session.rollback()
            status_code = INTERNAL_SERVER_ERROR
            myname = sys._getframe().f_code.co_name
            status = f"[{body['file_name']}] Unknown error seems to have occurred in {myname}, error: {str(e)}"
            self.logger.error(status)


        return status, status_code



    # create a rinex_file datamodel object
    def update_rinex_file(self, body, session, rinex_file, station_id, data_center_id, file_type_id, status):
        try:
            #### Insert Rinex file info ####
            self.logger.info(f"""[{body['file_name']}] Updating rinex_file information""")

            ### Fill None not present key or empty in json input ###
            # INGV March 2023
            if 'file_size' in body and body['file_size']:
                rinex_file_size = body['file_size']
            else:
                rinex_file_size = None

            if 'md5uncompressed' in body.keys() and body['md5uncompressed']:
                rinex_file_md5uncompressed = body['md5uncompressed']
            else:
                rinex_file_md5uncompressed = None


            # Description does not exist so it will be created in db.
            data_center_structure = {
                'id_data_center': data_center_id,
                'id_file_type': file_type_id,
            }
            dcs = {'directory_structure': data_center_structure}
            data_center_structure_id = self.insert_data_center_structure(dcs, session, data_center_id)


            rinex_file.name = body['file_name'] #Mandatory
            rinex_file.id_station = station_id
            rinex_file.id_data_center_structure = data_center_structure_id
            rinex_file.file_size = rinex_file_size
            rinex_file.id_file_type = file_type_id
            rinex_file.relative_path = body['relative_path'] #Mandatory
            rinex_file.reference_date = body['reference_date'] #Mandatory
            rinex_file.published_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#            the creation_date and revision_date are only set by the check_creation_revision_date fuction
#            rinex_file.creation_date = body['creation_date']
#            rinex_file.revision_date = body['revision_date']
            rinex_file.md5checksum = body['md5_checksum'] #Mandatory
            rinex_file.md5uncompressed = rinex_file_md5uncompressed
            rinex_file.status = status

        except Exception as e:
            myname = sys._getframe().f_code.co_name
            errMsg = f"""[{body['file_name']}] Error in function {myname}. error message: {str(e)} """
            self.logger.error(errMsg)
            raise GenericException(errMsg)




    ### T3 POST functions 

    def qc_post_echo(self, body):
        """
        Echo posted QC XML to webpage as JSON.
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        result = None
        status_code = NO_CONTENT

        qc = parse_qcxml(body)
        
        if qc:
            result = json.dumps(qc)
            status_code = OK
        return result, status_code


    def insert_qualityfile(self, session, d_qualityfile):
        """
        Insert new quality file (QC-XML).
        """
        self.logger.info('Checking quality_file information')
        try:
            # check existence
            """
                2020-06-19 (Sergio Bruni) removed filter by name because of 
                on database  the unique constraint is defined on md5checksum only
            """
            # .filter_by(md5checksum=d_qualityfile['md5checksum'],
            #            name=d_qualityfile['name']) \

            Q = session.query(QualityFile)\
                .filter_by(md5checksum=d_qualityfile['md5checksum']) \
                .scalar()



            if Q:
                msg = 'quality_file found: {}'.format(Q.id)
                self.logger.info(msg)
                # refuse insert action
                session.rollback()
                return -Q.id
            else:
                msg = 'quality_file not found, create new one now.'
                self.logger.info(msg)
                q = QualityFile(**d_qualityfile)
                session.add(q)
                session.flush()
                id_q = q.id
                self.logger.info('quality_file.id: {}'.format(id_q))
        except Exception as e:
            session.rollback()
            raise e
        return id_q


    def insert_qcparameter(self, session, d_qcparameter):
        """
        Get the id of a qc_parameter entry, or insert new one.
        """
        self.logger.info('[{}] Checking qc_parameter information' \
            .format(self.rinex_filename))
        try:
            # check if this qc_parameters set already exists
            Q = session.query(QcParameter)\
                .filter_by(**d_qcparameter)\
                .scalar()
            if Q:
                msg = '[{}] qc_parameter set was found: {}'.format(self.rinex_filename, Q.id)
                self.logger.info(msg)
                id_q = Q.id
            else:
                msg = '[{}] qc_parameter set not found, create new one now.'.format(self.rinex_filename)
                self.logger.info(msg)
                q = QcParameter(**d_qcparameter)
                session.add(q)
                session.flush()
                id_q = q.id
                self.logger.info('[{}] qc_parameter.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            session.rollback()
            raise e
        return id_q


    def insert_qcreportsummary(self, session, d_qcreportsum):
        """
        Insert the QC report summary entry of a QC file,
        and get its id.
        """
        self.logger.info('[{}] Checking qc_report_summary information'.format(self.rinex_filename))
        try:
            # check if qc info already exists for this RINEX file
            Q = session.query(QcReportSummary)\
                .filter_by(id_rinexfile = d_qcreportsum['id_rinexfile'])\
                .scalar()
            if Q:
                msg = '[{}] qc_report_summary for same RINEX file found: {}'\
                    .format(self.rinex_filename, Q.id)
                self.logger.info(msg)
                # So, what now? Overwrite, create another, refuse?
                # ...THERE CAN BE ONLY ONE...
                session.rollback()
                return -1
            else:
                msg = '[{}] qc_report_summary not found, create new one now.'.format(self.rinex_filename)
                self.logger.info(msg)
                q = QcReportSummary(**d_qcreportsum)
                session.add(q)
                session.flush()
                id_q = q.id
                self.logger.info('[{}] qc_report_summary.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            session.rollback()
            raise e
        return id_q
    
    
    # def insert_constellation(self, session, d_constellation):
    #     """
    #     Get satellite constellation id, or insert new if not found.
    #     """
    #     # TODO: return message if new inserted, to show in final response.
    #     self.logger.info('[{}] Checking constellation information'.format(self.rinex_filename))
    #     try:
    #         # check existence
    #         Q = session.query(Constellation)\
    #             .filter_by(**d_constellation)\
    #             .first()
    #             #.scalar()
    #         if Q:
    #             msg = '[{}] constellation was found: {}'.format(self.rinex_filename, Q.id)
    #             self.logger.info(msg)
    #             id_q = Q.id
    #         else:
    #             msg = '[{}] constellation not found, create new one now.'.format(self.rinex_filename)
    #             self.logger.info(msg)
    #             # This is unorthodox and values will be missing,
    #             # we default names to 'X' and have 'official'='false',
    #             # allowing for dynamic constellation insertion.
    #             q = Constellation()
    #             q['official'] = False
    #             for k in q:
    #                 q[k] = d_constellation.get(k, 'X')
    #             q['official'] = False
    #             session.add(q)
    #             session.flush()
    #             id_q = q.id
    #             self.logger.info('{}] constellation.id: {}'.format(self.rinex_filename, id_q))
    #     except Exception as e:
    #         session.rollback()
    #         raise e
    #     return id_q


    def insert_constellation(self, session, d_constellation):
        """
        Get satellite constellation id, or insert new if not found.
        """
        # TODO: return message if new inserted, to show in final response.
        self.logger.info('[{}] Checking constellation information'.format(self.rinex_filename))
        try:
            # check existence
            Q = session.query(Constellation)\
                .filter_by(**d_constellation)\
                .scalar()
            if Q:
                msg = '[{}] constellation was found: {}'.format(self.rinex_filename, Q.id)
                self.logger.info(msg)
                id_q = Q.id
            else:
                msg = '[{}] constellation not found, create new one now.'.format(self.rinex_filename)
                self.logger.info(msg)
                # This is unorthodox and values will be missing,
                # we default names to 'X' and have 'official'='false',
                # allowing for dynamic constellation insertion.

                td = {
                    'official': False
                }

                for k in ('identifier_1ch', 'identifier_3ch', 'name'):
                    td[k] = d_constellation.get(k, 'X')

                q = Constellation(**td)

                session.add(q)
                session.flush()
                id_q = q.id
                self.logger.info('{}] constellation.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e
        return id_q


    def insert_qcnavigationmsg(self, session, l_of_d):
        """
        Insert all QC navigation message summaries.
        """
        self.logger.info('[{}] Checking qc_navigation_msg information'.format(self.rinex_filename))
        try:
            # no need to check existence,
            # unless we're updating (not implemented), or
            # unless same constellation present twice...
            # TODO: check that
            #       return not-None message in case of duplicates
            for d in l_of_d:
                con = {'identifier_3ch': d['id_constellation']}
                id_const = self.insert_constellation(session, con)
                d['id_constellation'] = id_const
                q = QcNavigationMsg(**d)
                session.add(q)
                session.flush()
                id_q = q.id
                self.logger.info('[{}] qc_navigation_msg.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e
        return True


    def insert_qc_con_and_obs_summary(self, session, l_of_d):
        #return
        """
        Insert all qc_constellation_summary entries and all associated
        qc_observation_summary entries as well.
        """
        self.logger.info('[{}] Checking qc_constellation_summary information'.format(self.rinex_filename))
        try:
            # no need to check existence,
            # unless we're updating (not implemented), or
            # unless same constellation present twice...
            # TODO: check that
            #       return not-None message in case of duplicates
            obskeys = ['QcObservationSummaryC',
                       'QcObservationSummaryD',
                       'QcObservationSummaryL',
                       'QcObservationSummaryS']
            # for each constellation
            for d in l_of_d:
                conname = d['id_constellation']
                con = {'identifier_3ch': conname}
                id_const = self.insert_constellation(session, con)
                d['id_constellation'] = id_const
                qobs = {}  # move obs to separate dict
                for k in obskeys:
                    qobs[k] = d.pop(k, [])

                # Sergio Bruni
                # If at least one of x/y/z_crd, x/y/z/_rms is set to None
                # do not insert row on qc_constellation_summary table, to avoid an exception,
                # since NULL value are not accepted on DB for those fields
                # the other two optins are:
                # 1) SET None values to zero
                # 2) TODO ACCEPT NULL values on DB
                if d['x_crd'] == None \
                    or d['y_crd'] == None \
                    or d['x_crd'] == None \
                    or d['x_rms'] == None \
                    or d['y_rms'] == None \
                    or d['z_rms'] == None:

                    self.logger.error(
                        '[{}] qc_constellation_summary not added because at least one of position fields is NULL: x_crd={}, y_crd={}, z_crd={}, x_rms={}, y_rms={}, z_rms={}' \
                            .format(self.rinex_filename, d['x_crd'], d['y_crd'], d['z_crd'], d['x_rms'], d['y_rms'], d['z_rms']))
                    # if could not create QcConstellationSummary then neither
                    # observation summaries can be inserted
                    continue
                else:
                    q = QcConstellationSummary(**d)
                    session.add(q)
                    session.flush()
                    id_q = q.id
                    self.logger.info(
                        '[{}] qc_constellation_summary.id: {}'.format(self.rinex_filename, id_q))
                #
                # Now insert all observation summaries as well:
                msg = self.insert_qcobservationsummaries(session, qobs, id_q)
                msgs = msg + ' ({})'.format(conname)
                self.logger.info('[{}] {}'.format(self.rinex_filename, msgs))
                self.infos.append(msgs.upper())
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e
        return True


    def insert_qcobservationsummaries(self, session, d_l_d, id_consum):
        """
        Insert all QC observation summaries for one constellation.
        Input d_l_d is a dict of the four obstypes C,D,L,S,
        each being a list of dicts for one specific 3char-obsname.
        """
        self.logger.info('[{}] Checking qc_observation_summary information'.format(self.rinex_filename))
        msg = None
        try:
            # no need to check existence,
            # unless we're updating (not implemented), or
            # unless same observation type present twice...
            # TODO: check that
            #       return not-None message in case of duplicates
            n_obs = 0
            for k in d_l_d:
                for d in d_l_d[k]:
                    self.logger.info('[{}] Checking {} information'.format(self.rinex_filename, k))
                    # update foreign keys
                    d['id_qc_constellation_summary'] = id_consum
                    id_go = self.insert_gnss_obsname_3ch(
                        session, d['id_gnss_obsnames'])
                    d['id_gnss_obsnames'] = id_go
                    # choose correct target table
                    if k == 'QcObservationSummaryC':
                        # Sergio Bruni. OLD WORKAROUND made before the delete of Not-Null constrain on cod_mpth field
                        # If cod_mph is set to None, set it to 0
                        # since NULL value are not accepted on DB for cod_mpth field of table qc_observation_summary_c
                        # the alternative is:
                        #
                        # if d['cod_mpth'] == None:
                        #     q = None
                        #     self.logger.warning('[{}] {} QcObservationSummaryC row has not been inserted because cod_mpth value is [n/a]' \
                        #                         .format(self.rinex_filename, self.qc['RinexFile']['name']))
                        #     #d['cod_mpth'] = 0
                        # else:
                        #   q = QcObservationSummaryC(**d)
                        q = QcObservationSummaryC(**d)
                    elif k == 'QcObservationSummaryD':
                        q = QcObservationSummaryD(**d)
                    elif k == 'QcObservationSummaryL':
                        q = QcObservationSummaryL(**d)
                    else:
                        q = QcObservationSummaryS(**d)

                    #if q:  Sergio Bruni. OLD WORKAROUND made before the delete of Not-Null constrain on cod_mpth field
                    session.add(q)
                    session.flush()

                    id_q = q.id
                    self.logger.info('[{}] {}.id: {}'.format(self.rinex_filename, k,id_q))
                    n_obs += 1
            msg = 'Inserted {} observation summaries for this constellation.'\
                .format(n_obs)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))
            session.rollback()
            raise e
        return msg
    
    
    def insert_gnss_obsname_3ch(self, session, s_obsname):
        """
        Get id of given 3char observation type from table gnss_obsnames,
        or insert new if not found.
        """
        # TODO: return message if new inserted, to show in final response.
        #       OR always return message if official==false.
        #self.logger.info('Checking gnss_obsnames information')
        try:
            #check existence
            Q = session.query(GnssObsname)\
                .filter_by(name=s_obsname)\
                .scalar()
            if Q:
                msg = '[{}] gnss_obsname was found: {}'.format(self.rinex_filename, Q.id)
                self.logger.info(msg)
                id_q = Q.id
            else:
                msg = '[{}] gnss_obsname not found, create new one now.'.format(self.rinex_filename)
                self.logger.info(msg)
                # Input might be unorthodox, but
                # we allow dynamic observation type insertion,
                # setting 'official' = false
                #
                if len(s_obsname) == 2 or s_obsname[2] == ' ':
                    chn = None
                else:
                    chn = s_obsname[2]
                q = GnssObsname(
                        name=s_obsname,
                        frequency_band=s_obsname[1],
                        obstype=s_obsname[0],
                        channel=chn,
                        official=False
                )

                session.begin_nested()

                try:
                    session.add(q)
                    session.commit()
                except IntegrityError as e:
                    session.rollback()
                    q = session.query(GnssObsname) \
                        .filter_by(name=s_obsname) \
                        .scalar()

                id_q = q.id
                self.logger.info('[{}] gnss_obsnames.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error("module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                              .format(
                                fname,
                                self.__class__.__name__,
                                inspect.currentframe().f_code.co_name,
                                exc_tb.tb_lineno,
                                str(e),
                                exc_type))

            session.rollback()
            raise e
        return id_q
    
    
    def insert_rinex_error(self, session, error_types):
        """
        Get id of given error type from table rinex_error_types,
        (insert new if not found),
        then link rinex file and error type entry using linking table
        rinex_errors.
        """
        if len(error_types['text']) == 0:
            return True
        try:
            for txt in error_types['text']:
                # check existence
                Q = session.query(RinexErrorType)\
                    .filter_by(error_type=txt)\
                    .scalar()
                if Q:
                    id_q = Q.id
                else:
                    msg = '[{}] rinex_error_type not found, create new one now.'.format(self.rinex_filename)
                    self.logger.info(msg)
                    q = RinexErrorType(error_type=txt)
                    session.add(q)
                    session.flush()
                    id_q = q.id
                    self.logger.info('[{}] rinex_error_types.id: {}'.format(self.rinex_filename, id_q))
                # create new link
                rinexerrors = RinexError(
                    id_rinex_file=error_types['rinex_file_id'],
                    id_error_type=id_q
                )
                session.add(rinexerrors)
                session.flush()
                self.logger.info('[{}] new rinex_errors.id: {}'.format(self.rinex_filename, id_q))
        except Exception as e:
            session.rollback()
            raise e
        return True



    def checked_copy(self, adict, key, val, item, t1, t2):
        """
        Check if dict 'adict' has key 'key',
        if yes print warning (or sth else in future),
        add key to dict with given value 'val'
        """

        key_date_range = key+".date_range"

        if key in adict:
            msg = ('WARNING (QC VALIDATION): Station has '
                   'multiple contemporary item/attribute '
                   'types: {}/{} duplicated date range: {} {} / {} {}') \
                .format(
                    item,
                    key,
                    adict[key_date_range]['beg'],
                    adict[key_date_range]['end'],
                    t1,
                    t2)
            self.warnings.append(msg.upper())
            self.logger.info(msg)
        else:
            adict[key_date_range] = {}
            # this should probably be changed to an error later...
        adict[key] = val

        adict[key_date_range]['beg'] = t1
        adict[key_date_range]['end'] = t2
        return adict
    
    
    def qc_error_handler(self, efix, etxt, spec,
                         rinexfile_status, error_types):
        """
        Form error message, add to self.{error,warning} for final output,
        update current rf status code, add DB rf error text to list,
        include error message in debug logger,
        return updated rf code and error message dict.
        
        efix = [elbl, t_qcv, k, t_fsc, stat]
            elbl = text: Error Label {'WARNING', 'ERROR'}
            t_qcv = text: '(QC VALIDATION)'
            k = text: key name, i.e. parameter name
            t_fsc = text: 'CODE'
            stat = integer: resulting status code
        etxt = main message string
        spec = additional specific info string (API response only)
        rinexfile_status = current status code
        error_types = dict with 'text' list field keeping all error texts
        """

        new_status = efix[4]
        if efix[4] == 0:
            new_status = 'UNCHANGED'

        omsg = '{} {}: {}: {} {}; {}: {}'.format(
                efix[0], efix[1], etxt, efix[2], spec, efix[3], new_status)

        rinexfile_status = self.check_rinex_file_status(
                rinexfile_status, efix[4], omsg)
        db_etxt = ('{}: {}: {}').format(efix[0], etxt, efix[2])
        error_types['text'].append(db_etxt)
        self.logger.info('[{}] {}'.format(self.rinex_filename, omsg))
        return rinexfile_status, error_types
    
    
    def qc_validate(self, check_table, qcxml, dbval,
                    rinexfile_id, rinexfile_status):
        """
        QC parameter value validation against DB and fixed values.
        
        Checks will be performed according to definitions in check_table.
        If a parameter checks out fine, nothing happens.
        If something is amiss (missing value, unexpected None value, value
        mismatch or too large deviation), a specific error/warning message
        will be stored for web-service output and DB-storage linked to the
        corresponding RINEX file entry.
        
        INPUT:
            check_table = dict: validation definitions
            qcxml = dict: QC parameter values
            dbval = dict: corresponding DB values
            rinexfile_id = int: id of current rinex_file
            rinexfile_status = int: status code of current rinex_file
        OUTPUT:
            rinexfile_status = int: new status code of current rinex_file
            error_types = dict: rinex_file errors to index
        
        check_table contents/format:
            check_table = {
                key: [fmt, expval, operator, tolerance, stat, elbl, etext],
            }
            MEANING:
                key = parameter name as in qcxml[key], maybe dbval[key]
                fmt = type/format to cast, e.g. 'float', 'unicode', 'int'
                expval = expected value (fixed value, or None if dbval[key])
                operator = comparison operator string, can be:
                    'eq' = equal,         == (both numeric or string comp.)
                    'gt' = greater than,  >
                    'ge' = greater equal, >=
                    'lt' = less than,     <
                    'le' = less equal,    <=
                    QCXML value always left of operator (success condition)!
                tolerance = numerical comparison slack, e.g. 0, 30
                stat = status code in case of failed check, e.g. 2, 3, -3
                elbl = error label, e.g. 'ERROR', 'WARNING'
                etext = general explanation of failed check, e.g.:
                    etext = 'PARAMETER DIFFERENCE TOO LARGE'
                    etext = 'PARAMETER NOT FOUND IN DATABASE'
                    etext = 'PARAMETERS DO NOT MATCH'
                    etext = 'DAqc_validateTA ISSUE - NONZERO VALUE'
                
        Examples:
            'x_crd' : ['float', None, 'eq', 30, 2, 'WARNING', txt_bigdiff],
            --> abs(qcxml['x_crd'] - dbval['x_crd']) > 30: error...
            'xint'  : [ 'int' , 0   , 'gt', 0 , 2, 'WARNING', txt_nonz],
            --> qcxml['xint'] > (0+0): error...
        """
        # initialize dict for all encountered errors to index for this
        # rinex file after all checks:
        error_types = {
            'rinex_file_id': rinexfile_id,
            'text': []
        }
        # initialize some repeated fixed values/text:
        t_qcv = '(QC VALIDATION)'
        t_fsc = 'STATUS'

        # go through each check item:
#        for k, dlist in check_table.iteritems():
        for k, dlist in check_table.items():
            kfmt = dlist[0]
            expv = dlist[1]
            optr = dlist[2]
            tolr = dlist[3]
            etxt = dlist[6]
            efix = [dlist[5], t_qcv, k, t_fsc, dlist[4]]
            spec = ''
            # check if qc parameter obtainable
            if k not in qcxml:
                etxt = 'PARAMETER NOT FOUND IN QC-XML'
                rinexfile_status, error_types = self.qc_error_handler(
                        efix, etxt, spec, rinexfile_status, error_types)
                continue
            qcval = qcxml[k]
            # check if expected reference value fixed or from DB
            if expv is None:
                # from DB: check if DB value exists
                if k not in dbval:
                    etxt = 'PARAMETER NOT FOUND IN DATABASE'
                    rinexfile_status, error_types = self.qc_error_handler(
                            efix, etxt, spec, rinexfile_status, error_types)
                    continue
                expv = dbval[k]
                # check if DB value None --> cannot compare
                if (expv == None) and ((qcval == None) or (qcval == "")):
                    # but if both QC and DB values are None/empty, succeed
                    omsg = '[{}] {}: QC AND DB/REF BOTH NONE/EMPTY' \
                        .format(qcxml['file_name'],k)
                    self.logger.info(omsg)
                    continue
                elif expv is None:
                    etxt = 'PARAMETER IN DATABASE IS NONE'
                    spec = ': {}(QC), {}(DB/REF)'.format(qcval, expv)
                    rinexfile_status, error_types = self.qc_error_handler(
                            efix, etxt, spec, rinexfile_status, error_types)
                    continue
            # Cast QC and DB values to required format if not already correct.
            #   Can be necessary as QC numbers are originally type 'str',
            #   and DB float numbers are actually type 'decimal'.
            if kfmt == "float":
                if not isinstance(qcval, float):
                    qcval = float(qcval)
                if not isinstance(expv, float):
                    expv = float(expv)
            elif kfmt == "int":
                if not isinstance(qcval, int):
                    qcval = int(qcval)
                if not isinstance(expv, int):
                    expv = int(expv)
            elif kfmt == "str" or kfmt == "unicode":
#                if not isinstance(qcval, (str, unicode)):
                if not isinstance(qcval, str):
                    qcval = str(qcval)
#                if not isinstance(expv, (str, unicode)):
                if not isinstance(expv, str):
                    expv = str(expv)
            else:
                etxt = 'UNRECOGNIZED INTENDED FORMAT ({})'.format(kfmt)
                rinexfile_status, error_types = self.qc_error_handler(
                        efix, etxt, spec, rinexfile_status, error_types)
                continue
            # check operator itself:
            if not any(x==optr for x in ['eq','gt','ge','lt','le']):
                etxt = 'INVALID OPERATOR ({})'.format(optr)
                rinexfile_status, error_types = self.qc_error_handler(
                        efix, etxt, spec, rinexfile_status, error_types)
                continue
            # Choose comparison operation and apply it.
            # String types are special: only 'eq' comparison allowed.
            if kfmt == "str":   # or kfmt is 'unicode':
                # simple string comparison
                if optr == 'eq':
                    # Sergio Bruni 25-04-2019
                    # do not consider leading zeroes
                    clean_qcval = qcval.lstrip('0')
                    clean_expv = expv.lstrip('0')

                    if clean_qcval.upper() == clean_expv.upper():
                        omsg = '[{}] {}: {}(QC), {}(DB/REF) # SAME'.format(
                            qcxml['file_name'], k, qcval, expv)
                        self.logger.info(omsg)
                        continue
                    else:
                        spec = ': {}(QC), {}(DB/REF)'.format(qcval, expv)
                        rinexfile_status, error_types = self.qc_error_handler(
                            efix, etxt, spec, rinexfile_status, error_types)
                        continue
                else:
                    etxt = 'CANNOT OPERATE ({}) ON FORMAT ({})'.format(
                            optr, kfmt)
                    rinexfile_status, error_types = self.qc_error_handler(
                            efix, etxt, spec, rinexfile_status, error_types)
                    continue
            else:
                # numerical comparison with tolerance
                diff = qcval - expv
                # for relative diff, handle divide-by-zero cases:
                if expv == 0.0:
                    if abs(diff) > 0:
                        d_rel = float('inf')
                    else:
                        d_rel = 0.0
                else:
                    d_rel = diff/expv*100.0
                spec = '{}(QC) - {}(DB/REF) = {}, {} %'.format(
                        qcval, expv, round(diff, 3), round(d_rel, 2) )
                omsg = '[{}] {}: {}'.format(qcxml['file_name'], k, spec)
                # apply operator:
                if not (    (optr == 'eq' and abs(diff) <= tolr)
                         or (optr == 'gt' and qcval >  expv - tolr)
                         or (optr == 'ge' and qcval >= expv - tolr)
                         or (optr == 'lt' and qcval <  expv + tolr)
                         or (optr == 'le' and qcval <= expv + tolr)
                       ):
                    rinexfile_status, error_types = self.qc_error_handler(
                        efix, etxt, spec, rinexfile_status, error_types)
                    continue
                self.logger.info(omsg)
        return rinexfile_status, error_types
    
    
    def get_json_msg(self, adict=None):
        """
        Fill error/warning/info messages into JSON struct,
        also add given dict 'adict' to JSON struct, and return it.
        Clears out all stored messages.
        """
        response = {
            'messages': {
                'info': self.infos,
                'warning': self.warnings,
                'error': self.errors
            }
        }
        if adict:
            response['other'] = adict
        json_response = json.dumps(response, cls=JSONCustomEncoder)
        # Clear all messages of current session:
        self.infos = []
        self.warnings = []
        self.errors = []
        #
        return json_response


    def qc_result_text(self):
        """
        Fill error/warning messages into text string
        formatted in an easy-to-grep-way
        """
        text_msgs = []
        text_msg = ''

        if self.warnings:
            w = [re.search('(?:(?:WARNING|ERROR) \(QC VALIDATION\):)?\s*(.*)', text).group(1) for text in self.warnings]
            # w = []
            # for text in self.warnings:
            #     s = re.search('(?:WARNING \(QC VALIDATION\):)?\s*(.*)', text)
            #     if s:
            #         w.append(s.group(1))
            text_msgs.append('WARNINGS: ' + ' | '.join(w))

        if self.errors:
            t = [re.search('(?:ERROR \(QC VALIDATION\):)?\s*(.*)', text).group(1) for text in self.errors]
            text_msgs.append('ERRORS: ' + ' | '.join(t))

        text_msg = ' | '.join(text_msgs) + ' |'
        return text_msg

    
    def check_rinex_file_status(self, old, new, msg=None):
        """
        Decide whether new status code should override old one based on
        severity hierarchy:
            -3 = severe T3-T1/2 mismatch ERROR
            -2 = severe but not as much as -3 (added by Sergio Bruni 03/05/2019)
            2  = severe T3-T1/2 mismatch WARNING
            3  = QC data WARNING
            1  = check successful, all is well
            0  = not checked yet
        Log message accordingly.
        Return resulting (new) code.
        """
        rinexfile_status = old

        # Sergio Bruni 24/04/2017
        # add warning also if called with new=0
        # this happen when we want to WARN without change the status_code
        if new == 0 or new == 2 or new == 3 :
            # store WARNING status message, regardless of whether this
            # updates the rinex file status code (may already be same/worse).
            self.warnings.append(msg.upper())
        #
        if new == -3:
            # ERROR status, major T3-T1/2 mismatch,
            # also store error message.
            rinexfile_status = new
            self.errors.append(msg.upper())
        elif new == -2 and not rinexfile_status == -3:
            # ERROR status...,
            # also store error message.
            rinexfile_status = new
            self.errors.append(msg.upper())
        elif (new == 2 and not
                (rinexfile_status == -3 or
                 rinexfile_status == -2)):
            # WARNING, not ERROR, minor T3-T1/2 mismatch
            rinexfile_status = new
        elif (new == 3 and not
              (rinexfile_status == -3 or
               rinexfile_status == -2 or
               rinexfile_status == 2)):
            # WARNING, not ERROR, no mismatch, but major QC issues
            rinexfile_status = new
        elif (new == 1 and not
              (rinexfile_status == -3 or 
               rinexfile_status == -2 or
               rinexfile_status == 2 or
               rinexfile_status == 3)):
            # OK, no WARNING or ERROR, file and data check out fine
            rinexfile_status = new
        # else: no change from current status necessary.
        return rinexfile_status
    
    
    def update_final_rinex_file_status(
            self, session, rinexfile, rinexfile_status):
        """
        Prepare and log message about final RINEX file status code.
        Update the status code of the given rinex_file object.
        """
        msg1 = 'Final RINEX file status code is: '
        if rinexfile_status == -3:
            msg2 = '-3 (ERROR: major file header - station info mismatch)'
        elif rinexfile_status == -2:
            msg2 = '-2 (ERROR: QC-XM not valid)'
        elif rinexfile_status == 2:
            msg2 = '2 (WARNING: minor file header - station info mismatch)'
        elif rinexfile_status == 3:
            msg2 = '3 (WARNING: major QC issues, bad data indicated)'
        elif rinexfile_status == 1:
            msg2 = '1 (OK: file checked out fine)'
        else:
            msg = 'ERROR: unexpected final RINEX file status: {}'.format(
                    rinexfile_status)
            session.rollback()
            self.logger.info('[{}] {}'.format(rinexfile.name, msg))
            self.errors.append(msg.upper())
            return self.get_json_msg(), OK
        # Log the final status message:
        msg = msg1 + msg2
        self.logger.info('[{}] {}'.format(rinexfile.name, msg))
        self.infos.append(msg.upper())
        # Update RINEX file status:
        rinexfile.status = rinexfile_status
        session.flush()
        return True

    def manage_invalid_xml(self, xml):
        with closing(self.session()) as session:
            md5uncompressed = xml['QC_GNSS']['head']['file_md5sum']
            file_name = xml['QC_GNSS']['head']['file_name']
            self.rinex_filename = file_name
            try:
                rinexfile = session.query(RinexFile) \
                    .filter(RinexFile.md5uncompressed == md5uncompressed) \
                    .one_or_none()
            except MultipleResultsFound:
                status = '[{}] md5uncompressed "{}" result duplicated inside the database' \
                    .format( file_name,
                        md5uncompressed)
                self.logger.error(status)
                self.rinex_filename = None
                return status, CONFLICT

            if not rinexfile:
                msg = '[{}] md5uncompressed "{}" ERROR: No RINEX file for given QC file registered!' \
                    .format(file_name,
                        md5uncompressed)
                self.logger.error(msg)
                self.errors.append(msg.upper())
                return self.get_json_msg(), CONFLICT

            error_types = {
                'rinex_file_id': rinexfile.id,
                'text': []
            }

            tup = self.qc_error_handler(['ERROR', '(QC VALIDATION)', '', 'STATUS', -2],
                                    'NOT VALID QC-XML FILE',
                                    '',
                                    0,
                                    error_types)

            rinexfile_status, error_types = tup

            ## Update RINEX file status:
            rinexfile_status = self.check_rinex_file_status(
                    rinexfile_status, 1)  # is "1" if no problems encountered
            self.update_final_rinex_file_status(
                    session, rinexfile, rinexfile_status)

            ## Link errors to RINEX file if any:
            self.insert_rinex_error(session, error_types)

            try:
                session.commit()
            except Exception as e:
                session.rollback()
                raise e

        msg = self.qc_result_text()

        if self.errors:
            self.qc_logger.error('{} {} {}'.format(file_name, rinexfile_status, msg))
        elif self.warnings:
            self.qc_logger.warning('{} {} {}'.format(file_name, rinexfile_status, msg))

        self.rinex_filename = None
        return self.get_json_msg(), CONFLICT



    @profile
    def qc_post(self, body):
        """
        Insert posted QC XML to database.
        Requires QC XML from Anubis v2.1.3 or newer! (md5sum issue)
        """
        myname = sys._getframe().f_code.co_name
        self.logger.info(myname + '() was called')
        # Parse the input text and get ordered dict for DB tables

        try:
            xml = xmltodict.parse(body)  # have it as dict now
        except Exception as e:
            status = 'UNCORRECT XML INPUT DATA'
            self.logger.error(status)
            return status, CONFLICT
        try:
#todo pass only parse_qcxml(xml, file_size, md5_checksum), not the body
            qc = parse_qcxml(xml, body)

        except Exception as e:
            #xml = xmltodict.parse(body)  # have it as dict now
            self.logger.error(sys.exc_info())
            return self.manage_invalid_xml(xml)

        # Sergio Bruni.
        # reason: All methods needs to know the rinex_file_name to write it in log messages
        self.qc = qc

        # dynamically update table for gnss_obsnames:
        # insert obstype if "new", later can check if actually listed in
        # RINEX 3.03 doc.
        #
        # - cross-validation partly done
        #
        self.logger.info('Have parsed QC XML text to dict.')
        #
        qf = qc['QualityFile']
        rf = qc['RinexFile']
        hd = qc['Header']
        qp = qc['QcParameter']
        qr = qc['QcReportSummary']
        qnm = qc['QcNavigationMsg']
        qcs = qc['QcConstellationSummary']
        oth = qc['QcOthers']
        hdsys = qc['hdsys']

        # all function will use this for logging filename
        self.rinex_filename = rf['name']

        with closing(self.session()) as session:
            # Find RINEX file ID
            try:
                rinexfile = session.query(RinexFile)\
                    .filter(RinexFile.md5uncompressed == rf['md5checksum'])\
                    .one_or_none()
            except MultipleResultsFound:
                status = '[{}] md5uncompressed "{}" result duplicated inside the database'.format(
                    rf['name'],
                    rf['md5checksum'])
                self.logger.error(status)
                self.rinex_filename = None
                return status, CONFLICT

            if not rinexfile: # 2 cases: # - new file # - old file with different md5checksum (1 IndexGD, 2 modify RNX, 3 RunQC)
                msg = '[{}] ERROR: No RINEX file have this md5checksum, run IndexGD on the file to index it or to update it' \
                    .format(rf['name'])
                self.logger.info(msg)
                self.errors.append(msg.upper())
                self.rinex_filename = None
                return self.get_json_msg(), CONFLICT
            # Keep the rinexfile object until status update in the end:
            rinexfile_id = rinexfile.id
            qf['id_rinexfile'] = rinexfile_id
            qr['id_rinexfile'] = rinexfile_id

            # VALIDATION of T3 header with T1/2 values
            # checking:
            # file_format: RINEX2, RINEX3
            # site_id: <4-letter station marker>, RHOF
            # marker_numb: <station marker number>, RHOF
            # # marker_numb == station.iers_domes
            # marker_name: 
            # receiver_type: <type str>, TRIMBLE NETR9
            # receiver_numb: <rec numb>, 5038K70713
            # receiver_vers: 'NP 4.60 / SP 4.60' vs. 'NP4.85/SP4.85'
            # antenna_type: <type str>, TRM57971.00
            # antenna_numb: <ant numb>, 1441045161
            # antenna_dome: <type str>, ''
            # # which value is this?
            # software: <softw str>, teqc 2010Oct21
            # # not indexed by indexGD.py !!!
            # sampling interval (inverse frequency): 3 values to compare:
            #     QC: meta.data_int: <float>, 30.000 *ignore this one*
            #     QC: data.data_smp: <float>, 30.00,
            #     QC: head.data_int: <float>, 0.000, [optional in RINEX2]
            #     DB: file_type.sampling_frequency
            # #*NOTE: <head><data_int> can be 0.000, as in RINEX2 header
            # #     data_int is optional, so use <data><data_int> instead for
            # #     file_type check, as that is estimated from data.
            # sampling window (total data length): 3 values to compare:
            #     QC: date_beg & date_end,
            #     QC: data_smp & numb_epo, [can detect wrong sampling here]
            #     DB: file_type.sampling_window 
            # x/y/z_crd: <3 float>, 2456169.367 -701823.634 5824743.206
            # ecc_n/e/u: <3 float>, 0.000 0.000 1.014
            # # 
            #
            # Query:
            self.logger.info('[{}] Querying station info by rinex id {}'.format(
                    rf['name'],
                    rinexfile_id))

            # Sergio Bruni test query, made to see the sql code generated by Sqlalchemy
            # q = session.query(
            #     RinexFile,
            #     FileType,
            #     Station,
            #     Coordinates,
            #     Monument,
            #     StationItem,
            #     Item,
            #     ItemType,
            #     ItemAttribute,
            #     Attribute
            #     )\
            #         .join(FileType)\
            #         .join(Station)\
            #         .join(Location)\
            #         .join(Coordinates)\
            #         .join(Monument)\
            #         .join(StationItem)\
            #         .join(Item)\
            #         .join(ItemAttribute)\
            #         .join(ItemType)\
            #         .join(Attribute)\
            #         .filter(RinexFile.id==rinexfile_id)\
            #         .filter(or_(
            #             ItemType.name=='antenna',
            #             ItemType.name=='receiver',
            #             ItemType.name=='radome',
            #             ))\
            #         .filter(or_(
            #             Attribute.name=='antenna_type',
            #             Attribute.name=='marker_arp_up_ecc',
            #             Attribute.name=='marker_arp_north_ecc',
            #             Attribute.name=='marker_arp_east_ecc',
            #             Attribute.name=='receiver_type',
            #             Attribute.name=='serial_number',
            #             Attribute.name=='radome_type',
            #             Attribute.name=='elevation_cutoff_setting',
            #             Attribute.name=='satellite_system',
            #             ))
            #
            # print str(q.statement.compile(dialect=postgresql.dialect()))
            #print str(q)


            query_list = session.query(
                RinexFile,
                FileType,
                Station,
                Coordinates,
                Monument,
                StationItem,
                Item,
                ItemType,
                ItemAttribute,
                Attribute
                )\
                    .join(FileType)\
                    .join(Station)\
                    .join(Location)\
                    .join(Coordinates)\
                    .join(Monument)\
                    .join(StationItem)\
                    .join(Item)\
                    .join(ItemAttribute)\
                    .join(ItemType)\
                    .join(Attribute)\
                    .filter(RinexFile.id==rinexfile_id)\
                    .filter(or_(
                        ItemType.name=='antenna',
                        ItemType.name=='receiver',
                        ItemType.name=='radome',
                        ))\
                    .filter(or_(
                        Attribute.name=='antenna_type',
                        Attribute.name=='marker_arp_up_ecc',
                        Attribute.name=='marker_arp_north_ecc',
                        Attribute.name=='marker_arp_east_ecc',
                        Attribute.name=='receiver_type',
                        Attribute.name=='serial_number',
                        Attribute.name=='radome_type',
                        Attribute.name=='elevation_cutoff_setting',
                        Attribute.name=='satellite_system',
                        ))\
                    .all()
            #

            if not query_list:
                # No result, no RINEX file
                msg = '[{}] ERROR: No RINEX file for given QC file registered!' \
                    .format(rf['name'])
                self.logger.info(msg)
                self.errors.append(msg.upper())
                self.rinex_filename = None
                return self.get_json_msg(), CONFLICT
            #
            # turn query_list tuple objects to list of dicts
            t_list = querylist2dictlist(query_list)
            #
            # check file status:
            rinexfile_status = t_list[0]['rinex_file']['status']

            # Sergio Bruni 9/2/2019
            # I have commented the six following line. This way the current QC data (if they are present)
            # will be always updated with the new ones
            # todo Verify if this is correct
            # if not -3 < rinexfile_status < 1:
            #     msg = 'ERROR: File already has QC info, status = {}'.format(
            #                rinexfile_status)
            #     self.logger.info(msg)
            #     self.errors.append(msg)
            #     return self.get_json_msg(), OK
            #

            # Sergio Bruni 9/2/2019
            # since now qc validation can redone more time rinexfile_status is reset to 0
            rinexfile_status = 0

            #
            # reference date/time for this RINEX file by QC-XML:
            ref_time = datetime.strptime(
                qc['QcReportSummary']['date_beg'], "%Y-%m-%dT%H:%M:%S")
            # Collect T1/2 parameter values for verification:
            T12 = {}
            T12['site_id'] = t_list[0]['station']['marker']
            T12['marker_numb'] = t_list[0]['station']['iers_domes']
            T12['file_format'] = t_list[0]['file_type']['format']
            T12['file_name'] = t_list[0]['rinex_file']['name']
            T12['x_crd'] = t_list[0]['coordinates']['x']
            T12['y_crd'] = t_list[0]['coordinates']['y']
            T12['z_crd'] = t_list[0]['coordinates']['z']

            # go through station items to find wanted parameters:
            for d in t_list:
                vv = d['item_attribute']['value_varchar']
                vn = d['item_attribute']['value_numeric']
                t1 = d['item_attribute']['date_from']
                t2 = d['item_attribute']['date_to']
                # date_to can be NULL if open-ended, set to high value
                if not t2:
                    t2 = datetime(9999, 12, 30)
                # only accept entries relevant for our current RINEX file
                if not t1 <= ref_time <= t2:  # based on times
                    continue
                if d['item_type']['name'] == 'antenna':
                    if d['attribute']['name'] == 'antenna_type':
                        T12 = self.checked_copy(T12, 'antenna_type', vv, 'antenna', t1, t2)
                        continue
                    if d['attribute']['name'] == 'serial_number':
                        T12 = self.checked_copy(T12, 'antenna_numb', vv, 'antenna', t1, t2)
                        continue
                    if d['attribute']['name'] == 'marker_arp_up_ecc':
                        T12 = self.checked_copy(T12, 'ecc_u', vn, 'antenna', t1, t2)
                        continue
                    if d['attribute']['name'] == 'marker_arp_north_ecc':
                        T12 = self.checked_copy(T12, 'ecc_n', vn, 'antenna', t1, t2)
                        continue
                    if d['attribute']['name'] == 'marker_arp_east_ecc':
                        T12 = self.checked_copy(T12, 'ecc_e', vn, 'antenna', t1, t2)
                        continue
                if d['item_type']['name'] == 'receiver':
                    if d['attribute']['name'] == 'receiver_type':
                        T12 = self.checked_copy(T12, 'receiver_type', vv, 'receiver', t1, t2)
                        continue
                    if d['attribute']['name'] == 'serial_number':
                        T12 = self.checked_copy(T12, 'receiver_numb', vv, 'receiver', t1, t2)
                        continue
                    if d['attribute']['name'] == 'firmware_version':
                        T12 = self.checked_copy(T12, 'receiver_vers', vv, 'receiver', t1, t2)
                        continue
                    if d['attribute']['name'] == 'elevation_cutoff_setting':
                        T12 = self.checked_copy(T12, 'elev_cutoff', vn, 'receiver', t1, t2)
                        ###  NOSENSE -> T12['obs_elev'] = T12['elev_cutoff']
                        continue
                    if d['attribute']['name'] == 'satellite_system':
                        T12 = self.checked_copy(T12, 'satellite_system', vv, 'receiver', t1, t2)
                        continue
                if d['item_type']['name'] == 'radome':
                    if d['attribute']['name'] == 'radome_type':
                        T12 = self.checked_copy(T12, 'antenna_dome', vv, 'radome', t1, t2)
                        # issue: QC-XML can have None type, DB 'NONE' string!
                        continue
            #
            # assemble qc parameter set:
            qcxml = hd  # qc header parameters
            qcxml['elev_cutoff'] = qc['QcParameter']['elevation_cutoff']
            qcxml.update(qr) # add qc report summary parameters
            # QCXML sampling interval info:
            #qcxml <-- hd['data_int'] <head><data_int>
            #qcxml <-- qr['data_smp'] <data><data_int>
            qcxml.update(oth)  # numb_epo, numb_gap
            qcxml['file_name'] = rf['name']
            #
            ## Derive values which cannot be compared directly:
            # Minimum requested sampling:
            # # from QC XML:
            time1 = datetime.strptime(qcxml['date_beg'],"%Y-%m-%dT%H:%M:%S")
            time2 = datetime.strptime(qcxml['date_end'],"%Y-%m-%dT%H:%M:%S")
            dtime = time2-time1
            qc_len_beg_end = dtime.total_seconds()
            qc_len_smp_epo = float(qcxml['data_smp'])*float(qcxml['numb_epo'])
            qcxml['data_len'] = qc_len_beg_end
            qcxml['data_len2'] = qc_len_smp_epo
            qcxml['ratio'] = float(qr['obs_have'])/float(qr['obs_expt'])
            # RINEX file name: Remove extensions (nontrivial for RINEX2)
            fn = qcxml['file_name']
            if len(fn) > 34:  # RINEX3 observation file
                qcxml['file_name'] = fn[0:34]
            elif len(fn) > 30:  # RINEX3 navigation file
                qcxml['file_name'] = fn[0:30]
            else:  # probably RINEX2 file
                # ABCD1234.56O == ABCD1234.56D.Z
                if re.match(r'\w{4}\d{2}\w{3}', fn) and len(fn) > 15:
                    # 9char station name detected
                    qcxml['file_name'] = fn[0:15]
                else:
                    # assume just 4char station name
                    qcxml['file_name'] = fn[0:11]
            #
            # # from DB:
            # entire data length in seconds from RINEX metadata (DB):
            db_length = t_list[0]['file_type']['sampling_window']
            if re.match(r'^(\d{1,2})(h)(r|our)*$', db_length.lower()):
                str_tlen = re.search(r'^(\d{1,2})', db_length).group(1)
                rf_md_len = 3600.0 * float(str_tlen)
            elif re.match(r'^(\d{1,2})(m)(in)*$', db_length.lower()):
                str_tlen = re.search(r'^(\d{1,2})', db_length).group(1)
                rf_md_len = 60.0 * float(str_tlen)
            elif re.match(r'^\d{1,2}d$', db_length.lower()):
                str_tlen = re.search(r'^(\d{1,2})', db_length).group(1)
                rf_md_len = 86400.0 * float(str_tlen)
            else:
                # this is unexpected ...
                rf_md_len = None
            T12['data_len'] = rf_md_len
            # data sampling interval in seconds from RINEX metadata (DB):
            db_interv = t_list[0]['file_type']['sampling_frequency']
            if re.match(r'^(\d{1,2})(hz|z)*$', db_interv.lower()):
                str_tlen = re.search(r'^(\d{1,2})', db_interv).group(1)
                rf_md_smp = 1.0 / float(str_tlen)
            elif re.match(r'^(\d{1,2})(s)(ec)*$', db_interv.lower()):
                str_tlen = re.search(r'^(\d{1,2})', db_interv).group(1)
                rf_md_smp = float(str_tlen)
            else:
                # this is unexpected ...
                rf_md_smp = None
            T12['data_smp'] = rf_md_smp
            # satellite systems enabled by receiver:
            T12['satsys'] = []
            if 'satellite_system' in T12 and T12['satellite_system']:
                # separate them, turn to 3ch code, make list
                x = (re.sub(r'[,+]',' ',T12['satellite_system'])).split()
                for b in x:
                    c = re.sub(r'GLONASS','GLO',b.upper())
                    c = re.sub(r'SBAS','SBS',c)
                    c = re.sub(r'QZSS','QZS',c)
                    c = re.sub(r'IRNSS','IRN',c)
                    c = re.sub(r'GALILEO','GAL',c)
                    c = re.sub(r'BEIDOU2?','BDS',c)
                    T12['satsys'].append(c)
            T12['satsys'].append('GNS')
            # RINEX file name: Remove extensions (nontrivial for RINEX2)
            fn = T12['file_name']
            if len(fn) > 34:  # RINEX3 observation file
                T12['file_name'] = fn[0:34]
            elif len(fn) > 30:  # RINEX3 navigation file
                T12['file_name'] = fn[0:30]
            else:  # probably RINEX2 file
                # ABCD1234.56O == ABCD1234.56D.Z
                if re.match(r'\w{4}\d{2}\w{3}', fn) and len(fn) > 15:
                    # 9char station name detected
                    T12['file_name'] = fn[0:15]
                else:
                    # assume just 4char station name
                    T12['file_name'] = fn[0:11]
            #
            ####
            tPDNM = 'PARAMETERS DO NOT MATCH'
            tPDTL = 'PARAMETER DIFFERENCE TOO LARGE'
            tDINV = 'DATA ISSUE - NONZERO VALUE'
            tDCCI = 'DATA CONSISTENCY/COMPLETENESS ISSUE'
            tDOHE = 'DATA OBS HAVE/EXPT RATIO < 0.4'
            tDSI1 = 'DATA SAMPLING ISSUE: QC DATA VS T2 MISMATCH'
            tDSI2 = 'DATA SAMPLING ISSUE: QC HEAD VS QC DATA MISMATCH'
            tDLI1 = 'DATA LENGTH ISSUE: QC BEG-END VS T2 MISMATCH'

            # Sergio NOT USED anymore
            # tDLI2 = 'DATA LENGTH ISSUE: QC BEG-END VS QC SMP*EPO MISMATCH'

            tERR = 'ERROR'
            tWRN = 'WARNING'
            # key: [fmt, expval, operator, tolerance, stat, elbl, etext],
            check_table = {
                'file_format':   ['str', None, 'eq', None , -3, tERR, tPDNM],
                'file_name':     ['str', None, 'eq', None ,  2, tERR, tPDNM],
                'antenna_type':  ['str', None, 'eq', None , -3, tERR, tPDNM],
                'receiver_type': ['str', None, 'eq', None , -3, tERR, tPDNM],
                'antenna_dome':  ['str', None, 'eq', None , -3, tERR, tPDNM],
                'antenna_numb':  ['str', None, 'eq', None ,  2, tWRN, tPDNM],
                'receiver_numb': ['str', None, 'eq', None ,  2, tWRN, tPDNM],
                'site_id':       ['str', None, 'eq', None ,  2, tWRN, tPDNM],
                'marker_numb':   ['str', None, 'eq', None ,  2, tWRN, tPDNM],
                'ecc_n':       ['float', None, 'eq',0.0001, -3, tERR, tPDTL],
                'ecc_e':       ['float', None, 'eq',0.0001, -3, tERR, tPDTL],
                'ecc_u':       ['float', None, 'eq',0.0001, -3, tERR, tPDTL],
                'x_crd':       ['float', None, 'eq', 100   ,  2, tWRN, tPDTL],
                'y_crd':       ['float', None, 'eq', 100   ,  2, tWRN, tPDTL],
                'z_crd':       ['float', None, 'eq', 100   ,  2, tWRN, tPDTL],
                'data_smp':    ['float', None, 'eq', 0.001,  2, tWRN, tDSI1],
                'data_int': 
                    ['float', qcxml['data_smp'], 'eq', 0.001, 2, tWRN, tDSI2],
                'data_len':
                    ['float', T12['data_len']/2, 'ge', 0,  2, tWRN, tDLI1],
                'data_len2':
                    ['float', T12['data_len'] / 2, 'ge', 0, 2, tWRN, tDLI1],
                'elev_cutoff': ['float', None, 'ge', 2.5,  2, tWRN, tPDTL],
                'xint':          ['int', 0   , 'eq', 0    ,  3, tWRN, tDINV],
                'xbeg':          ['int', 0   , 'eq', 0    ,  3, tWRN, tDINV],
                'xend':          ['int', 0   , 'eq', 0    ,  3, tWRN, tDINV],
                'xsys':          ['int', 0   , 'eq', 0    ,  3, tWRN, tDINV],
                'clk_jmps':      ['int', 0   , 'eq', 0    ,  3, tWRN, tDINV],
                'numb_epo':      ['int', 60  , 'ge', 0    ,  3, tWRN, tDCCI],
                'numb_gap':      ['int',  5  , 'le', 0    ,  3, tWRN, tDCCI],
                'cyc_slps':      ['int', 3000 , 'le', 0    ,  3, tWRN, tDCCI],
                'obs_elev':    ['float', 10  , 'le', 3    ,  3, tWRN, tDCCI],
                'ratio':       ['float', 0.4 , 'ge', 0    ,  3, tWRN, tDOHE],
            }
            # go through checklist:
            rinexfile_status, error_types = self.qc_validate(
                    check_table, qcxml, T12, rinexfile_id, rinexfile_status)
            #
            ## Checks for each satellite system:
            #  RINEX3 header is always system-specific,
            #  RINEX2 header can have global summary 'GNS' if multiple systems,
            #  both data sections have system-specific position estimates.
            obsts = ['QcObservationSummaryC', 'QcObservationSummaryD',
                     'QcObservationSummaryL', 'QcObservationSummaryS']
            p_xyz = ['x_crd', 'y_crd', 'z_crd']
            r_xyz = ['x_rms', 'y_rms', 'z_rms']
            eX0 = 'WARNING'
            eX1 = '(QC VALIDATION)'
            eX3 = 'STATUS'
            thr_SPP = 30  # threshold for SPP coordinates [m]
            thr_rms = 30.0  # threshold for SPP rms deviation [m]
            thr_rms_gps_1 = 300.0  # threshold for SPP rms deviation [m] before 2000-05-01, (i.e. DoY 122 including)
            thr_rms_gps_2 = 50.0  # threshold for SPP rms deviation [m] as of 2000-05-02, (i.e. DoY 123 including)
            thr_nsat = 6  # threshold for number of satellites

            GLOBAL_SYSTEM = ['GPS', 'GLO', 'BDS', 'GAL']
            # Sergio Bruni 16-04-2019 the check on xcod_sat is no longer considered usefull
            # thr_xsat = 1500  # threshold for excluded satellites

            # all <head> constellation and obs:
            # First check if RINEX2 or RINEX3 header:
            # Sergio Bruni 'GNS' seems NOT to be used (DEPRECATED???)
            if 'GNS' in hdsys:
                # most likely RINEX2 file with global obs info in header,
                # create list of all data observations (any system)
                dobs2 = []
                for cons in qcs:
                    for g in obsts:
                        if cons[g]:  # type D can be empty
                            for d in cons[g]:
                                dobs2.append(d['id_gnss_obsnames'])
                # check them with header
                etxt = 'MISSING OBSERVATION DATA (GNS)'
                spec = ''
                for g in hdsys['GNS']:  # obs in header
                    if g not in dobs2:  # but not in data
                        efix = [eX0,eX1,g,eX3,2]
                        tup = self.qc_error_handler(
                                efix, etxt, spec,
                                rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
                # system-specific checks based on data section (RINEX2)
                for cons in qcs:
                    k = cons['id_constellation']
                    # check if single point position (SPP) same
                    if cons['x_crd']:  # SPP data can be all None
                        etxt = 'LARGE SPP HEAD-SYS DIFFERENCE ('+k+')'
                        for g in p_xyz:
                            d = float(hd[g]) - float(cons[g])
                            if abs(d) - thr_SPP > 0:
                                efix = [eX0,eX1,g,eX3,2]
                                spec = (': <head> {} - <system> {} '
                                        '= {}').format(hd[g], cons[g], d)
                                tup = self.qc_error_handler(
                                        efix, etxt, spec,
                                        rinexfile_status, error_types)
                                rinexfile_status, error_types = tup
                        etxt = 'LARGE SPP DEVIATION ('+k+')'
                        for g in r_xyz:
                            if float(cons[g]) - thr_rms > 0:
                                efix = [eX0,eX1,g,eX3,3]
                                spec = ': {} = {}'.format(g, cons[g])
                                tup = self.qc_error_handler(
                                        efix, etxt, spec,
                                        rinexfile_status, error_types)
                                rinexfile_status, error_types = tup
                    else:
                        # Sergio Bruni 05/05/2019
                        # 1) do only for GLOBAL_SYSTEM (GPS, GLO, BDS, GAL)
                        # 2) set status_code = 2 only for GPS
                        if k in GLOBAL_SYSTEM:
                            if k == 'GPS':
                                efix = [eX0, eX1, k, eX3, 2]
                            else:
                                efix = ['', eX1, k, eX3, 0]

                            etxt = 'MISSING CONSTELLATION SPP'
                            spec = ''
                            tup = self.qc_error_handler(efix, etxt, spec,
                                        rinexfile_status, error_types)
                            rinexfile_status, error_types = tup
                    # check if enough satellites
                    g = 'nsat'
                    if int(cons[g]) < thr_nsat:
                        # Sergio Bruni 24/04/2019
                        # 1) do only for GPS, GLO, BDS, GAL
                        # 2) set status_code = 3 only for GPS
                        if k in GLOBAL_SYSTEM:
                            if k == 'GPS':
                                my_err_status = 3
                                eX0_ = eX0
                            else:
                                my_err_status = 0
                                eX0_ = ''
                            etxt = 'FEW SATELLITES IN DATA (' + k + ')'
                            efix = [eX0_, eX1, g, eX3, my_err_status]
                            spec = ': {} = {}'.format(g, cons[g])
                            tup = self.qc_error_handler(efix, etxt, spec,
                                                        rinexfile_status, error_types)
                            rinexfile_status, error_types = tup

                    # check if too many excluded satellites (dual-freq)

                    # Sergio Bruni 16-04-2019 the check on xcod_sat is no longer considered usefull
                    # g = 'xcod_sat'
                    # if int(cons[g]) > thr_xsat:
                    #     etxt = 'TRACKING ISSUE ('+k+')'
                    #     efix = [eX0,eX1,g,eX3,3]
                    #     spec = ': {} = {}'.format(g, cons[g])
                    #     tup = self.qc_error_handler(efix, etxt, spec,
                    #                 rinexfile_status, error_types)
                    #     rinexfile_status, error_types = tup


                    # check if have navi data for each constellation:
                    found_n = False
                    for nav in qnm:
                        if nav['id_constellation'] == k:
                            found_n = True
                            # check if enough satellites
                            g = 'nsat'
                            if int(nav[g]) < thr_nsat:
                                etxt = 'LOW NAVIGATION DATA ('+k+')'
                                efix = [eX0,eX1,g,eX3,3]
                                spec = ': {} = {}'.format(g, cons[g])
                                tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                                rinexfile_status, error_types = tup
                    if not found_n:  # sys from header not in navi
                        etxt = 'MISSING NAVIGATION DATA'
                        efix = [eX0,eX1,k,eX3,3]
                        spec = ''
                        tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
                    # check if T1 receiver info contains this constellation
                    if k not in T12['satsys']:
                        etxt = 'CONSTELLATION NOT LISTED AS RECEIVER SAT SYS'
                        efix = [eX0,eX1,k,eX3,2]
                        spec = ''
                        tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
            # finished RINEX2 header and system checks
            else:
                # assume RINEX3 header or RINEX2 with only one system instead:
#                for k, hobs in hdsys.iteritems():
                for k, hobs in hdsys.items():
                    # k = constellation name, e.g. 'GPS'
                    # hobs = observations listed in header for constellation k
                    found_d = False
                    found_n = False
                    # check if same constellation in data
                    for cons in qcs:
                        if cons['id_constellation'] == k:
                            found_d = True
                            # create list of data observations
                            dobs = []
                            for g in obsts:
                                if cons[g]:  # type D can be empty
                                    for d in cons[g]:
                                        dobs.append(d['id_gnss_obsnames'])
                            # check if have same observations/constellation
                            etxt = 'MISSING OBSERVATION DATA ('+k+')'
                            spec = ''
                            for g in hobs:  # obs in header
                                if g not in dobs:  # but not in data
                                    efix = [eX0,eX1,g,eX3,2]
                                    tup = self.qc_error_handler(
                                        efix, etxt, spec,
                                        rinexfile_status, error_types)
                                    rinexfile_status, error_types = tup
                            # check if single point position (SPP) same
                            if cons['x_crd']:  # SPP data can be all None
                                etxt = 'LARGE SPP HEAD-SYS DIFFERENCE ('+k+')'
                                for g in p_xyz:
                                    d = float(hd[g]) - float(cons[g])
                                    if abs(d) - thr_SPP > 0:
                                        efix = [eX0,eX1,g,eX3,2]
                                        spec = (': <head> {} - <system> {} = '
                                                '{}').format(hd[g], cons[g], d)
                                        tup = self.qc_error_handler(
                                                efix, etxt, spec,
                                                rinexfile_status, error_types)
                                        rinexfile_status, error_types = tup
                                etxt = 'LARGE SPP DEVIATION ('+k+')'

                                if k in GLOBAL_SYSTEM:
                                    for g in r_xyz:
                                        #if k == 'GPS':
                                        reference_date = ref_time.date()
                                        d1 = datetime(2000, 5, 1).date()
                                        if reference_date <= d1:
                                            my_threshold = thr_rms_gps_1
                                        else:
                                            my_threshold = thr_rms_gps_2

                                        #else:
                                        #    my_threshold = thr_rms

                                        if float(cons[g]) - my_threshold > 0:
                                            efix = [eX0,eX1,g,eX3,3]
                                            spec = ': {} = {} > {}'.format(g, cons[g], my_threshold)
                                            tup = self.qc_error_handler(
                                                    efix, etxt, spec,
                                                    rinexfile_status, error_types)
                                            rinexfile_status, error_types = tup
                            else:
                                # Sergio Bruni 24/04/2019
                                # 1) do only for GLOBAL_SYSTEM (GPS, GLO, BDS, GAL)
                                # 2) set status_code = 2 only for GPS
                                if k in GLOBAL_SYSTEM:
                                    if k == 'GPS':
                                        my_err_status = 2
                                        eX0_ = eX0
                                    else:
                                        my_err_status = 0
                                        eX0_ = ''

                                    etxt = 'MISSING CONSTELLATION SPP'
                                    efix = [eX0_, eX1, k, eX3, my_err_status]
                                    spec = ''
                                    tup = self.qc_error_handler(efix, etxt, spec,
                                                rinexfile_status, error_types)
                                    rinexfile_status, error_types = tup

                            # check if enough satellites
                            g = 'nsat'
                            if int(cons[g]) < thr_nsat:
                                # Sergio Bruni 24/04/2019
                                # 1) do only for GPS, GLO, BDS, GAL
                                # 2) set status_code = 3 only for GPS
                                if k in GLOBAL_SYSTEM:
                                    if k == 'GPS':
                                        my_err_status = 3
                                        eX0_ = eX0
                                    else:
                                        my_err_status = 0
                                        eX0_ = ''
                                    etxt = 'FEW SATELLITES IN DATA ('+k+')'
                                    efix = [eX0_, eX1, g, eX3, my_err_status]
                                    spec = ': {} = {}'.format(g, cons[g])
                                    tup = self.qc_error_handler(efix, etxt, spec,
                                                rinexfile_status, error_types)
                                    rinexfile_status, error_types = tup
                            # check if too many excluded satellites (dual-freq)

                            # Sergio Bruni 16-04-2019 the check on xcod_sat is no longer considered usefull
                            # g = 'xcod_sat'
                            # if int(cons[g]) > thr_xsat:
                            #     etxt = 'TRACKING ISSUE ('+k+')'
                            #     efix = [eX0,eX1,g,eX3,3]
                            #     spec = ': {} = {}'.format(g, cons[g])
                            #     tup = self.qc_error_handler(efix, etxt, spec,
                            #                 rinexfile_status, error_types)
                            #     rinexfile_status, error_types = tup


                            # Sergio Bruni 17/04/2019 add CHECK ON xcod_epo and xpha_epo
                            # only if total number of epochs (i.e. system is active)
                            if int(oth['numb_epo']) > 0:

                                if k == 'GPS':
                                    my_err_status = 3
                                    eX0_ = eX0
                                else:
                                    my_err_status = 0
                                    eX0_ = ''

                                epo_threshold = int(0.5 * int(cons['epo_have']))
                                g = 'xcod_epo'
                                if int(cons[g]) > epo_threshold:
                                    etxt = 'MORE THAN 50% OF UNUSABLE EPOCHS FOR CODE OBS (' + k + ')'
                                    efix = [eX0_, eX1, g, eX3, my_err_status]
                                    spec = ': {} = {} have epochs = {}'.format(g, cons[g], cons['epo_have'])
                                    tup = self.qc_error_handler(efix, etxt, spec,
                                                                rinexfile_status, error_types)
                                    rinexfile_status, error_types = tup

                                g = 'xpha_epo'
                                if int(cons[g]) > epo_threshold:
                                    etxt = 'MORE THAN 50% OF UNUSABLE EPOCHS FOR PHASE OBS (' + k + ')'
                                    efix = [eX0, eX1, g, eX3, my_err_status]
                                    spec = ': {} = {} have epochs = {}'.format(g, cons[g], cons['epo_have'])
                                    tup = self.qc_error_handler(efix, etxt, spec,
                                                                rinexfile_status, error_types)
                                    rinexfile_status, error_types = tup

                    if not found_d:  # constellation from header not in data
                        etxt = 'MISSING CONSTELLATION DATA'
                        efix = [eX0,eX1,k,eX3,2]
                        spec = ''
                        tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
                    # check if have navi data for each constellation:
                    for nav in qnm:
                        if nav['id_constellation'] == k:
                            found_n = True
                            # check if enough satellites
                            g = 'nsat'
                            if int(nav[g]) < thr_nsat:
                                etxt = 'LOW NAVIGATION DATA ('+k+')'
                                efix = [eX0,eX1,g,eX3,3]
                                spec = ': {} = {}'.format(g, nav[g])
                                tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                                rinexfile_status, error_types = tup
                    if not found_n:  # sys from header not in navi
                        etxt = 'MISSING NAVIGATION DATA'
                        efix = [eX0,eX1,k,eX3,3]
                        spec = ''
                        tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
                    # check if T1 receiver info contains this constellation
                    if k not in T12['satsys']:
                        etxt = 'CONSTELLATION NOT LISTED AS RECEIVER SAT SYS'
                        efix = [eX0,eX1,k,eX3,2]
                        spec = ''
                        tup = self.qc_error_handler(efix, etxt, spec,
                                            rinexfile_status, error_types)
                        rinexfile_status, error_types = tup
                # finished RINEX3 header and system checks
            #
            # How many warnings do we have?
            self.logger.info('[{}] # Have {} warnings.' \
                             .format(rf['name'], len(self.warnings)))
            self.logger.info('[{}] # Have {} errors.' \
                             .format(rf['name'], len(self.errors)))

            #
            #
            ##  Sergio Bruni 9/2/2019  ##
            #
            # Delete QC data any existing entry
            # so new QC data will overwrite the exiisting one's
            #
            self.delete_QcReportSummary(session, rinexfile_id)

            ##  INSERT  ##

            ## QcParameter
            qr['id_qc_parameters'] = self.insert_qcparameter(session, qp)

            ## QcReportSummary
            id_qc_report_summary = self.insert_qcreportsummary(session, qr)
            if id_qc_report_summary<0:
                msg = 'ERROR: Will not overwrite existing entry.'
                self.logger.error('[{}] {}'.format(rf['name'], msg))
                self.errors.append(msg.upper())
                self.rinex_filename = None
                return self.get_json_msg(), OK
            msg = ('Inserted QC_Report_Summary '
                   'with id {}.').format(id_qc_report_summary)
            self.logger.info('[{}] {}'.format(rf['name'], msg))
            self.infos.append(msg.upper())

            ## update the related foreign keys
            for d in qnm:  # navi msg
                d['id_qc_report_summary'] = id_qc_report_summary
            for d in qcs:  # constellation summary
                d['id_qc_report_summary'] = id_qc_report_summary

            try:
                ## QcNavigationMsg
                self.insert_qcnavigationmsg(session, qnm)
                #
                ## QcConstellationSummary
                self.insert_qc_con_and_obs_summary(session, qcs)

                ## Update RINEX file status:
                rinexfile_status = self.check_rinex_file_status(
                        rinexfile_status, 1)  # is "1" if no problems encountered
                self.update_final_rinex_file_status(
                        session, rinexfile, rinexfile_status)

                ## Link errors to RINEX file if any:
                self.insert_rinex_error(session, error_types)

                session.commit()
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                self.logger.error(
                    "module: [{}]; class: [{}]; method: [{}]; line: [{}]; Message: [{}]; excepion info: [{}]" \
                        .format(
                        fname,
                        self.__class__.__name__,
                        inspect.currentframe().f_code.co_name,
                        exc_tb.tb_lineno,
                        str(e),
                        exc_type))
                session.rollback()
                raise e

        msg = self.qc_result_text()

        if self.errors:
            self.qc_logger.error('{} {} {}'.format(rf['name'], rinexfile_status, msg))
        elif self.warnings:
            self.qc_logger.warning('{} {} {}'.format(rf['name'], rinexfile_status, msg))

        self.rinex_filename = None
        return self.get_json_msg(), OK


    ## delete all QC data related to a specific rinex file
    """
        06-19-2020 this function has been completely revised, taking into account that the
        CASCADE option has been defined for all tables. So It is almost unusefull
    """

    # def delete_QcReportSummary(self, session, rinexfile_id):
    #
    #     Q = session.query(QcReportSummary) \
    #         .filter_by(id_rinexfile=rinexfile_id) \
    #         .scalar()
    #
    #     if not Q:
    #         return
    #
    #      # qc_navigation_msg
    #     session.query(QcNavigationMsg).filter_by(id_qc_report_summary=Q.id).delete(synchronize_session=False)
    #
    #     # qc_observation_summary_l/d/c/s
    #     qc_constellation_summary_ids_list = [r.id for r in
    #                                          session.query(QcConstellationSummary.id).filter_by(id_qc_report_summary=Q.id)]
    #
    #     # qc_observation_summary_l
    #     gnss_obsnames_ids_l_list = [r.id_gnss_obsnames for r in session.query(QcObservationSummaryL.id_gnss_obsnames).filter(
    #         QcObservationSummaryL.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list))]
    #     session.query(QcObservationSummaryL).filter(
    #         QcObservationSummaryL.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list)).delete(
    #         synchronize_session=False)
    #
    #     # qc_observation_summary_d
    #     gnss_obsnames_ids_d_list = [r.id_gnss_obsnames for r in session.query(QcObservationSummaryD.id_gnss_obsnames).filter(
    #         QcObservationSummaryD.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list))]
    #     session.query(QcObservationSummaryD).filter(
    #         QcObservationSummaryD.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list)).delete(
    #         synchronize_session=False)
    #
    #     # qc_observation_summary_c
    #     gnss_obsnames_ids_c_list = [r.id_gnss_obsnames for r in session.query(QcObservationSummaryC.id_gnss_obsnames).filter(
    #         QcObservationSummaryC.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list))]
    #     session.query(QcObservationSummaryC).filter(
    #         QcObservationSummaryC.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list)).delete(
    #         synchronize_session=False)
    #
    #     # qc_observation_summary_s
    #     gnss_obsnames_ids_s_list = [r.id_gnss_obsnames for r in session.query(QcObservationSummaryS.id_gnss_obsnames).filter(
    #         QcObservationSummaryS.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list))]
    #     session.query(QcObservationSummaryS).filter(
    #         QcObservationSummaryS.id_qc_constellation_summary.in_(qc_constellation_summary_ids_list)).delete(
    #         synchronize_session=False)
    #
    #     # qc_constellation_summary
    #     session.query(QcConstellationSummary).filter_by(id_qc_report_summary=Q.id).delete(synchronize_session=False)
    #
    #     # gnss_obsnames
    #     gnss_obsnames_ids_list = list(set(gnss_obsnames_ids_l_list) | \
    #                                   set(gnss_obsnames_ids_d_list) | \
    #                                   set(gnss_obsnames_ids_c_list) | \
    #                                   set(gnss_obsnames_ids_s_list))
    #
    #     for id_gnss_obsnames in gnss_obsnames_ids_list:
    #         if session.query(QcObservationSummaryL).filter(
    #                 QcObservationSummaryL.id_gnss_obsnames == id_gnss_obsnames).count() == 0 \
    #                 and session.query(QcObservationSummaryD).filter(
    #             QcObservationSummaryD.id_gnss_obsnames == id_gnss_obsnames).count() == 0 \
    #                 and session.query(QcObservationSummaryC).filter(
    #             QcObservationSummaryC.id_gnss_obsnames == id_gnss_obsnames).count() == 0 \
    #                 and session.query(QcObservationSummaryS).filter(
    #             QcObservationSummaryS.id_gnss_obsnames == id_gnss_obsnames).count() == 0:
    #             session.query(GnssObsname).filter_by(id=id_gnss_obsnames).delete(synchronize_session=False)
    #
    #     # qc_report_summary
    #     id_qc_parameters = Q.id_qc_parameters
    #     session.query(QcReportSummary).filter_by(id_rinexfile=Q.id_rinexfile).delete(synchronize_session=False)
    #
    #     # qc_parameters
    #     if session.query(QcReportSummary).filter(QcReportSummary.id_qc_parameters == id_qc_parameters).count() == 0:
    #         session.query(QcParameter).filter_by(id=id_qc_parameters).delete(synchronize_session=False)
    #
    #     # rinex errors
    #     rinex_error_type_ids_list = [r.id_error_type for r in
    #                                  session.query(RinexError.id_error_type).filter(RinexError.id_rinex_file == Q.id_rinexfile)]
    #     session.query(RinexError.id).filter_by(id_rinex_file=Q.id_rinexfile).delete(synchronize_session=False)
    #
    #     # rinex error_types
    #     for rinex_error_type_id in rinex_error_type_ids_list:
    #         if session.query(RinexError).filter(RinexError.id_error_type == rinex_error_type_id).count() == 0:
    #             session.query(RinexErrorType).filter_by(id=rinex_error_type_id).delete(synchronize_session=False)
    #     return

    def delete_QcReportSummary(self, session, rinexfile_id):
        session.query(QcReportSummary).filter_by(id_rinexfile=rinexfile_id).delete(synchronize_session=False)
        session.query(RinexError.id).filter_by(id_rinex_file=rinexfile_id).delete(synchronize_session=False)
        return


    # Sergio Bruni (INGV) 14/11/2018
    @profile
    def new_contact_post(self, body):
        '''
        This method accepts a contact JSON-turned-dict
        It insert the new contact
        '''
        with closing(self.session()) as session:
            if isinstance(body, dict):
                a = [body]
                body=a
            try:
                for contact in body:
                    agency_id = self.insert_agency(contact, session)
                    status_code, contact_id = self.insert_contact(contact, session, agency_id)
            except Exception as e:
                status_code =  CONFLICT
                return 0, status_code, e.message

            try:
                session.commit()
            except Exception as e:
                session.rollback()
                status_code =  CONFLICT
                return 0, status_code, e.message

            #status_code = CREATED
            if status_code == OK:
                msg = "Contact/s has been succesfully updated"
            else:
                msg = "Contact/s has been succesfully inserted"

            return contact_id, status_code, msg

    # Sergio Bruni (INGV) 14/11/2018
    @profile
    def update_contact_put(self, body):
        '''
        This method accepts a contact JSON-turned-dict (specific for update contact) "body"
        the the two keys: 'old_name' and 'old_email' are mandatory
        '''
        try:

            if isinstance(body, dict):
                a = [body]
                body = a

            with closing(self.session()) as session:
                for contact in body:
                    validated_contact = self.validate_contact(contact)

                    if "old_name" not in validated_contact or "old_email" not in validated_contact:
                        status_code = BAD_REQUEST
                        return status_code, "Missing mandatory keys in request body (old_name and/or old_email"

                    # use the already made insert_agency function
                    new_id_agency = self.insert_agency(contact, session)

                    Q = session.query(Contact) \
                        .filter_by(name=validated_contact['old_name'], email=validated_contact['old_email']) \
                        .scalar()

                    if not Q:
                        msg = ('Contact with name: {0} and email {1} was NOT found')\
                                  .format(validated_contact['old_name'].encode('utf8'),
                                          validated_contact['old_email'].encode('utf8'))
                        self.logger.error(msg)
                        continue

                    old_agency_id = Q.id_agency

                    Q.name = validated_contact['name']
                    Q.email = validated_contact['email']
                    Q.comment = validated_contact['comment']
                    Q.phone = validated_contact['phone']
                    Q.role = validated_contact['role']
                    Q.title = validated_contact['title']
                    Q.id_agency = new_id_agency

                    if old_agency_id != new_id_agency:
                        # check if old agency has no more linked comtact and, if so, delete it
                        if session.query(Contact).filter(Contact.id_agency == old_agency_id).count() == 0:
                            session.query(Agency).filter(Agency.id == old_agency_id).delete()


                session.commit()

        except Exception as e:
            status_code = CONFLICT
            return status_code, e.message


        status_code = OK

        return status_code, "Contact has been succesfully updated"

    # Sergio Bruni (INGV) 24/11/2018
    def update_station_contact(self, body, session, station_id, curr_contacts):
        for contact in body['station_contacts']:

            # I reuse this alredy made funcion even if it's to complex
            # for this new scenario (contact has only name and email)
            contact = self.validate_contact(contact)
            Q = session.query(Contact) \
                .filter_by(name=contact['name'], email=contact['email']) \
                .scalar()
            if not Q:
                msg = 'Contact name: {} email: {} was NOT found for station id: {}. Could not be linked it to this station!!!'\
                       .format(contact['name'].encode('utf8'), contact['email'], station_id)
                self.logger.error(msg)
                continue

            if Q.id in curr_contacts:
                curr_contacts[Q.id] = True
            else:
                station_contact = StationContact()
                station_contact.id_station = station_id
                station_contact.id_contact = Q.id
                session.add(station_contact)
                session.flush()

    # Sergio Bruni (INGV) 26/11/2018
    def update_station_network(self, body, session, station_id, network_dict):
        for a_network in body['station_networks']:

            try:
                #### Insert Network info ####
                self.logger.info('Checking network information')
                network = Network()
                network_id = None

                ### 1 ### Fill N/A in for none
                if a_network['name']:
                    network_name = a_network['name']
                else:
                    network_name = 'None'

                ### 2 ### Check if network description exists and create one if not.
                network_id = session.query(Network.id)\
                              .filter(Network.name == network_name)\
                              .one_or_none()
                if network_id:
                    network_id = network_id[0]
                    self.logger.info('Network description was found: {}'.format(network_id))
                    if network_id in network_dict:
                        network_dict[network_id] = True
                    else:
                        self.insert_station_network(session, station_id, [network_id])
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Network description not found. New network will be created.')
                    network.name = network_name
                    #TODO: See how to handle contact_id-less network info...
                    #network.id_contact = contact_id
                    session.add(network)
                    session.flush()

                    self.insert_station_network(session, station_id, [network.id])
            except Exception as e:
                session.rollback()
                raise e
        return True

    # sergio bruni 26/11/2018
    def insert_item(self, body, session, station_id):
        #try:
            #### Insert Contact info ####
            self.logger.info('Checking item information')
            item_id_list = []

            for a_item in body['station_items']:
                item = Item()
                item_id = None
                item_type_id = None

                self.logger.info('A item: {0}'.format(a_item['item']))

                # Get contact IDs. Contact must exest apriori inside DB
                if a_item['item']['contact_as_producer']:
                    c = a_item['item']['contact_as_producer']
                    Q = session.query(Contact) \
                        .filter_by(name=c['name'],
                                   email=c['email']) \
                        .scalar()
                    if not Q:
                        self.logger.error(
                            "{0} Contact with name: [{1}] and email: [{2}] NOT FOUND in Database!!! Current item will have producer contact set to NULL. item comment: [{3}]" \
                            .format(__name__, c['name'].encode('utf8'), c['email'], a_item['item']['comment']))
                        id_contact_as_producer = None
                    else:
                        id_contact_as_producer = Q.id
                else:
                    id_contact_as_producer = None

                if a_item['item']['contact_as_owner']:
                    c = a_item['item']['contact_as_owner']
                    Q = session.query(Contact) \
                        .filter_by(name=c['name'],
                                   email=c['email']) \
                        .scalar()
                    if not Q:
                        self.logger.error(
                            "{0} Contact with name: [{1}] and email: [{2}] NOT FOUND in Database!!! Current item will have owner contact set to NULL. item comment: [{3}]" \
                            .format(__name__, c['name'].encode('utf8'), c['email'], a_item['item']['comment']))
                        id_contact_as_owner = None
                    else:
                        id_contact_as_owner = Q.id
                else:
                    id_contact_as_owner = None


                self.logger.info('Contacts are done: id: {0} and {1}'.format(id_contact_as_producer, id_contact_as_owner))

                # Get item type ID
                if a_item['item']['item_type']['name']:

                    item_type_name = a_item['item']['item_type']['name']
                    self.logger.info('Item type name was found: {}'.format(item_type_name))

                    item_type_id = session.query(ItemType.id)\
                              .filter(and_(ItemType.name == item_type_name))\
                              .one_or_none()

                    if item_type_id:
                        item_type_id = item_type_id[0]
                        self.logger.info('Item type id "{0}" found.'.format(item_type_id))
                    else:
                        self.logger.info('SKIPPING ITEM: Not a valid item type: {0}'.format(item_type_name))
                        break
                else:
                    self.logger.info('SKIPPING ITEM: No item type given.'.format(item_type_name))
                    break

                # Get comment
                try:
                    # This is to counter inconcistencies in the item object structure.
                    # This should be managed via schema check.
                    item_comment = a_item['item']['comment']
                except Exception as e:
                    item_comment = 'None'



            ### 2 ### Check if state description exists and create one if not.

                # item_id = session.query(Item.id)\
                #               .filter(and_(Item.id_item_type == item_type_id,
                #                             Item.id_contact_as_producer == id_contact_as_producer,
                #                             Item.id_contact_as_owner == id_contact_as_owner,
                #                             Item.comment == item_comment))\
                #               .one_or_none()
                #
                # if item_id:
                #     item_id = item_id[0]
                #     self.logger.info('Item was found: {}'.format(item_id))
                    # The next line is a hack-fix. For some reason the query returns a tuple but
                    # not just the value - if there is a result is should only be one and integer.
                # else:
                    # Description does not exist so it will be created in db.
                # self.logger.info('Item description not found. New item will be created.')
                self.logger.info('Adding a new item.')
                # item.id_item_type = 3
                item.id_item_type = item_type_id
                item.id_contact_as_producer = id_contact_as_producer
                item.id_contact_as_owner = id_contact_as_owner
                item.comment = item_comment

                session.add(item)
                session.flush()

                item_id = item.id
                self.logger.info('Item id: {}'.format(item.id))
                session.flush()

                item_id_list.append(
                    {
                        'item_id':item_id,
                        'date_from':a_item['item']['date_from'],
                        'date_to':a_item['item']['date_to']
                    }
                )

                # Insert all item attributes:
                for item_attribute in a_item['item']['item_attributes']:
                    self.insert_item_attribute(
                        item_attribute, session, item_id
                    )

                self.insert_station_item(
                    session, station_id, item_id,
                    a_item['item']['date_from'], a_item['item']['date_to']
                )

            self.logger.info('Return: {}'.format(item_id_list))
            return item_id_list



    def network_post(self, body):
        '''
        This method accepts a network JSON
        It insert the new network(s) if not present and update the table station_network
        '''

        self.logger.info('network_post() was called')
        self.logger.info('Station marker: {}'.format(body['marker']))
        with closing(self.session()) as session:
            status_code = NO_CONTENT



            try:
                station_id = session.query(
                    Station.id) \
                    .filter(Station.marker == body['marker'].upper(),
                            Station.country_code == body['country_code'],
                            Station.monument_num == body['monument_number'],
                            Station.receiver_num == body['receiver_number']
                            ).scalar()
                if station_id:
                    self.logger.info(
                        ('station {} was found in the database.'
                         '').format(body['marker']))
                    network_id_list = self.insert_network(body, session)
                    session.commit()
                    session.flush()
                    self.update_station_network(body, session, station_id,
                                                body['station_networks'])
                    session.commit()
                    session.flush()
                    unknown_id = session.query(Network.id).filter(Network.name == 'N/A').scalar()
                    station_network_list = session.query(StationNetwork).filter(StationNetwork.id_station == station_id).all()
                    if len(station_network_list) > 1:
                        for sn in station_network_list:
                            if sn.id_network == unknown_id:
                                session.delete(sn)
                                session.commit()
                                session.flush()
                   #for s in session.query(Station.marker, Network.name, StationNetwork.id).filter(Station.id == station_id, Station.id == StationNetwork.id_station, Network.id == StationNetwork.id_network).all():
                    #    print 'jjjj',s
                    status_code = CREATED
                    session.commit()
                    return status_code, "network(s) has/have been succesfully inserted"
                else:
                    self.logger.info(
                    ('station {} was NOT found in the database').format(body['marker']))
                    status_code = NOT_FOUND
                    return  status_code, 'station {} was NOT found in the database'.format(body['marker'])

            except Exception as e:
                status_code =  CONFLICT
                return status_code, e.message


    # José Manteigueiro (UBI)
    # Set user group station to EPOS or Non-EPOS of the given station
    # 09/08/2019
    def insert_user_group_station(self, args):

        ## Preset result
        result = None
        status_code = NO_CONTENT

        self.logger.info(('insert_user_group_station() was called with '
                          'arguments {}').format(args))

        # DEFAULT values:
        id_station = None
        marker = None
        user_group = None
        user_group_arg = 0

        if args['user_group'].encode('UTF-8') != '1' and args['user_group'].encode('UTF-8') != '0':
            self.logger.info('user_group: -> value is missing')
            result = json.dumps({'message': 'WARNING: Station should be 0 (non-epos) or 1 (epos)!'})
            return result, BAD_REQUEST
        else:
            user_group_arg = args['user_group'].encode('UTF-8')

        # Station identifier from http-input parameters
        if 'station_id' in args and args['station_id']:
            if args['station_id'].isdigit():
                self.logger.info('marker_id is TYPE "digit"')
                id_station = int(args['station_id'])
            else:
                self.logger.info('marker_id is TYPE "marker"')
                marker = args['station_id'].upper()
        else:
            self.logger.info('marker_id: -> value is missing')
            result = json.dumps({'message': 'WARNING: No station ID given!'})
            return result, OK

        # Execute sqlAlchemy query
        with closing(self.session()) as session:

            # Collect criteria as tuple of SQLAlchemy filters:
            filters = ()
            if id_station:
                filters += (Station.id == id_station,)
            if marker:
                filters += (Station.marker == marker,)
            # if 'date_from' in args and args['date_from']:
            #     filters += (RinexFile.reference_date >= args['date_from'],)

            station_query = session.query(Station) \
                .filter(or_(Station.marker == marker, Station.id == id_station)) \
                .first()

            if station_query:

                # self.logger.info('Length of station query result list: {0}'.format(len(station_query)))
                # self.logger.info('Query result list: {0}'.format(station_query))

                ### Define a dict to collect the dictified models
                station_dict = {}

                user_group = session.query(
                    UserGroupStation) \
                    .filter(UserGroupStation.id_station == station_query.id) \
                    .one_or_none()
                if user_group_arg == '1':
                    user_group_name = 'EPOS'
                else:
                    user_group_name = 'Non-EPOS'
                    user_group_arg = 2

                ### Check if user group exists
                if user_group:
                    ### User Group exists, change it
                    try:
                        user_group.id_user_group = user_group_arg
                        session.add(user_group)
                        session.flush()
                        session.commit()

                    except Exception as e:
                        session.rollback()
                        raise e

                    result = json.dumps({'message': 'User group changed to {0}.'.format(user_group_name)})
                    status_code = OK

                    return result, status_code
                else:
                    ### Create User Group for station
                    try:

                        user_group_station = UserGroupStation()
                        user_group_station.id_station = station_query.id
                        user_group_station.id_user_group = user_group_arg

                        session.add(user_group_station)
                        session.flush()
                        session.commit()

                    except Exception as e:
                        session.rollback()
                        raise e

                    result = json.dumps({'message': 'User group created (station {1}) -> {0}.'.format(user_group_name,
                                                                                                      station_query.marker)})
                    status_code = OK

                    return result, status_code
            else:
                # No result - no content
                result = json.dumps({'message': 'Station not found.'})
                status_code = BAD_REQUEST
                return result, status_code

        # Finally finally return the response as results!
        result = json.dumps({'message': 'Something went wrong!'})
        status_code = NOT_FOUND

        return result, status_code



#todo 09/06/2023 added from abbreviation_issue
    # link a station to a specific node
    def station2node_post(self, body):
        with closing(self.session()) as session:
            try:
                station = list(body["station"].keys())[0]
                nodes = body["station"][station]
                source = body["source"]
                metadata = body["metadata"]
            except:
                self.logger.error("station2node: Unable to read the json")
                return BAD_REQUEST, {"status": "Badly formatted json", "newNode": None}

            try:

                Q = (
                    session.query(
                        func.concat(
                            Station.marker,
                            Station.monument_num,
                            Station.receiver_num,
                            Station.country_code,
                        ),
                        Node.name,
                        Connection.metadata_,
                    )
                    .join(Connection, Connection.station == Station.id)
                    .join(Node, Node.id == Connection.destiny)
                    .filter(
                        func.concat(
                            Station.marker,
                            Station.monument_num,
                            Station.receiver_num,
                            Station.country_code,
                        )
                        == station
                    )
                    .all()
                )

            except Exception as e:
                self.logger.error(
                    "station2node: Unable to read the database for station {} : {}".format(
                        station, e
                    )
                )
                return BAD_REQUEST, {"status": "Database query error: {}".format(e), "newNodes": None}

            db_nodes = [x[1] for x in Q if x[2] == metadata]
            # temporary hack to manage the discrepancy between M3G and DGW in the names
            translation = {
                "ROB-EUREF": "ROB-EUREF",
                "CzechGeo": "CzechGeo",
                "INGV": "Italian National Node for RING",
                "NOA": "NOA",
                "C4G": "Portuguese EPOS National Node",
                "Romanian National Node": "Romanian National Node",
                "French-node": "French-Node",
                "IPGP": "IPGP-GNSS",
                "Pan-European": "EPOS-GNSS Pan-European Node",
                "IGE": "Spanish EPOS Data node",
                "PGW": "PGW",
            }

            new_nodes = []
            for node in nodes:
                if node not in translation:
                    self.logger.info(
                        "station2node: expected node {} not in DB".format(node)
                    )
                    continue  # skip the nodes not present in the DGW
                target_node = translation[node]
                if target_node not in db_nodes:
                    new_nodes.append(target_node)
            self.logger.info(
                "station2node: new nodes for stations {} :  {} ".format(
                    station, new_nodes
                )
            )
            if new_nodes == []:

                status_code = OK
                msg = {"status": "No new node for station: {}".format(station), "newNodes": None}
                self.logger.info(
                    "station2node: No new node for station: {}".format(station)
                )
            else:
                connections_to_add = []
                for node in new_nodes:
                    Qdestiny = session.query(Node.id).filter(Node.name == node).one()
                    Qstation = (
                        session.query(Station.id)
                        .filter(
                            func.concat(
                                Station.marker,
                                Station.monument_num,
                                Station.receiver_num,
                                Station.country_code,
                            )
                            == station
                        )
                        .one()
                    )
                    Qsource = session.query(Node.id).filter(Node.name == source).one()
                    connections_to_add.append(
                        Connection(
                            source=Qsource.id,
                            destiny=Qdestiny.id,
                            station=Qstation.id,
                            metadata_=metadata,
                        )
                    )
                self.logger.info(
                    "station2node: connections_to_add :  {}".format(
                        connections_to_add
                    )
                )
                try:
                    session.add_all(connections_to_add)
                    session.commit()
                    status_code = OK
                    msg = {"status": "Station {} linked to nodes {}".format(station, new_nodes), "newNodes": new_nodes}
                    self.logger.info(
                        "station2node: Station {} linked to node {}".format(
                            station, node
                        )
                    )
                except IntegrityError as e:
                    session.rollback()
                    status_code = BAD_REQUEST
                    msg = {"status": "Unable to insert the new connection for station {} and nodes {}".format(
                        station, new_nodes
                    ), "newNodes": new_nodes}
                    self.logger.error(
                        "station2node: No new node for station: {}".format(station)
                    )

            self.logger.info("adding/updating station {}".format(station))
            return status_code, msg

# todo 09/06/2023 added from abbreviation_issue
    def station2pgw_post(self, body):
        self.logger.warning("station2pgw in station2node is deprecated")

        with closing(self.session()) as session:
            try:
                SubQ = (
                    session.query(Station.id)
                    .join(Connection, Connection.station == Station.id)
                    .join(Node, Node.id == Connection.destiny)
                    .filter(Node.name == "PGW")
                    .subquery()
                )

                Q = session.query(Station.id).filter(Station.id.notin_(SubQ)).all()
                Qpgw = session.query(Node.id).filter(Node.name == "PGW").one()
                Qdgw = session.query(Node.id).filter(Node.name == "DGW").one()
                metadata = "T1"
            except Exception as e:
                self.logger.error(
                        "station2node: Unable to read the database when processing all stations: {}".format(
                            e
                        )
                )
                return BAD_REQUEST, "Database query error: {}".format(e)

            try:
                if Q == []:
                    return OK, "No station to link to PGW"
                connections_to_add = []
                for station_id in Q:
                    connections_to_add.append(
                        Connection(
                            source=Qdgw.id,
                            destiny=Qpgw.id,
                            station=station_id,
                            metadata_=metadata,
                        )
                    )

                session.add_all(connections_to_add)
                session.commit()
                status_code = OK
                msg = "All stations linked to DGW"
                self.logger.info("station2node: All stations linked to the PGW")
            except IntegrityError as e:
                session.rollback()
                status_code = BAD_REQUEST
                msg = "Unable to link all stations to the PGW: {}".format(e)
                self.logger.error(
                    "station2node: Unable to link all stations to the PGW: {}".format(e)
                )

        return status_code, msg



def main():
    '''
    This main function of the class is mainly for testing purposes.
    It runs all the queries to see if all of them run as expected.
    '''

    print(" >> This is a test run for the Queries() module")
    print(" >> ...")

    # We must provide the Queries class database information for connection
    print(" >> Defining database conneciton")
    db_info = {'db_type':'postgresql', 'user':'gps', 'password':'gps',
               'hostname':'localhost', 'db_name':'gps'}

    # Then we instantiate the class
    print(" >> Instantiating the Queries() class ")
    queries = Queries()

    # Test if data is in the data model.
    # The test will fail if data is missing
    #print " >> Test 1: Data in datamodel"
    #queries.datamodel_check()

    # Test /station
    #print " "
    #print " >> Test 2: Query '/station'"
    #print " "
    #args = {'bbox':None, 'llrad':None, 'dates':None, 'item_type':None, 'model':None}
    #station_list, status_code = queries.station_list(args)

    #parsed_list = json.loads(station_list)
    #print json.dumps(parsed_list, indent=3, sort_keys=True)

    # Test /station/{marker}
    print(" ")
    print(" >> Test 3: Query '/station/{marker}'")
    print(" ")
    station_single, status_code = queries.station_single('710')

    parsed_single = json.loads(station_single)
    print(json.dumps(parsed_single, indent=3, sort_keys=True))

if __name__ == "__main__":
    main()

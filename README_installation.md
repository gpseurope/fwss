# Install Flask Web Server Application (FWSS):
documentation written by Patrizia Pizzulo (2023-09-12)

## a.	PREREQUISITE: Python 3.10, GUNICORN and NGINX to install Fwss in production environment
## b.	Clone the project from https://gitlab.com/gpseurope/fwss
## c.	in fwss install the need libraries:
```bash
pip install flask
pip install sqlalchemy==1.2.19
pip install psycopg2-binary
pip install requests
pip install xmltodict
pip install -U flask-cors
```
Remark: (is -U necessary for flask-cors, to be checked)


## d.	copy the configuration file web_server.cfg.default in the file web_server.cfg and modify Database Configuration data:
```
app_run: 127.0.0.1     # Make sure Flask only listens on the localhost interface.
port: 8000 		# Change the default FWSS port from 5555 to 8000, which is the default port used by Gunicorn.
proxy: True 	# Enabling tells the Flask is behind a proxy.
```

## e.	To run Gunicorn as a systemd service, create and edit with root privilege the file /etc/systemd/system/fwss.service as reported below:

```
[Unit]
Description=Gunicorn Daemon for FWSS
After=network.target

[Service]
Type=simple
# The specific user and group that the FWSS service will run as.
User=<USER>
Group=<GROUP>

# RuntimeDirectory=<DIRECORY NAME - DEFAULT IS [fwss]>
RuntimeDirectory=fwss

# The working directory where Gunicorn will look for web apps.
# WorkingDirectory=<ABSOLUTE PATH to RuntimeDirectory>
WorkingDirectory=/absolute/path/to/your/epos/fwss

# ExecStart=<ABSOLUTE PATH TO gunicorn (*) - INSIDE PYTHON ENVIRONMENT> -w ##4 --log-file=<ABSOLUTE PATH WITH FILE NAME OF THE LOGFILE> --log-level=info 'web_server:app'
ExecStart=/absolute/path/to/your/python/version/bin/gunicorn -w 4 --log-file=/absolute/path/to/your/log/fwss/fwss.log --log-level=info 'web_server:app'

ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true

[Install]
WantedBy=multi-user.target 

```


##f.	reload systemd configuration to make your changes effective and set the service to automatically start at boot:
```
$ sudo systemctl daemon-reload
$ sudo systemctl enable --now fwss
$ sudo systemctl restart fwss
$ systemctl status fwss
```
From now, Gunicorn should listen on http://127.0.0.1:8000 for HTTP requests.

## g.	enable the nginx service and checking the status: 
```
$ sudo systemctl enable --now nginx
$ systemctl status nginx 
```

## h.	create a new vhost file /etc/nginx/sites-available/fwss to act as a reserve proxy for Gunicorn, as reported below:
```
server {
 # listen <fwss_port>;
   listen 5555;
 # server_name <fwss_fqdn>;
   server_name 127.0.0.1;

 access_log /var/log/nginx/fwss_access.log combined buffer=16k flush=1m;
   error_log /var/log/nginx/fwss_error.log warn;

location / {
       proxy_pass http://127.0.0.1:8000/;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
       proxy_set_header X-Forwarded-Host $host;
       proxy_set_header X-Forwarded-Port $server_port;
       proxy_set_header X-Forwarded-Prefix /;
    }
}
```

## i.	enable the new vhost and reload Nginx configuration.
```
$ sudo ln -s ../sites-available/fwss /etc/nginx/sites-enabled/fwss
$ sudo systemctl restart nginx
$ sudo nginx -t && sudo systemctl reload nginx
```
From now, Nginx should listen for incoming FWSS requests on port 5555 and forward them to Gunicorn locally on port 8000.



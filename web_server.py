# -*- coding: utf-8 -*-
# Responsible:    Daniele Randazzo, Patrizia Pizzulo
# Contributors: Juliette Legrand, Jean-Luc Menut
# Previous contributors: Fjalar Sigurðarson (fjalar@vedur.is),  Tim Sonnemann (tim@vedur.is)
# Date:   2023-12-04

# standard library imports:
import sys
import json
import os.path
from configparser import ConfigParser
#from httplib import (
from http.client import (
    NO_CONTENT,
    OK,
    NOT_FOUND,
    CONFLICT,
    BAD_REQUEST,
    INTERNAL_SERVER_ERROR,
    CREATED,
    ACCEPTED,
)
import requests
#from urlparse import urljoin
from urllib.parse import urljoin

# third party imports:
from flask import (
    Flask,
    request,
    render_template,
    Response,
    send_from_directory,
)
from flask_cors import CORS

# local app imports:
#from queries import Queries, __benchmark__, create_benchmark_log
import queries
from queries import profile

###


#FWSS_VERSION = "v2.0.9" # dr>6/12/2023      #    was: "v2.0.8" # 13/09/2023
#FWSS_VERSION = "v2.0.10" # dr>01/02/2024
#FWSS_VERSION = "v2.0.12" # dr>08/02/2024
FWSS_VERSION = "v2.0.13" # jl>26/04/2024

app = Flask(__name__)
CORS(app)

def parseConfig(type =""):
    '''
    Config parser that returns necessary parameters for the server to run
    '''

    try:
        configFile = "web_server.cfg"
#        parser = SafeConfigParser()
        parser = ConfigParser()

        # Check if config file exists
        if os.path.isfile(configFile):

            parser.read(configFile)
            #configs.append(parser)

            ## Server parameters ##
            if type == "server":
                # app_debug = parser.get("Server", "app_debug")
                app_debug = parser.getboolean("Server", "app_debug") # gunicorn
                try:
                    app_run = parser.get("Server", "app_run")
                    app_port = int(parser.get("Server", "port"))
                except Exception as e:
                    app_run = None
                    app_port = 5000
                try:
                    app_proxy = parser.getboolean("Server", "proxy")
                except Exception as e:
                    app_proxy = False

                # return app_run, app_debug, app_port
                return app_run, app_debug, app_port, app_proxy

            ## Log rotating parameters ##
            if type == "log-rotation":
                # Pull log rotating settings info from the config file,
                # store in a dict and return it
                try:
                    maxBytes = int(parser.get("Log-rotation", "maxBytes"))
                except Exception as e:
                    maxBytes = 1000000

                if maxBytes <= 0:
                    maxBytes = 1000000

                try:
                    backupCount = int(parser.get("Log-rotation", "backupCount"))
                except Exception as e:
                    backupCount = 10

                if backupCount <= 0:
                    backupCount = 1

                log_info = {'maxBytes': maxBytes,
                            'backupCount': backupCount
                            }
                return log_info

            ## Database parameteres ##
            if type == "database":

                # Pull database settings info from the config file,
                # store in a dict and return it
                db_info = {'db_type':parser.get("Database", "db_type"),
                           'username':parser.get("Database", "username"),
                           'password':parser.get("Database", "password"),
                           'hostname':parser.get("Database", "hostname"),
                           'db_port': int(parser.get("Database", "db_port")),
                           'db_name':parser.get("Database", "db_name")
                           }

                return db_info

            if type == 'glass-api':
                # Pull GLASS-API info from the config file
                # store in a dict and return it


                glass_api_info = {'url': parser.get("GLASS-API", "url")}
                return glass_api_info

            if type == "benchmark":
                try:
                    active = parser.get("Benchmark", "active")
                    if active.lower() == 'true':
                        active = True
                    else:
                        active = False
                except Exception as e:
                    active = False

                try:
                    threshold_seconds = float(parser.get("Benchmark", "threshold_seconds"))
                except Exception as e:
                    threshold_seconds = 0.5

                benchmark = {
                    'active': active,
                    'threshold_seconds': threshold_seconds
                }
                return benchmark

        else:
            print('''>> Error in parseConfig(): Config file "{0}" does not exist.'''.format(configFile))
            print('''>> Please provide a valid config file. See default file "web_server.cfg.default"''')
            print('''>> rename it to web_server.cfg and configure for your environment.''')
            sys.exit(0)

    except Exception as e:
        #        print >> sys.stderr, "Error in parseConfig(): "
        #        print >> sys.stderr, e
        print(f"Error in parseConfig(): {e}")



# Change app middleware if it runs behind a reverse proxy.
app_run, app_debug, app_port, app_proxy = parseConfig("server")
if app_proxy:
    from werkzeug.middleware.proxy_fix import ProxyFix
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1, x_port=1)


## Instantiate a database conneciton
db_info = parseConfig("database")
log_info = parseConfig("log-rotation")

benchmark = parseConfig("benchmark")
queries.__benchmark__ = benchmark
if benchmark['active']:
    queries.create_benchmark_log(log_info)

queries = queries.Queries(db_info, log_info)

# load GLASS-API info from the config file
glass_api_info = parseConfig('glass-api')

###
### Services begin ###
###

@app.route('/', alias=True)
@app.route('/gps/', alias=True)
@app.route('/gps')
def hello():
    """
    Basic greeting message on server main page.
    """
    title = 'GNSS Europe'
    head1 = ('Hello! I am your friendly Flask webserver '
             'built to function as a GPS Web Service')
    rooturl = request.url_root
    services = [
        {'endpoint':'checkversion',
         'description':'Return running DB-API version'
        },
        {'endpoint':'gps/station',
         'description':'List all GPS stations with a subset of metadata.'
        },
        {'endpoint':'gps/station/<station_identifier>',
         'description':'Get all information about one station based on marker/id.'
        },
        {'endpoint':'gps/station/<station_identifier>',
         'description':'DELETE method. Delete the station based on long_name/id with all its related data. long_name is 9 characters formatted name (Marker + Monument number + Receiver number + Country code)'
        },
        {'endpoint':'gps/sitelog/<station_identifier>',
         'description':'Get sitelog information for one station.'
        },
        {'endpoint':'gps/sitelog',
         'description':'Insert/update sitelog information for one station, provided in json format'
        },
        {'endpoint':'gps/data/rinex/<station_identifier>',
         'description':'Get (all?) RINEX file metadata for one station.'
        },
        {'endpoint':'gps/data/rinex/<station_identifier>/<file_identifier>',
         'description':'Get specified RINEX file metadata for one station.'
        },
        {'endpoint':'gps/data/rinex',
         'description':'Index new RINEX file or update if exists. File metadata are provided in json format'
        },
        {'endpoint': 'gps/datacenter',
         'description': 'List all datacenter'
        },
        {'endpoint': 'gps/datacenter',
         'description': 'Insert new datacenter. Input data are expected in json format'
        },
        {'endpoint': 'gps/agency',
         'description': 'List all agencies'
        },
        {'endpoint': 'gps/data/qcfile/echo',
         'description': 'Return the DB - corresponding JSON dict upon POSTing a QC - XML file text. This does not interact with the DB and is mostly for testing.'
        },
        {'endpoint': 'gps/data/qcfile',
         'description': 'Validate and insert or update QC data to DB. Input data are expected in XML format'
        },
        {'endpoint': 'gps/contact',
         'description': '(POST) Insert new contact/s provided in json format as single element or list of elements'
        },
        {'endpoint': 'gps/contact',
         'description': '(PUT) Update contact/s provided in json format as single element or list of elements. On respect to the insert service, json data contains two more fields: old-email and old-name of the contact/s to be updated'
        },
        {'endpoint': 'gps/usergroup/<station_identifier>/<epos>',
         'description': '(GET) Insert or update the user group station of a station. <epos> value is 0 for Non-EPOS and 1 for EPOS'
        },
    ]

    mailtocontact=None
    return render_template('services_home.html', # should be in 'templates/'
                           title=title,
                           head1=head1,
                           rooturl=rooturl,
                           services=services,
                           mailtocontact=mailtocontact)

@app.route('/gps/geodetic.yaml')
def send_yaml():
    return send_from_directory('swagger', 'geodetic.yaml')

@app.route('/gps/station', methods=['GET', 'POST'])
def station():
    '''Station list service'''

    #Preset variables
    status_code = NO_CONTENT
    result = (("<h1>{} - This service gave no results.</h1>"
               "Either the database is empty or necessary "
               "tables are missing data.").format(status_code),NO_CONTENT)

    ## Handle GET requests
    if request.method == 'GET':

        # 1) Argument handling

        ## Define and prese arg dict
        args = {'bbox':None, 'llrad':None, 'dates':None, 'item_type':None, 'model':None}

        ## Collect arguments to the arg dict
        for key in args:
            if request.args.get(key):
                args[key] = request.args.get(key)

        ## FOR TESTING PURPOSES: output arguments
        if args['bbox']:
            bbox = args['bbox'].split(';')
            print('Bounding box:', bbox)

        if args['llrad']:
            lat_lon_rad = args['llrad'].split(';')
            print("Lat: {0} Lon: {1} Radius: {2} m ".format(lat_lon_rad[0], lat_lon_rad[1], lat_lon_rad[2]))

        if args['dates']:
            dates = args['dates'].split(';')
            print("Dates: from {0} to {1} ".format(dates[0], dates[1]))

        if args['item_type']:
            item_type = args['item_type']
            print("Item type: {0} ".format(item_type))

        if args['model']:
            model = args['model']
            print("Model: {0}".format(model))

        # 2) Query database
        #query_result, status_code = queries.station_list_get(args)
        query_result, status_code = queries.station_list_get_new(args)

        # 3) Process the response
        if status_code == OK:
            result = (Response(query_result), status_code)
            result[0].headers['Content-type'] = 'application/json'

        elif status_code == NOT_FOUND:
            result = NOT_FOUND

    ## Handle POST requests
    elif request.method == 'POST':

        # 1) Collect JSON body
        body = request.get_json(force=True)

        # 2) Query database
        query_result, status_code = queries.station_post(body)

        # Process and deliver the responce
        result = (Response("<h1>{0}</h1>".format(query_result)), status_code)

    return result

@app.route('/gps/station/<station_identifier>', methods=['GET'])
def station_get_by_station_id(station_identifier):
    '''Station - Single station service'''

    #query_result, status_code = queries.station_single_get(station_identifier)
    query_result, status_code = queries.station_single_get_new(station_identifier)

    # # Process and deliver the responce
    #
    # result[0].headers['Content-type'] = 'application/json'
    #
    # if status_code == OK:
    #     result = (Response(query_result), status_code)
    # else:
    #     result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    #
    # result[0].headers['Content-type'] = 'application/json'

    result = (Response(query_result), status_code)
    result[0].headers['Content-type'] = 'application/json'
    return result

# Just check if station exist or not (may be was made only for test purpouse?)
# It is not exposed on default/splash page
# for now I comment this
# @app.route('/gps/station2/<station_identifier>', methods=['GET'])
# def station_get_by_station_id2(station_identifier):
#     '''Station - Single station service'''
#
#     query_result, status_code = queries.station_single_get2(station_identifier)
#
#     result = (Response(query_result), status_code)
#     return result


@app.route('/gps/station/<station_identifier>', methods=['DELETE'])
def station_delete_by_station_id(station_identifier):
    '''Station - Single station service'''

    query_result, status_code = queries.station_single_delete(station_identifier)

    result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    result[0].headers['Content-type'] = 'application/json'

    return result


@app.route('/gps/sitelog', methods=['GET','POST'])
@profile
def sitelog_post_main():
    '''Sitelog - Post sitelog service'''

     # 1) Collect JSON body
    body = request.get_json(force=True)

    # 2) Query database
    station_marker, station_id, status_code = queries.sitelog_post(body)

    # 3) Process the response
    if status_code == CREATED:
        # result = (Response("<h1> {0} - New station {1} was created with id = {2}.</h1>".format(status_code, station_marker, station_id), content_type='text/plain'), status_code)

        data = {}
        data['Status'] = status_code
        data['StationMarker'] = station_marker
        data['StationId'] = station_id
        data['Info'] = 'New Station has been created'
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    elif status_code == OK:
        data = {}
        data['Status'] = status_code
        data['StationMarker'] = station_marker
        data['StationId'] = station_id
        data['Info'] = 'Station has been updated'
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    elif status_code == CONFLICT:
        # result = (Response("<h1> {0} - Station {1} is already in database. Already existing stations cannot be updated at present.</h1>".format(status_code, body['marker'].upper(), content_type='text/plain')), status_code)

        data = {}
        data['Status'] = status_code
        data['StationMarker'] = body['marker'].upper()
        data['Error'] = 'Station is already in database'
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)
        
# TODO 09/06/2023 removed because done on DGW
    # Warn GLASS-API that the DB was updated
    # if result:
    #     try:
    #         glass_api_response = requests.get(urljoin(glass_api_info['url'], str(station_id)), verify=False)
    #     except Exception as e:
    #         print("Connexion error to GlassFramework")
    #         print("url:", glass_api_info['url'])
    #         print(e.message)



    return result

@app.route('/gps/sitelog/<station_identifier>', methods=['GET'])
def sitelog_get_by_station_id(station_identifier):
    '''Sitelog - Get sitelog for station service'''

    query_result, status_code = queries.sitelog_single_get(station_identifier)

    # # Process and deliver the responce
    # result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    # result[0].headers['Content-type'] = 'application/json'
    #
    # if status_code == OK:
    #     result = (Response(query_result), status_code)
    #
    # elif status_code == NOT_FOUND:
    #     result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    #

    result = (Response(query_result), status_code)
    result[0].headers['Content-type'] = 'application/json'
    return result

@app.route('/gps/data/rinex', methods=['POST'])
def data_rinex_post():
    '''Data - Index/Post rinex data'''

    ## Formalize JSON body
    body = request.get_json(force=True)

    ## Post data to database
    query_result, status_code = queries.data_rinex_post(body)

    # Process and deliver the responce
    result = (Response("<h1>{0}</h1>".format(query_result)), status_code)
    #result[0].headers['Content-type'] = 'application/json'

    return result

@app.route('/gps/data/rinex/<station_identifier>', methods=['GET'])
def data_rinex_get_by_station_id(station_identifier):
    '''
    Data - Get list of Rinex files
    
    Required: <station_identifier> = station ID (4/9 char marker name)
    Optional query parameters:
        date_from = start date
        date_to   = end date
        status    = RINEX file status number in DB
    '''
    # Argument handling (mostly in queries.py)
    args = request.args.copy() # make mutable flat multidict
    args.add('station_id', station_identifier)

    ## Query database
    query_result, status_code = queries.data_rinex_station_get(args)

    # Process and deliver the response
    result = (Response(query_result), status_code)
    result[0].headers['Content-type'] = 'application/json'        

    return result

@app.route('/gps/data/rinex/<station_identifier>/<file_identifier>', methods=['GET'])
def data_rinex_get_by_station_and_file_id(station_identifier, file_identifier):
    '''Data - Get information about a single data file service'''

    ## Query database
    query_result, status_code = queries.data_rinex_single_get(station_identifier, file_identifier)

    # Process and deliver the responce
    result = (Response("<h1>{0}</h1>".format(query_result)), status_code)

    return result

@app.route('/gps/datacenter', methods=['GET', 'POST'])
def datacenter():
    '''datacenter listing and inserting service'''
    
    ## Handle GET requests
    if request.method == 'GET':

        query_result, status_code = queries.datacenter_list_get()

        # 3) Process the response
        if status_code == OK:
            result = (Response(query_result), status_code)
            result[0].headers['Content-type'] = 'application/json'

        elif status_code == NOT_FOUND:
            result = NOT_FOUND

    ## Handle POST requests
    elif request.method == 'POST':

        # Expecting JSON input
        body = request.get_json(force=True)

        # 2) Query database
        query_result, status_code = queries.data_center_post(body)

        # 3) Process the response
        result = (Response(query_result), status_code)
        result[0].headers['Content-type'] = 'application/json'
        
    ## Finally: Method not handled
    else:
        result = (Response("<h1>HTTP method '{0}' not supported</h1>".format(request.method)))


    return result

@app.route('/gps/agency', methods=['GET'])
def agency():
    '''Station list service'''

    #Preset variables
    status_code = NO_CONTENT
    result = ('''<h1>{} - This service gave no results.</h1>Either the database is empty or 
              necessary tables are missing data.'''.format(status_code), NO_CONTENT)

    ## Handle GET requests
    if request.method == 'GET':

        # 1) Argument handling

        ## Define and prese arg dict
        #args = {'bbox':None, 'llrad':None, 'dates':None, 'item_type':None, 'model':None}

        ## Collect arguments to the arg dict
        #for key in args:
        #    if request.args.get(key):
        #        args[key] = request.args.get(key)

        # 2) Query database
        #query_result, status_code = queries.datacenter_list_get(args)
        query_result, status_code = queries.agency_list_get()

        # 3) Process the response
        if status_code == OK:
            result = (Response(query_result), status_code)
            result[0].headers['Content-type'] = 'application/json'

        elif status_code == NOT_FOUND:
            result = NOT_FOUND

    ## Finally: Method not handled
    else:
        result = (Response("<h1>HTTP method '{0}' not supported</h1>".format(request.method)))


    return result


@app.route('/gps/data/qcfile/echo', methods=['POST'])
def qc_post_echo():
  """
  Return the DB-corresponding JSON dict upon POSTing a QC-XML file text.
  This does not interact with the DB and is mostly for testing.
  """
  ## Expecting XML data stream input
  body = request.get_data()

  ## Post data to database
  query_result, status_code = queries.qc_post_echo(body)

  # Process and deliver the response
  if status_code == OK:
      result = (Response("<h1>{}</h1>".format(query_result)),status_code)
      result[0].headers['Content-type'] = 'application/json'
  else:
      result = (Response("<h1>{}</h1>".format(query_result)),status_code)

  return result


@app.route('/gps/data/qcfile', methods=['POST'])
def qc_post():
  """
  Validate and insert QC data to DB upon POSTing a QC-XML file text.
  """
  ## Expecting XML data stream input
  body = request.get_data()

  ## Post data to database
  query_result, status_code = queries.qc_post(body) #   ref.1.0

  # Process and deliver the response
  result = (Response(query_result), status_code)
  result[0].headers['Content-type'] = 'application/json'

  return result

# Sergio Bruni (INGV) 14/11/2018
@app.route('/gps/contact', methods=['POST'])
@profile
def new_contact_post_main():

    body = request.get_json(force=True)

    id, status_code, msg = queries.new_contact_post(body)

    # Process the response
    if status_code == CREATED:
        data = {}
        data['Status'] = status_code
        data['msg'] =  msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    else:
        data = {}
        data['Status'] = status_code
        data['Error'] = msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    # Warn GLASS-API that the DB was updated
    # if result: # 09/06/2023 removed disabled because done on DGW epos-gnss-datamanager
    #     try:
    #         glass_api_response = requests.get(urljoin(glass_api_info['url'], 'all'), verify=False)
    #     except Exception as e:
    #         print("Connexion error to GlassFramework")
    #         print("url:", glass_api_info['url'])
    #         print(e.message)

    return result

# Sergio Bruni (INGV) 15/11/2018
# Cannot use the same service for insert/update contacts
# because of input json data for update contain more information (old-name and old-email)
@app.route('/gps/contact', methods=['PUT'])
def update_contact_put():

    body = request.get_json(force=True)

    status_code, msg = queries.update_contact_put(body)

    # Process the response
    if status_code == OK:
        data = {}
        data['Status'] = status_code
        data['msg'] = msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    else:
        data = {}
        data['Status'] = status_code
        data['Error'] = msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

#todo 09/06/2023 removed because done on DGW
    # Warn GLASS-API that the DB was updated
    # if result:
    #     try:
    #         glass_api_response = requests.get(urljoin(glass_api_info['url'], 'all'), verify=False)
    #     except Exception as e:
    #         print("Connexion error to GlassFramework")
    #         print("url:", glass_api_info['url'])
    #         print(e.message)

    return result



#
# endpoint for adding a network not initally present in the geodesyml
#
@app.route('/gps/networks', methods=['POST'])
def network_post():
    body = request.get_json(force=True)
    status_code, msg = queries.network_post(body)
    # Process the response
    if status_code == OK:
        data = {}
        data['Status'] = status_code
        data['msg'] = msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    else:
        data = {}
        data['Status'] = status_code
        data['Error'] = msg
        json_data = json.dumps(data)
        result = (Response(json_data, content_type='application/json'), status_code)

    # Warn GLASS-API that the DB was updated
    # if result: # 09/06/2023 removed beacuse it's done on DGW
    #     try:
    #         glass_api_response = requests.get(urljoin(glass_api_info['url'], 'all'), verify=False)
    #     except Exception as e:
    #         print("Connexion error to GlassFramework")
    #         print("url:", glass_api_info['url'])
    #         print(e.message)

    return result




#todo 09/06/2023 inserted the function station2node_post() from abbreviation_issue, used by DGW
@app.route("/gps/station2node", methods=["POST"])
def station2node_post():
    body = request.get_json(force=True)
    if "all" in body["station"]:
        status_code, msg = queries.station2pgw_post(body)
    else:
        status_code, msg = queries.station2node_post(body)

    json_data = json.dumps({"message": msg})

    return (Response(json_data, content_type="application/json"), status_code)



@app.route('/checkversion', methods=['GET'])
def checkversion():

    # Process the response
    data = {}
    data['status'] = 200
    data['version'] = FWSS_VERSION
    json_data = json.dumps(data)
    result = (Response(json_data, content_type='application/json'), 200)

    return result

# @app.route('/test_log', methods=['GET'])
# def test_log():
#
#     # Process the response
#     queries.test_log()
#     data = {}
#     data['status'] = 200
#     json_data = json.dumps(data)
#     result = (Response(json_data, content_type='application/json'), 200)
#
#     return result

# José Manteigueiro (UBI) 07/08/2019
@app.route('/gps/usergroup/<station_identifier>/<epos>', methods=['GET'])
def new_user_group_station(station_identifier, epos):

    # Argument handling (mostly in queries.py)
    args = request.args.copy()  # make mutable flat multidict
    args.add('station_id', station_identifier)
    args.add('user_group', epos)

    ## Query database
    query_result, status_code = queries.insert_user_group_station(args)

    # Process and deliver the response
    result = (Response(query_result), status_code)
    result[0].headers['Content-type'] = 'application/json'

    return result



if __name__ == "__main__":
    # Run this version of app.run while devloping. Only localhost/127.0.0.1 can access the server
    #app.run()

    # Run this version of app.run to expose the server to the internal network
    # app_run, app_debug, app_port = parseConfig("server")
    #app.run(host=app_run, debug=False, port=app_port)

    # The app is started by the Flask development server.
    app.run(host=app_run, debug=app_debug, port=app_port)
else:
    # The app is started by Gunicorn: use Gunicorn logger.
    import logging
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
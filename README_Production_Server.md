# Deploy FWSS in a production environment using "Gunicorn" and "Nginx"

This section explains how to deploy FWSS in a production environment using "Gunicorn" (https://docs.gunicorn.org/en/stable/index.html) WSGI server and "Nginx" (https://nginx.org/) as a reverse proxy. This has been tested on Ubuntu 22.04 LTS and on CentOS 7, with the latest Gunicorn version (19.10.0) available for Python2.

1) Stop the FWSS Python process that runs the built-in development server of Flask:

   `ps -ef | grep "web_server.py" | awk '{print($2)}' | xargs kill -9`
2) clone the project tag "v2.0.6":

```bash
git clone --depth 1 --branch py3_ingv https://gitlab.com/gpseurope/fwss.git
```

3) Edit the "**web_server.cfg**" configuration file (inside FWSS folder) and set the following parameters:

* [ ] `app_run: 127.0.0.1` # Make sure Flask only listens on the [localhost](http://localhost) interface.
* [ ] `port: 8000` # Change the default FWSS port from 5555 to 8000, which is the default port used by Gunicorn.
* [ ] `proxy: True` # Enabling tells the Flask is behind a proxy.

4) On **Ubuntu** install **Gunicorn** with the Python package manager: `pip2 install gunicorn` This installs the "gunicorn" code inside the python environment which needs to be further pointed with the absolute path (\*) .\
   To install on **CentOS 7** follow this [link](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7) and apply as showed below: \
   use: <span dir="">`sudo yum install python-gunicorn`</span>

   It's important to upgrade the package: <span dir="">`pip install Werkzeug==0.16`</span>
5) To run Gunicorn as a systemd service, create and edit with root privilege the file `/etc/systemd/system/fwss.service` as reported below:

```bash
[Unit]
Description=Gunicorn Daemon for FWSS
After=network.target

[Service]
Type=simple
# The specific user and group that the FWSS service will run as.
User=<USER>
Group=<GROUP>

# RuntimeDirectory=<DIRECORY NAME - DEFAULT IS [fwss]>
RuntimeDirectory=fwss

# The working directory where Gunicorn will look for web apps.
# WorkingDirectory=<ABSOLUTE PATH to RuntimeDirectory>
WorkingDirectory=/absolute/path/to/your/epos/fwss

# ExecStart=<ABSOLUTE PATH TO gunicorn (*) - INSIDE PYTHON ENVIRONMENT> -w 4 --log-file=<ABSOLUTE PATH WITH FILE NAME OF THE LOGFILE> --log-level=info 'web_server:app'
ExecStart=/absolute/path/to/your/python/version/bin/gunicorn -w 4 --log-file=/absolute/path/to/your/log/fwss/fwss.log --log-level=info 'web_server:app'

ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true

[Install]
WantedBy=multi-user.target 
```

On CentOS 7 the Exec configuration is: `ExecStart=/home/.pyenv/shims/gunicorn --log-file=/absolute/path/to/your/fwss/fwss.log --log-level=info 'web_server:app'`

Note that Gunicorn should not be run as root because it would cause your application to run as root, which could lead to potential security issues. Make sure that the ownership and permissions of those files are set correctly with respect to the FWSS user and group. The `-w` option specifies the number of workers (processes) to run. The default is only 1 worker, which is probably not what you want in production.

6) Reload systemd configuration to make your changes effective and set the service to automatically start at boot:

```bash
sudo systemctl daemon-reload
sudo systemctl enable --now fwss
sudo systemctl restart fwss
systemctl status fwss
```

From now, Gunicorn should listen on [http://127.0.0.1:8000](http://127.0.0.1:8000) for HTTP requests.

7) On **UBUNTU** install **Nginx** using your system package manager:

   `apt install nginx`

   On **CentOS7** use: `sudo yum install nginx`\
   to manage the SELinux permission, refer to this [link](https://serverfault.com/questions/566317/nginx-no-permission-to-bind-port-8090-but-it-binds-to-80-and-8080)

   check the port 5555 is active: <span dir="">`sudo semanage port -l | grep http_port_t`</span>

   **<span dir="">http_port_t</span>**<span dir="">                    tcp      5555, 80, 81, 443, 488, 8008, 8009, 8443, 9000</span>\
   \
   if not in the list, it needs to be added: <span dir="">`sudo semanage port -a -t http_port_t  -p tcp 5555`</span>
8) Then enable the nginx service: <span dir="">`sudo systemctl enable --now nginx`</span>\
   and checking the status: <span dir="">`systemctl status nginx`</span> will give a similar output:

```plaintext
● nginx.service - The nginx HTTP and reverse proxy server 
  Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled) 
  Active: active (running) since Tue 2023-01-24 11:12:27 WET; 1min 50s ago 
Main PID: 7915 (nginx) 
  CGroup: /system.slice/nginx.service 
          ├─7915 nginx: master process /usr/sbin/nginx 
          ├─7916 nginx: worker process 
          └─7917 nginx: worker process
```

On **UBUNTU** create a new vhost file `/etc/nginx/sites-available/fwss` to act as a reserve proxy for Gunicorn, as reported below:

```bash
server {
#    listen <fwss_port>;
    listen 5555;
#    server_name <fwss_fqdn>;
    server_name 127.0.0.1;

    access_log /var/log/nginx/fwss_access.log combined buffer=16k flush=1m;
    error_log /var/log/nginx/fwss_error.log warn;

location / {
        proxy_pass http://127.0.0.1:8000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

Replace `<fwss_port>` with your effective FWSS listening port (5555 by default) and `<fwss_fqdn`\\\> with your fully qualified domain name of your server.

8) Enable the new vhost and reload Nginx configuration.

```bash
sudo ln -s ../sites-available/fwss /etc/nginx/sites-enabled/fwss
sudo service apache2 stop
sudo systemctl restart nginx
sudo nginx -t && sudo systemctl reload nginx
```

On **CentOS 7** to configure nginx need to update the file: <span dir="">`sudo vi /etc/nginx/nginx.conf`</span>\
adding the above code server{... location / {...}} under the http field as shown in the [reference link](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7)

From now, Nginx should listen for incoming FWSS requests on port 5555 and forward them to Gunicorn locally on port 8000.

# **Troubleshooting**

Make sure that the ports you are using for Gunicorn and Nginx are not already bound to other running processes.

Check that FWSS files indeed belong to the user defined for running the FWSS service.

Gunicorn debug messages are logged in the file specified in the `--log-file` option.

SELinux: [link](https://stackoverflow.com/questions/23948527/13-permission-denied-while-connecting-to-upstreamnginx)

Check used ports by using one of the following command:

```bash
sudo lsof -i -P -n | grep LISTEN
sudo netstat -tulpn | grep LISTEN
sudo ss -tulpn | grep LISTEN
sudo lsof -i:22 ## see a specific port such as 22 ##
sudo nmap -sTU -O IP-address-Here
```

Control nginx process by:

```plaintext
nginx -s <SIGNAL>

where <SIGNAL> can be one of the following:
quit – Shut down gracefully (the SIGQUIT signal)
reload – Reload the configuration file (the SIGHUP signal)
reopen – Reopen log files (the SIGUSR1 signal)
stop – Shut down immediately (or fast shutdown, the SIGTERM singal)
```

# FWSS: Updates to web_server.py

The actual **release "v1.4.3" the web_server.py file is already updated and does not need any change**. By the way in the following section are reported the difference between the original file and the updated one\*\*, r\*\*ows with a "-" (minus) has to be replaced with respective rows with the "+" (plus):

```diff
diff --git a/web_server.py b/web_server.py
index 1b53ebe..0ae5ff7 100644
--- a/web_server.py
+++ b/web_server.py


@@ -61,15 +61,19 @@ def parseConfig(type =""):
 
             ## Server parameters ##
             if type == "server":
-                app_debug = parser.get("Server", "app_debug")
+                app_debug = parser.getboolean("Server", "app_debug")
                 try:
                     app_run = parser.get("Server", "app_run")
                     app_port = int(parser.get("Server", "port"))
                 except Exception as e:
                     app_run = None
                     app_port = 5000
+                try:
+                    app_proxy = parser.getboolean("Server", "proxy")
+                except Exception as e:
+                    app_proxy = False
 
-                return app_run, app_debug, app_port
+                return app_run, app_debug, app_port, app_proxy
 
             ## Log rotating parameters ##
             if type == "log-rotation":


@@ -150,6 +154,13 @@ def parseConfig(type =""):
         print >> sys.stderr, "Error in parseConfig(): "
         print >> sys.stderr, e
 
+
+# Change app middleware if it runs behind a reverse proxy.
+app_run, app_debug, app_port, app_proxy = parseConfig("server")
+if app_proxy:
+    from werkzeug.middleware.proxy_fix import ProxyFix
+    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1, x_port=1)
+
 ## Instantiate a database conneciton
 db_info = parseConfig("database")
 log_info = parseConfig("log-rotation")


@@ -760,10 +771,11 @@ def new_user_group_station(station_identifier, epos): 
 if __name__ == "__main__":
-    # Run this version of app.run while devloping. Only localhost/127.0.0.1 can access the server
-    #app.run()
-
-    # Run this version of app.run to expose the server to the internal network
-    app_run, app_debug, app_port = parseConfig("server")
-    #app.run(host=app_run, debug=False, port=app_port)
+    # The app is started by the Flask development server.
     app.run(host=app_run, debug=app_debug, port=app_port)
+else:
+    # The app is started by Gunicorn: use Gunicorn logger.
+    import logging
+    gunicorn_logger = logging.getLogger('gunicorn.error')
+    app.logger.handlers = gunicorn_logger.handlers
+    app.logger.setLevel(gunicorn_logger.level)
```
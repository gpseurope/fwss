# -*- coding: utf-8 -*-
# Authors:
#   Fjalar Sigurðarson (fjalar@vedur.is)
#   Tim Sonnemann (tim@vedur.is)
# Date:   Nov 2016
# Last:   Jan 2018
#
# This DB definition is for version 0-2-12,
# but all NOT-NULL constraints are removed here.

'''
SQLAlchemy classes for EPOS WP10 database tables.
'''

## SqlAlchemy imports
from sqlalchemy import (
    BigInteger, Boolean, Column, Date, DateTime, Enum, Float,
    ForeignKey, Index, Integer, Numeric, SmallInteger, String,
    Text, UniqueConstraint, text,
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

## Declarations

Base = declarative_base()

## Data Models ##

class Agency(Base):
    __tablename__ = 'agency'

    id = Column(Integer, primary_key=True, server_default=text("nextval('agency_id_seq'::regclass)"))
    name = Column(Text)
    abbreviation = Column(String(50), server_default=text("NULL::character varying"))
    address = Column(Text, server_default=text("NULL::character varying"))
    www = Column(String(250), server_default=text("NULL::character varying"))
    infos = Column(Text)

class AnalysisCenter(Base):
    __tablename__ = 'analysis_centers'

    id = Column(Integer, primary_key=True, server_default=text("nextval('analysis_centers_id_seq'::regclass)"))
    name = Column(String(150))
    abbreviation = Column(String(50), unique=True)
    contact = Column(String(150))
    email = Column(String(150))
    url = Column(String(150))

class AntennaType(Base):
    __tablename__ = 'antenna_type'
    
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    igs_defined = Column(String(1))
    model = Column(String(50))


class Attribute(Base):
    __tablename__ = 'attribute'

    id = Column(Integer, primary_key=True, server_default=text("nextval('attribute_id_seq'::regclass)"))
    name = Column(String(50), unique=True)


class Bedrock(Base):
    __tablename__ = 'bedrock'

    id = Column(Integer, primary_key=True, server_default=text("nextval('bedrock_id_seq'::regclass)"))
    condition = Column(Text)
    type = Column(Text)
    
    def __repr__(self):
        return "<Bedrock(id='{}', condition='{}', type='{}')>".format(
            self.id, self.condition, self.type)


class City(Base):
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True, server_default=text("nextval('city_id_seq'::regclass)"))
    id_state = Column(ForeignKey(u'state.id'))
    name = Column(Text)

    state = relationship(u'State')


class ColocationOffset(Base):
    __tablename__ = 'colocation_offset'

    id = Column(Integer, primary_key=True, server_default=text("nextval('colocation_offset_id_seq'::regclass)"))
    id_station_colocation = Column(ForeignKey(u'station_colocation.id'), index=True)
    offset_x = Column(Numeric)
    offset_y = Column(Numeric)
    offset_z = Column(Numeric)
    date_measured = Column(Date)

    station_colocation = relationship(u'StationColocation')


class Condition(Base):
    __tablename__ = 'condition'

    id = Column(Integer, primary_key=True, server_default=text("nextval('condition_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'))
    id_effect = Column(ForeignKey(u'effects.id'))
    date_from = Column(DateTime, server_default=text("('now'::text)::timestamp without time zone"))
    date_to = Column(DateTime)
    degradation = Column(Text)
    comments = Column(Text)

    effect = relationship(u'Effect')
    station = relationship(u'Station')


class Connection(Base):
    __tablename__ = 'connections'

    id = Column(Integer, primary_key=True)
    source = Column(ForeignKey(u'node.id'), index=True)
    destiny = Column(ForeignKey(u'node.id'), index=True)
    station = Column(ForeignKey(u'station.id'), index=True)
    metadata_ = Column('metadata',String(2))

    node = relationship(u'Node', primaryjoin='Connection.destiny == Node.id')
    node1 = relationship(u'Node', primaryjoin='Connection.source == Node.id')
    station1 = relationship(u'Station')


class Constellation(Base):
    __tablename__ = 'constellation'

    id = Column(Integer, primary_key=True, server_default=text("nextval('constellation_id_seq'::regclass)"))
    identifier_1ch = Column(String)
    identifier_3ch = Column(String(3))
    name = Column(String(127))
    official = Column(Boolean)


class Contact(Base):
    __tablename__ = 'contact'
    __table_args__ = (
        UniqueConstraint('name', 'email'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('contact_id_seq'::regclass)"))
    name = Column(String(50))
    title = Column(String(50))
    email = Column(String(400))
    phone = Column(String(50))
    gsm = Column(String(50))
    comment = Column(Text)
    id_agency = Column(ForeignKey(u'agency.id'))
    role = Column(String(50))
    fax = Column(String(50))   # added by Sergio Bruni (Nov 19 2018)

    agency = relationship(u'Agency')
    stations = relationship('Station', secondary='station_contact')

    def __repr__(self):
        return "<Contact(id='{}', name='{}', email='{}')>".format(
            self.id, self.name, self.email)


class Coordinates(Base):
    __tablename__ = 'coordinates'

    id = Column(Integer, primary_key=True, server_default=text("nextval('coordinates_id_seq'::regclass)"))
    x = Column(Numeric)
    y = Column(Numeric)
    z = Column(Numeric)
    lat = Column(Numeric(6, 4))
    lon = Column(Numeric(7, 4))
    altitude = Column(Numeric(6, 2))
    
    def __repr__(self):
        return ("<Coordinates(id='{}', x='{}', y='{}', z='{}', lat='{}', "
                "lon='{}', altitude='{}')>").format(
                    self.id, self.x, self.y, self.z,
                    self.lat, self.lon, self.altitude)

class Country(Base):
    __tablename__ = 'country'

    id = Column(Integer, primary_key=True, server_default=text("nextval('country_id_seq'::regclass)"))
    name = Column(Text)
    iso_code = Column(Text)


class DataCenter(Base):
    __tablename__ = 'data_center'

    id = Column(Integer, primary_key=True, server_default=text("nextval('data_center_id_seq'::regclass)"))
    acronym = Column(String(100))
    id_agency = Column(ForeignKey(u'agency.id'), index=True)
    hostname = Column(String(200))
    root_path = Column(Text)
    name = Column(String(100))
    protocol = Column(String(5))

    agency = relationship(u'Agency')


class DataCenterStructure(Base):
    __tablename__ = 'data_center_structure'

    id = Column(Integer, primary_key=True, server_default=text("nextval('data_center_structure_id_seq'::regclass)"))
    id_data_center = Column(ForeignKey(u'data_center.id'), index=True)
    id_file_type = Column(Integer)
    directory_naming = Column(Text)
    comments = Column(Text)

    data_center = relationship(u'DataCenter')


class DatacenterStation(Base):
    __tablename__ = 'datacenter_station'

    id = Column(Integer, primary_key=True)
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_datacenter = Column(ForeignKey(u'data_center.id'), index=True)
    datacenter_type = Column(Text)

    data_center = relationship(u'DataCenter')
    station = relationship(u'Station')


class Document(Base):
    __tablename__ = 'document'

    id = Column(Integer, primary_key=True, server_default=text("nextval('document_id_seq'::regclass)"))
    date = Column(Date)
    title = Column(Text)
    description = Column(Text)
    link = Column(String(60))
    id_station = Column(ForeignKey(u'station.id'))
    id_item = Column(ForeignKey(u'item.id'))
    id_document_type = Column(ForeignKey(u'document_type.id'))

    document_type = relationship(u'DocumentType')
    item = relationship(u'Item')
    station = relationship(u'Station')


class DocumentType(Base):
    __tablename__ = 'document_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('document_type_id_seq'::regclass)"))
    name = Column(String(50))


class Effect(Base):
    __tablename__ = 'effects'

    id = Column(Integer, primary_key=True, server_default=text("nextval('effects_id_seq'::regclass)"))
    type = Column(Text)


class EstimatedCoordinate(Base):
    __tablename__ = 'estimated_coordinates'
    __table_args__ = (
        Index('idx_estimated_coordinates_station_epoch', 'id_station', 'epoch'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('estimated_coordinates_id_seq'::regclass)"))
    x = Column(Numeric(35, 15))
    y = Column(Numeric(35, 15))
    z = Column(Numeric(35, 15))
    var_xx = Column(Numeric(35, 15))
    var_yy = Column(Numeric(35, 15))
    var_zz = Column(Numeric(35, 15))
    var_xy = Column(Numeric(35, 15))
    var_xz = Column(Numeric(35, 15))
    var_yz = Column(Numeric(35, 15))
    outlier = Column(Integer)
    epoch = Column(DateTime, index=True)
    id_method_identification = Column(ForeignKey(u'method_identification.id'))
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_processing_parameters = Column(ForeignKey(u'processing_parameters.id'))
    id_sinex_files = Column(ForeignKey(u'sinex_files.id'))

    method_identification = relationship(u'MethodIdentification')
    processing_parameter = relationship(u'ProcessingParameter')
    sinex_file = relationship(u'SinexFile')
    station = relationship(u'Station')


class FailedQuery(Base):
    __tablename__ = 'failed_queries'

    id = Column(Integer, primary_key=True)
    query = Column(Text)
    destiny = Column(ForeignKey(u'node.id'), index=True)

    node = relationship(u'Node')


class FileGenerated(Base):
    __tablename__ = 'file_generated'

    id_station = Column(ForeignKey(u'station.id'))
    id = Column(Integer, primary_key=True, server_default=text("nextval('file_generated_id_seq'::regclass)"))
    id_file_type = Column(ForeignKey(u'file_type.id'))

    file_type = relationship(u'FileType')
    station = relationship(u'Station')


class FileType(Base):
    __tablename__ = 'file_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('file_type_id_seq'::regclass)"))
    format = Column(Text)
    sampling_window = Column(Text)
    sampling_frequency = Column(Text)


class FilterAntenna(Base):
    __tablename__ = 'filter_antenna'

    id = Column(Integer, primary_key=True, server_default=text("nextval('filter_antenna_id_seq'::regclass)"))
    id_item_attribute = Column(ForeignKey(u'item_attribute.id'))
    id_antenna_type = Column(ForeignKey(u'antenna_type.id'))

    antenna_type = relationship(u'AntennaType')
    item_attribute = relationship(u'ItemAttribute')


class FilterRadome(Base):
    __tablename__ = 'filter_radome'

    id = Column(Integer, primary_key=True, server_default=text("nextval('filter_radome_id_seq'::regclass)"))
    id_item_attribute = Column(ForeignKey(u'item_attribute.id'))
    id_radome_type = Column(ForeignKey(u'radome_type.id'))

    item_attribute = relationship(u'ItemAttribute')
    radome_type = relationship(u'RadomeType')


class FilterReceiver(Base):
    __tablename__ = 'filter_receiver'

    id = Column(Integer, primary_key=True, server_default=text("nextval('filter_receiver_id_seq'::regclass)"))
    id_item_attribute = Column(ForeignKey(u'item_attribute.id'))
    id_receiver_type = Column(ForeignKey(u'receiver_type.id'))

    item_attribute = relationship(u'ItemAttribute')
    receiver_type = relationship(u'ReceiverType')


class Geological(Base):
    __tablename__ = 'geological'

    id = Column(Integer, primary_key=True, server_default=text("nextval('geological_id_seq'::regclass)"))
    id_bedrock = Column(ForeignKey(u'bedrock.id'))
    characteristic = Column(Text)
    fracture_spacing = Column(Text)
    fault_zone = Column(Text)
    distance_to_fault = Column(Text)

    bedrock = relationship(u'Bedrock')
    
    def __repr__(self):
        return ("<Geological(id='{}', characteristic='{}', "
                "fracture_spacing='{}, fault_zone='{}', "
                "distance_to_fault='{}', id_bedrock='{}')>").format(
                    self.id, self.characteristic, self.fracture_spacing,
                    self.fault_zone,self.distance_to_fault, self.id_bedrock)

class GnssObsname(Base):
    __tablename__ = 'gnss_obsnames'

    id = Column(SmallInteger, primary_key=True, server_default=text("nextval('gnss_obsnames_id_seq'::regclass)"))
    name = Column(String(3))
    frequency_band = Column(Integer)
    obstype = Column(String(1))
    channel = Column(String(1))
    official = Column(Boolean)


class Helmert(Base):
    __tablename__ = 'helmert'

    id = Column(Integer, primary_key=True, server_default=text("nextval('helmert_id_seq'::regclass)"))
    cx = Column(Numeric(35, 15))
    cy = Column(Numeric(35, 15))
    cz = Column(Numeric(35, 15))
    scale = Column(Numeric(35, 15))
    rx = Column(Numeric(35, 15))
    ry = Column(Numeric(35, 15))
    rz = Column(Numeric(35, 15))
    id_analysis_centers = Column(ForeignKey(u'analysis_centers.id'), index=True)
    id_old_frame = Column(ForeignKey(u'reference_frame.id'), index=True)
    id_new_frame = Column(ForeignKey(u'reference_frame.id'), index=True)

    analysis_center = relationship(u'AnalysisCenter')
    reference_frame = relationship(u'ReferenceFrame', primaryjoin='Helmert.id_new_frame == ReferenceFrame.id')
    reference_frame1 = relationship(u'ReferenceFrame', primaryjoin='Helmert.id_old_frame == ReferenceFrame.id')


class InstrumentColocation(Base):
    __tablename__ = 'instrument_collocation'

    id = Column(Integer, primary_key=True, server_default=text("nextval('colocation_instrument_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'))
    type = Column(String(100))
    status = Column(Enum(u'PERMANENT', u'MOBILE', name='coloc_status'))
    date_from = Column(DateTime)
    date_to = Column(DateTime)
    comment = Column(Text)

    station = relationship(u'Station')


class Item(Base):
    __tablename__ = 'item'

    id = Column(Integer, primary_key=True, server_default=text("nextval('item_id_seq'::regclass)"))
    id_item_type = Column(ForeignKey(u'item_type.id'))
    id_contact_as_producer = Column(ForeignKey(u'contact.id'))
    id_contact_as_owner = Column(ForeignKey(u'contact.id'))
    comment = Column(Text)

    contact = relationship(u'Contact', primaryjoin='Item.id_contact_as_owner == Contact.id')
    contact1 = relationship(u'Contact', primaryjoin='Item.id_contact_as_producer == Contact.id')

    item_type = relationship(u'ItemType')

    def __repr__(self):
        return ("<Item(id='{}', id_item_type='{}', "
                "id_contact_as_producer='{}', id_contact_as_owner='{}'"
                ")>").format(
                    self.id, self.id_item_type,
                    self.id_contact_as_producer,
                    self.id_contact_as_owner)


class ItemAttribute(Base):
    __tablename__ = 'item_attribute'

    id = Column(Integer, primary_key=True, server_default=text("nextval('item_attribute_id_seq'::regclass)"))
    id_item = Column(ForeignKey(u'item.id'))
    id_attribute = Column(ForeignKey(u'attribute.id'))
    date_from = Column(DateTime, server_default=text("('now'::text)::timestamp without time zone"))
    date_to = Column(DateTime)
    value_varchar = Column(String(50))
    value_date = Column(Date)
    value_numeric = Column(Numeric)

    attribute = relationship(u'Attribute')
    item = relationship(u'Item')


class ItemType(Base):
    __tablename__ = 'item_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('item_type_id_seq'::regclass)"))
    name = Column(String(50))


class ItemTypeAttribute(Base):
    __tablename__ = 'item_type_attribute'

    id = Column(Integer, primary_key=True, server_default=text("nextval('item_type_attribute_id_seq'::regclass)"))
    id_item_type = Column(ForeignKey(u'item_type.id'), index=True)
    id_attribute = Column(ForeignKey(u'attribute.id'), index=True)

    attribute = relationship(u'Attribute')
    item_type = relationship(u'ItemType')


class Jump(Base):
    __tablename__ = 'jump'

    id = Column(Integer, primary_key=True, server_default=text("nextval('jump_id_seq'::regclass)"))
    epoch = Column(DateTime)
    description = Column(String(100))
    dx = Column(Numeric(35, 15))
    dy = Column(Numeric(35, 15))
    dz = Column(Numeric(35, 15))
    dx_sigma = Column(Numeric(35, 15))
    dy_sigma = Column(Numeric(35, 15))
    dz_sigma = Column(Numeric(35, 15))
    jump_type = Column(String(100))
    rho_xy = Column(Numeric(35, 15))
    rho_xz = Column(Numeric(35, 15))
    rho_yz = Column(Numeric(35, 15))
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_method_identification = Column(ForeignKey(u'method_identification.id'), index=True)

    method_identification = relationship(u'MethodIdentification')
    station = relationship(u'Station')


class LocalTie(Base):
    __tablename__ = 'local_ties'

    id = Column(Integer, primary_key=True, server_default=text("nextval('local_ties_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'))
    name = Column(String(100))
    usage = Column(String(100))
    cpd_num = Column(String(9))
    iers_domes = Column(String(9))
    dx = Column(Numeric)
    dy = Column(Numeric)
    dz = Column(Numeric)
    accuracy = Column(Numeric)
    survey_method = Column(String(100))
    date_at = Column(Date)
    comment = Column(Text)

    station = relationship(u'Station')


class Location(Base):
    __tablename__ = 'location'

    id = Column(Integer, primary_key=True, server_default=text("nextval('location_id_seq'::regclass)"))
    id_city = Column(ForeignKey(u'city.id'))
    id_coordinates = Column(ForeignKey(u'coordinates.id'))
    id_tectonic = Column(ForeignKey(u'tectonic.id'))
    description = Column(Text)

    city = relationship(u'City')
    coordinate = relationship(u'Coordinates')
    tectonic = relationship(u'Tectonic')


class Log(Base):
    __tablename__ = 'log'

    id = Column(Integer, primary_key=True, server_default=text("nextval('log_id_seq'::regclass)"))
    title = Column(String(50))
    date = Column(Date)
    id_log_type = Column(ForeignKey(u'log_type.id'))
    id_station = Column(ForeignKey(u'station.id'))
    modified = Column(Text)
    previous = Column(String(30))
    id_contact = Column(ForeignKey(u'contact.id'))

    contact = relationship(u'Contact')
    log_type = relationship(u'LogType')
    station = relationship(u'Station')


class LogType(Base):
    __tablename__ = 'log_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('log_type_id_seq'::regclass)"))
    name = Column(String(30), unique=True)


class MethodIdentification(Base):
    __tablename__ = 'method_identification'

    id = Column(Integer, primary_key=True, server_default=text("nextval('method_identification_id_seq'::regclass)"))
    creation_date = Column(DateTime)
    doi = Column(String(100))
    software = Column(String(100))
    data_type = Column(String(100))
    id_analysis_centers = Column(ForeignKey(u'analysis_centers.id'))
    id_reference_frame = Column(ForeignKey(u'reference_frame.id'))

    analysis_center = relationship(u'AnalysisCenter')
    reference_frame = relationship(u'ReferenceFrame')


class Monument(Base):
    __tablename__ = 'monument'

    id = Column(Integer, primary_key=True, server_default=text("nextval('monument_id_seq'::regclass)"))
    description = Column(Text)
    inscription = Column(Text)
    height = Column(Numeric)
    foundation = Column(Text)
    foundation_depth = Column(Numeric)
    
    def __repr__(self):
        return ("<Monument(id='{}', descripton='{}', inscription='{}, "
                "height='{}, foundation='{}, foundation_depth='{}')>").format(
                    self.id, self.description, self.inscription, self.height,
                    self.foundation, self.foundation_depth)


class Network(Base):
    __tablename__ = 'network'

    id = Column(Integer, primary_key=True, server_default=text("nextval('network_id_seq'::regclass)"))
    name = Column(String(100), unique=True)
    id_contact = Column(ForeignKey(u'contact.id'))

    contact = relationship(u'Contact')
    stations = relationship ('Station', secondary='station_network')


class Node(Base):
    __tablename__ = 'node'

    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    host = Column(String(30))
    port = Column(String(30))
    dbname = Column(String(30))
    username = Column(String(30))
    password = Column(String(30))


class Noise(Base):
    __tablename__ = 'noise'

    id = Column(Integer, primary_key=True, server_default=text("nextval('noise_id_seq'::regclass)"))
    e_amplitude_white_noise = Column(Numeric(35, 15))
    n_amplitude_white_noise = Column(Numeric(35, 15))
    u_amplitude_white_noise = Column(Numeric(35, 15))
    e_amplitude_power_law_noise = Column(Numeric(35, 15))
    n_amplitude_power_law_noise = Column(Numeric(35, 15))
    u_amplitude_power_law_noise = Column(Numeric(35, 15))
    e_spectral_index_power_law_noise = Column(Numeric(35, 15))
    n_spectral_index_power_law_noise = Column(Numeric(35, 15))
    u_spectral_index_power_law_noise = Column(Numeric(35, 15))
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_method_identification = Column(ForeignKey(u'method_identification.id'), index=True)

    method_identification = relationship(u'MethodIdentification')
    station = relationship(u'Station')


class OtherFile(Base):
    __tablename__ = 'other_files'

    id = Column(Integer, primary_key=True, server_default=text("nextval('other_files_id_seq'::regclass)"))
    name = Column(String(250))
    id_data_center_structure = Column(ForeignKey(u'data_center_structure.id'), index=True)
    file_size = Column(Integer)
    id_file_type = Column(ForeignKey(u'file_type.id'), index=True)
    relative_path = Column(Text)
    creation_date = Column(Date)
    revision_time = Column(DateTime)
    md5checksum = Column(Text)
    status = Column(Integer)
    id_station = Column(ForeignKey(u'station.id'), index=True)

    data_center_structure = relationship(u'DataCenterStructure')
    file_type = relationship(u'FileType')
    station = relationship(u'Station')


class PlotVelocity(Base):
    __tablename__ = 'plot_velocities'

    id = Column(Integer, primary_key=True, server_default=text("nextval('plot_velocities_id_seq'::regclass)"))
    lat = Column(Numeric(35, 15))
    lon = Column(Numeric(35, 15))
    rad = Column(Numeric(35, 15))
    var_lat = Column(Numeric(35, 15))
    var_lon = Column(Numeric(35, 15))
    var_rad = Column(Numeric(35, 15))
    e_lat = Column(Numeric(35, 15))
    e_lon = Column(Numeric(35, 15))
    e_rad = Column(Numeric(35, 15))
    var_e_lat = Column(Numeric(35, 15))
    var_e_lon = Column(Numeric(35, 15))
    var_e_rad = Column(Numeric(35, 15))
    outlier = Column(Integer)
    epoch = Column(DateTime)
    jump = Column(Boolean)
    id_station = Column(ForeignKey(u'station.id'), index=True)

    station = relationship(u'Station')


class ProcessingParameter(Base):
    __tablename__ = 'processing_parameters'

    id = Column(Integer, primary_key=True, server_default=text("nextval('processing_parameters_id_seq'::regclass)"))
    sampling_period = Column(String(100), server_default=text("'daily'::character varying"))
    sinex_version = Column(String(100))
    otl_model = Column(String(100))
    antena_model = Column(String(100))
    cut_of_angle = Column(Numeric(35, 15))
    id_helmert = Column(ForeignKey(u'helmert.id'))

    helmert = relationship(u'Helmert')


class QcConstellationSummary(Base):
    __tablename__ = 'qc_constellation_summary'

    id = Column(Integer, primary_key=True, server_default=text("nextval('qc_constellation_summary_id_seq'::regclass)"))
    id_qc_report_summary = Column(ForeignKey(u'qc_report_summary.id'), index=True)
    id_constellation = Column(ForeignKey(u'constellation.id', match=u'FULL'), index=True)
    nsat = Column(Integer)
    xele = Column(Integer)
    epo_expt = Column(Integer)
    epo_have = Column(Integer)
    epo_usbl = Column(Integer)
    xcod_epo = Column(Integer)
    xcod_sat = Column(Integer)
    xpha_epo = Column(Integer)
    xpha_sat = Column(Integer)
    xint_epo = Column(Integer)
    xint_sat = Column(Integer)
    xint_sig = Column(Integer)
    xint_slp = Column(Integer)
    x_crd = Column(Float)
    y_crd = Column(Float)
    z_crd = Column(Float)
    x_rms = Column(Float)
    y_rms = Column(Float)
    z_rms = Column(Float)

    constellation = relationship(u'Constellation')
    qc_report_summary = relationship(u'QcReportSummary')


class QcNavigationMsg(Base):
    __tablename__ = 'qc_navigation_msg'

    id = Column(Integer, primary_key=True, server_default=text("nextval('qc_navigation_msg_id_seq'::regclass)"))
    id_qc_report_summary = Column(ForeignKey(u'qc_report_summary.id', match=u'FULL'), index=True)
    id_constellation = Column(ForeignKey(u'constellation.id', match=u'FULL'), index=True)
    nsat = Column(Integer)
    have = Column(Integer)

    constellation = relationship(u'Constellation')
    qc_report_summary = relationship(u'QcReportSummary')


class QcObservationSummaryC(Base):
    __tablename__ = 'qc_observation_summary_c'

    id = Column(BigInteger, primary_key=True, server_default=text("nextval('qc_observation_summary_c_id_seq'::regclass)"))
    id_qc_constellation_summary = Column(ForeignKey(u'qc_constellation_summary.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_gnss_obsnames = Column(ForeignKey(u'gnss_obsnames.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    obs_sats = Column(SmallInteger)
    obs_have = Column(Integer)
    obs_expt = Column(Integer)
    user_have = Column(Integer)
    user_expt = Column(Integer)
    cod_mpth = Column(Float(53))

    gnss_obsname = relationship(u'GnssObsname')
    qc_constellation_summary = relationship(u'QcConstellationSummary')


class QcObservationSummaryD(Base):
    __tablename__ = 'qc_observation_summary_d'

    id = Column(BigInteger, primary_key=True, server_default=text("nextval('qc_observation_summary_d_id_seq'::regclass)"))
    id_qc_constellation_summary = Column(ForeignKey(u'qc_constellation_summary.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_gnss_obsnames = Column(ForeignKey(u'gnss_obsnames.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    obs_sats = Column(SmallInteger)
    obs_have = Column(Integer)
    obs_expt = Column(Integer)
    user_have = Column(Integer)
    user_expt = Column(Integer)

    gnss_obsname = relationship(u'GnssObsname')
    qc_constellation_summary = relationship(u'QcConstellationSummary')


class QcObservationSummaryL(Base):
    __tablename__ = 'qc_observation_summary_l'

    id = Column(BigInteger, primary_key=True, server_default=text("nextval('qc_observation_summary_l_id_seq'::regclass)"))
    id_qc_constellation_summary = Column(ForeignKey(u'qc_constellation_summary.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_gnss_obsnames = Column(ForeignKey(u'gnss_obsnames.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    obs_sats = Column(SmallInteger)
    obs_have = Column(Integer)
    obs_expt = Column(Integer)
    user_have = Column(Integer)
    user_expt = Column(Integer)
    pha_slps = Column(Integer)

    gnss_obsname = relationship(u'GnssObsname')
    qc_constellation_summary = relationship(u'QcConstellationSummary')


class QcObservationSummaryS(Base):
    __tablename__ = 'qc_observation_summary_s'

    id = Column(BigInteger, primary_key=True, server_default=text("nextval('qc_observation_summary_s_id_seq'::regclass)"))
    id_qc_constellation_summary = Column(ForeignKey(u'qc_constellation_summary.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_gnss_obsnames = Column(ForeignKey(u'gnss_obsnames.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    obs_sats = Column(SmallInteger)
    obs_have = Column(Integer)
    obs_expt = Column(Integer)
    user_have = Column(Integer)
    user_expt = Column(Integer)

    gnss_obsname = relationship(u'GnssObsname')
    qc_constellation_summary = relationship(u'QcConstellationSummary')


class QcParameter(Base):
    __tablename__ = 'qc_parameters'

    id = Column(Integer, primary_key=True, server_default=text("nextval('qc_parameters_id_seq'::regclass)"))
    software_name = Column(String(127))
    software_version = Column(String(10))
    elevation_cutoff = Column(Integer)
    gps = Column(Boolean)
    glo = Column(Boolean)
    gal = Column(Boolean)
    bds = Column(Boolean)
    qzs = Column(Boolean)
    sbs = Column(Boolean)
    nav_gps = Column(Boolean)
    nav_glo = Column(Boolean)
    nav_gal = Column(Boolean)
    nav_bds = Column(Boolean)
    nav_qzs = Column(Boolean)
    nav_sbs = Column(Boolean)


class QcReportSummary(Base):
    __tablename__ = 'qc_report_summary'

    id = Column(Integer, primary_key=True, server_default=text("nextval('qc_report_summary_id_seq'::regclass)"))
    id_rinexfile = Column(ForeignKey(u'rinex_file.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_qc_parameters = Column(ForeignKey(u'qc_parameters.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    date_beg = Column(DateTime)
    date_end = Column(DateTime)
    data_smp = Column(Float(53))
    obs_elev = Column(Float)
    obs_have = Column(Integer)
    obs_expt = Column(Integer)
    user_have = Column(Integer)
    user_expt = Column(Integer)
    cyc_slps = Column(Integer)
    clk_jmps = Column(Integer)
    xbeg = Column(Integer)
    xend = Column(Integer)
    xint = Column(Integer)
    xsys = Column(Integer)

    qc_parameter = relationship(u'QcParameter')
    rinex_file = relationship(u'RinexFile')


class QualityFile(Base):
    __tablename__ = 'quality_file'

    id = Column(Integer, primary_key=True, server_default=text("nextval('quality_file_id_seq'::regclass)"))
    name = Column(String(250))
    id_rinexfile = Column(ForeignKey(u'rinex_file.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    id_data_center_structure = Column(ForeignKey(u'data_center_structure.id'), index=True)
    file_size = Column(Integer)
    id_file_type = Column(ForeignKey(u'file_type.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    relative_path = Column(Text)
    creation_date = Column(Date)
    revision_time = Column(DateTime)
    md5checksum = Column(Text)
    status = Column(Integer)

    data_center_structure = relationship(u'DataCenterStructure')
    file_type = relationship(u'FileType')
    rinex_file = relationship(u'RinexFile')


class Query(Base):
    __tablename__ = 'queries'

    id = Column(Integer, primary_key=True)
    query = Column(Text)
    metadata_ = Column('metadata',String(2))
    station_id = Column(ForeignKey(u'station.id'), index=True)

    station = relationship(u'Station')


class RadomeType(Base):
    __tablename__ = 'radome_type'

    id = Column(Integer, primary_key=True)
    name = Column(String(20), unique=True)
    igs_defined = Column(String(1))
    description = Column(String(250))


class ReceiverType(Base):
    __tablename__ = 'receiver_type'
    
    id = Column(Integer, primary_key=True)
    name = Column(String(20), unique=True)
    igs_defined = Column(String(1))
    model = Column(String(50))


class ReferenceFrame(Base):
    __tablename__ = 'reference_frame'

    id = Column(Integer, primary_key=True, server_default=text("nextval('reference_frame_id_seq'::regclass)"))
    name = Column(String(100))
    epoch = Column(Date)


class ReferencePositionVelocity(Base):
    __tablename__ = 'reference_position_velocities'

    id = Column(Integer, primary_key=True, server_default=text("nextval('reference_position_velocities_id_seq'::regclass)"))
    velx = Column(Numeric(35, 15))
    vely = Column(Numeric(35, 15))
    velz = Column(Numeric(35, 15))
    velx_sigma = Column(Numeric(35, 15))
    vely_sigma = Column(Numeric(35, 15))
    velz_sigma = Column(Numeric(35, 15))
    vel_rho_xy = Column(Numeric(35, 15))
    vel_rho_xz = Column(Numeric(35, 15))
    vel_rho_yz = Column(Numeric(35, 15))
    reference_position_x = Column(Numeric(35, 15))
    reference_position_y = Column(Numeric(35, 15))
    reference_position_z = Column(Numeric(35, 15))
    reference_position_x_sigma = Column(Numeric(35, 15))
    reference_position_y_sigma = Column(Numeric(35, 15))
    reference_position_z_sigma = Column(Numeric(35, 15))
    reference_position_rho_xy = Column(Numeric(35, 15))
    reference_position_rho_xz = Column(Numeric(35, 15))
    reference_position_rho_yz = Column(Numeric(35, 15))
    start_epoch = Column(DateTime)
    end_epoch = Column(DateTime)
    ref_epoch = Column(DateTime)
    id_method_identification = Column(ForeignKey(u'method_identification.id'), index=True)
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_sinex_files = Column(ForeignKey(u'sinex_files.id'))

    method_identification = relationship(u'MethodIdentification')
    sinex_file = relationship(u'SinexFile')
    station = relationship(u'Station')


class RinexErrorType(Base):
    __tablename__ = 'rinex_error_types'

    id = Column(Integer, primary_key=True)
    error_type = Column(Text)


class RinexError(Base):
    __tablename__ = 'rinex_errors'

    id = Column(Integer, primary_key=True)
    id_rinex_file = Column(ForeignKey(u'rinex_file.id'), index=True)
    id_error_type = Column(ForeignKey(u'rinex_error_types.id'), index=True)

    rinex_error_type = relationship(u'RinexErrorType')
    rinex_file = relationship(u'RinexFile')


class RinexFile(Base):
    __tablename__ = 'rinex_file'

    id = Column(Integer, primary_key=True, server_default=text("nextval('rinex_file_id_seq'::regclass)"))
    name = Column(String(250))
    id_station = Column(ForeignKey(u'station.id', ondelete=u'CASCADE', onupdate=u'CASCADE'), index=True)
    id_data_center_structure = Column(ForeignKey(u'data_center_structure.id'), index=True)
    file_size = Column(Integer)
    id_file_type = Column(ForeignKey(u'file_type.id', ondelete=u'RESTRICT', onupdate=u'RESTRICT'), index=True)
    relative_path = Column(Text)
    reference_date = Column(DateTime)
    creation_date = Column(DateTime)
    published_date = Column(DateTime)
    revision_date = Column(DateTime)
    md5checksum = Column(Text)
    md5uncompressed = Column(Text)
    status = Column(SmallInteger)

    data_center_structure = relationship(u'DataCenterStructure')
    file_type = relationship(u'FileType')
    station = relationship(u'Station')


class SeasonalSignal(Base):
    __tablename__ = 'seasonal_signal'

    id = Column(Integer, primary_key=True, server_default=text("nextval('seasonal_signal_id_seq'::regclass)"))
    amplitude_x_signal = Column(Numeric(35, 15))
    amplitude_y_signal = Column(Numeric(35, 15))
    amplitude_z_signal = Column(Numeric(35, 15))
    phase_x_signal = Column(Numeric(35, 15))
    phase_y_signal = Column(Numeric(35, 15))
    phase_z_signal = Column(Numeric(35, 15))
    amplitude_x_signal_sigma = Column(Numeric(35, 15))
    amplitude_y_signal_sigma = Column(Numeric(35, 15))
    amplitude_z_signal_sigma = Column(Numeric(35, 15))
    phase_x_signal_sigma = Column(Numeric(35, 15))
    phase_y_signal_sigma = Column(Numeric(35, 15))
    phase_z_signal_sigma = Column(Numeric(35, 15))
    amplitude_rho_xy = Column(Numeric(35, 15))
    amplitude_rho_xz = Column(Numeric(35, 15))
    amplitude_rho_yz = Column(Numeric(35, 15))
    phase_rho_xy = Column(Numeric(35, 15))
    phase_rho_xz = Column(Numeric(35, 15))
    phase_rho_yz = Column(Numeric(35, 15))
    frequency = Column(Numeric(35, 15))
    id_station = Column(ForeignKey(u'station.id'))
    id_method_identification = Column(ForeignKey(u'method_identification.id'), index=True)

    method_identification = relationship(u'MethodIdentification')
    station = relationship(u'Station')


class SinexFile(Base):
    __tablename__ = 'sinex_files'

    id = Column(Integer, primary_key=True, server_default=text("nextval('sinex_files_id_seq'::regclass)"))
    url = Column(String(100))
    sampling_period = Column(String(100), server_default=text("'daily'::character varying"))
    epoch = Column(DateTime)
    data_type = Column(String(50))
    id_analysis_centers = Column(ForeignKey(u'analysis_centers.id'))

    analysis_center = relationship(u'AnalysisCenter')


class State(Base):
    __tablename__ = 'state'

    id = Column(Integer, primary_key=True, server_default=text("nextval('state_id_seq'::regclass)"))
    id_country = Column(ForeignKey(u'country.id'))
    name = Column(Text)

    country = relationship(u'Country')


class Station(Base):
    __tablename__ = 'station'
    __table_args__ = (
        UniqueConstraint('marker', 'monument_num', 'receiver_num', 'country_code'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_id_seq'::regclass)"))
    name = Column(String(100))
    marker = Column(String(4))
    description = Column(Text)
    date_from = Column(DateTime)
    date_to = Column(DateTime)
    id_station_type = Column(ForeignKey(u'station_type.id'))
    comment = Column(Text)
    id_location = Column(ForeignKey(u'location.id'))
    id_monument = Column(ForeignKey(u'monument.id'))
    id_geological = Column(ForeignKey(u'geological.id'))
    iers_domes = Column(String(9), server_default=text("NULL::character varying"))
    cpd_num = Column(String(15), server_default=text("NULL::character varying"))
    monument_num = Column(Integer)
    receiver_num = Column(Integer)
    country_code = Column(String(3))

    #geological = relationship(u'Geological', lazy='joined')
    geological = relationship(u'Geological')
    location = relationship(u'Location')
    monument = relationship(u'Monument')
    station_type = relationship(u'StationType')
    networks = relationship (Network, secondary='station_network')
    contacts = relationship('Contact', secondary='station_contact')

    def __repr__(self):
        return ("<Station(id='{}', marker='{}', description='{}')>").format(
            self.id, self.marker, self.description)


class StationColocation(Base):
    __tablename__ = 'station_colocation'

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_colocation_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'), index=True)
    id_station_colocated = Column(ForeignKey(u'station.id'), index=True)

    station = relationship(u'Station', primaryjoin='StationColocation.id_station == Station.id')
    station1 = relationship(u'Station', primaryjoin='StationColocation.id_station_colocated == Station.id')


class StationContact(Base):
    __tablename__ = 'station_contact'

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_contact_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'))
    id_contact = Column(ForeignKey(u'contact.id'))
    role = Column(Text)

    contact = relationship(u'Contact', backref=backref("station_contact"))
    station = relationship(u'Station', backref=backref("station_contact"))

class StationItem(Base):
    __tablename__ = 'station_item'

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_item_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id', ondelete=u'CASCADE', onupdate=u'CASCADE'), index=True)
    id_item = Column(ForeignKey(u'item.id', ondelete=u'CASCADE', onupdate=u'CASCADE'), index=True)
    date_from = Column(DateTime)
    date_to = Column(DateTime)

    item = relationship(u'Item')
    station = relationship(u'Station')


class StationNetwork(Base):
    __tablename__ = 'station_network'

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_network_id_seq'::regclass)"))
    id_station = Column(ForeignKey(u'station.id'))
    id_network = Column(ForeignKey(u'network.id'))

    #network = relationship(u'Network')
    #station = relationship(u'Station')
    network = relationship(u'Network', backref=backref("station_network"))
    station = relationship(u'Station' , backref=backref("station_network"))


class StationType(Base):
    __tablename__ = 'station_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('station_type_id_seq'::regclass)"))
    name = Column(String(50), unique=True)
    type = Column(String(50))


class Tectonic(Base):
    __tablename__ = 'tectonic'

    id = Column(Integer, primary_key=True, server_default=text("nextval('tectonic_id_seq'::regclass)"))
    plate_name = Column(Text)
    
    def __repr__(self):
        return "<Tectonic(id='{}', plate_name='{}')>".format(
            self.id, self.plate_name)


class UserGroup(Base):
    __tablename__ = 'user_group'

    id = Column(Integer, primary_key=True, server_default=text("nextval('user_group_id_seq'::regclass)"))
    name = Column(String(50))


class UserGroupStation(Base):
    __tablename__ = 'user_group_station'

    id = Column(Integer, primary_key=True, server_default=text("nextval('user_group_station_id_seq'::regclass)"))
    id_user_group = Column(ForeignKey(u'user_group.id'), index=True)
    id_station = Column(ForeignKey(u'station.id'), index=True)

    station = relationship(u'Station')
    user_group = relationship(u'UserGroup')


class Zone(Base):
    __tablename__ = 'zone'

    id = Column(Integer, primary_key=True, server_default=text("nextval('zone_id_seq'::regclass)"))
    name = Column(String(50), unique=True)
